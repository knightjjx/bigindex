## Implementation plan

### TODO

#### Main framework
- [x] 1. Blinks

- [ ] 2. r-clique: adjust to the new decompressors (2018.02.28~2018.03.03)  

- [ ] 3. Heuristic generalization
   
   - [x] 1. Baseline: generalize to the parents
   - [x] 2. H1: Mul-layer generalization 
   - [x] 3. H2: Generalize with high potential compression ratio
   - [ ] 4. H3: Generalize with low potential graph distortion (2 days) 
   - [ ] 5. H4: Combine H2 and H3 (2~3 days)
   

### Experiments

#### Query 

1. Vary query semantics:
  
  - [x] 1. Random query keywords
  - [ ] 2. Under one branch (1 day) 
  - [x] 3. Some query from previous works

3. Vary query size

	- [ ] |Q| from 5 to 15 (1 day)

#### Data graph

- [x] 1. Yago: Done 

- [x] 2. dbpedia: Done

- [x] 3. synthetic:
	
	1. Change with different size of the ontology 
	2. Change with different size of the data graph 
	
- [ ] 4. Vary data graph size

	- [x] 1. synthetic datasets
	- [ ] 2. Sample with real knowledge graphs (2 days)

#### Evaluation

- [x] 1. Vistied node: the efficiency of the pruning
- [x] 2. Evaluation time: Query time(on compressed graph) + Decompression time **VS** Query time(on original graph)
- [ ] 3. Index size: The index on the BiGindex and that of the original graph (1 day)
- [x] 4. Index construction time: The index built on the BiGindex and that of the original graph  


#### Parameter of BiGindex
With different heuristic generalization function

- [x] 1. Query time
- [x] 2. Decompression time
- [ ] 3. Index size (Included in the previous plan)
- [x] 4. Index construction time