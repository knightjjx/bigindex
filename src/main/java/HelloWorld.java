import java.util.HashSet;
import java.util.Set;

/**
 * Created by samjjx on 2017/5/5.
 */
public class HelloWorld {
    public static void main(String[] args){
        pair p1= new pair(3,5);
        pair p2= new pair(3,5);
        Set<pair> tmp=new HashSet<pair>();
        tmp.add(p1);
        tmp.add(p2);
        System.out.println(tmp.size());
        String test1="hello";
        String test2="hello";
        System.out.println(test1==test2);
    }
}

class pair{
    int src,dst;
    public pair(int src, int dst){
        this.src = src;
        this.dst = dst;
    }

    @Override
    public int hashCode() {
        return src+dst;
    }

}