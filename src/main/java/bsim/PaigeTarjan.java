package bsim;

import graph.SGraph;
import graph.SVertex;

import java.util.*;

/**
 * Created by samjjx on 2017/5/10.
 */
public class PaigeTarjan {
    public HashSet<SVertex> WF = new HashSet<SVertex>();
    int maxrank;
    public HashMap<Integer, Integer> bmaps;
    public HashMap<Integer, ArrayList<Integer>> dmaps;
    public HashMap<Integer, HashSet<SVertex>> invert_blocks;

    public PaigeTarjan() {
        maxrank = Integer.MIN_VALUE;
        bmaps = new HashMap<Integer, Integer>();
        dmaps = new HashMap<Integer, ArrayList<Integer>>();
        invert_blocks = new HashMap<Integer, HashSet<SVertex>>();
    }

    public boolean simCheck(SVertex v1, SVertex v2, SGraph graph) {
        if (v1.getAttr() != v2.getAttr())
            return false;
        if (graph.getChildren(v1).isEmpty() && graph.getChildren(v2).isEmpty())
            return true;

        boolean judge = false;

        for (SVertex c1 : graph.getChildren(v1)) {
            judge = false;
            for (SVertex c2 : graph.getChildren(v2)) {
                if (this.simCheck(c1, c2, graph)) {
                    judge = true;
                    break;
                }
            }
            if (!judge) {
                return judge;
            }
        }
        return judge;
    }

    public SGraph akIndex(SGraph graph, int k) {
        HashMap<Integer, ArrayList<SVertex>> attr = new HashMap<Integer, ArrayList<SVertex>>();
        for (SVertex v : graph.allVertices()) {
            if (!attr.containsKey(v.getAttr())) {
                attr.put(v.getAttr(), new ArrayList<SVertex>());
            }
            attr.get(v.getAttr()).add(v);
        }

        Set<Integer> label = attr.keySet();
        for (int l : label) {
            ArrayList<SVertex> P = attr.get(l);
            for (SVertex v : P) {
                v.bmap = v.id;
            }
            for (int i = 0; i < P.size(); i++) {
                for (int j = i + 1; j < P.size(); j++) {
                    if (simCheckAk(P.get(i), P.get(j), graph, k)) {
                        P.get(j).bmap = P.get(i).bmap;
                    }
                }
            }
        }
        SGraph cgraph = new SGraph();
        for (SVertex v : graph.allVertices()) {
            if (!cgraph.contains(v.bmap)) {
                cgraph.addVertex(v.bmap, graph.getVertex(v.bmap).getAttr());
            }
        }
        for (SVertex v : graph.allVertices()) {
            for (int c : graph.getChildren(v.getID())) {
                int from = bmaps.get(v.getID());
                int to = bmaps.get(c);
                if (!cgraph.contains(from, to)) {
                    cgraph.addEdge(from, to);
                }
            }
        }
        return cgraph;
    }

    public boolean simCheckAk(SVertex v1, SVertex v2, SGraph graph, int k) {
        if (v1.getAttr() != v2.getAttr())
            return false;
        if (graph.getChildren(v1).isEmpty() && graph.getChildren(v2).isEmpty())
            return true;
        if (k == 0)
            return true;
        boolean judge = false;
        Set<SVertex> v2Children = graph.getChildren(v2);
        for (SVertex c1 : graph.getChildren(v1)) {
            judge = false;
            for (SVertex c2 : v2Children) {
                if (this.simCheckAk(c1, c2, graph, k - 1)) {
                    judge = true;
                    break;
                }
            }
            if (!judge) {
                return judge;
            }
        }
        return judge;
    }

    public HashSet<HashSet<SVertex>> clusterByattr(HashSet<SVertex> block) {
        HashSet<HashSet<SVertex>> res = new HashSet<HashSet<SVertex>>();
        HashMap<Integer, HashSet<SVertex>> blocks = new HashMap<Integer, HashSet<SVertex>>();
        for (SVertex v : block) {
            int attr = v.getAttr();
            if (!blocks.containsKey(attr)) {
                blocks.put(attr, new HashSet<SVertex>());
            }
            blocks.get(attr).add(v);
            invert_blocks.put(v.id, blocks.get(attr));
        }
        Set<Integer> keyset = blocks.keySet();

        for (int i : keyset) {
            res.add(blocks.get(i));
        }
        return res;
    }

    public void sortBlocks(HashSet<Integer> pb, HashSet<Integer> cb, SGraph graph){
        for(int v: pb){
            graph.getChildren(v);



        }
    }
    /**
     * This method make sure the dmaps are ordered.
     */
    public void orderDMap(HashMap<Integer, HashSet<SVertex>> partitionUni, SGraph compressed) {
        for (int i = maxrank; i > 0; i--) {
            HashSet<SVertex> par = partitionUni.get(i);
            HashSet<Integer> par_dig = new HashSet<Integer>();
            for(SVertex v: par){
                par_dig.add(v.id);
            }
            Iterator<Integer> parIte = par_dig.iterator();
            while(parIte.hasNext()){
                int first = parIte.next();
                int comP = bmaps.get(first);
                HashSet<Integer> comChildren = compressed.getChildren(comP);
                ArrayList<Integer> parents_dmap = dmaps.get(comP);




                par_dig.removeAll(parents_dmap);
                parIte = par_dig.iterator();
            }
        }
    }

    /**
     * Compress a graph by bisimulation
     *
     * @param graph original graph
     * @return compressed graph
     */
    public SGraph bisimulationGeneral(SGraph graph) {
        HashMap<Integer, HashSet<HashSet<SVertex>>> partition = new HashMap<Integer, HashSet<HashSet<SVertex>>>();
        HashMap<Integer, HashSet<SVertex>> partitionUni = new HashMap<Integer, HashSet<SVertex>>();
        HashMap<Integer, HashSet<SVertex>> partmp = new HashMap<Integer, HashSet<SVertex>>();
        maxrank = graph.maxRank;

        /*
            Init the partitions: step 3~4
         */
        SGraph compressed = graph.clone();

        for (SVertex v : graph.allvertices) {
            if (!partmp.containsKey(v.getRank())) {
                partmp.put(v.getRank(), new HashSet<SVertex>());
                partitionUni.put(v.getRank(), new HashSet<SVertex>());
            }
            partitionUni.get(v.getRank()).add(v);
            partmp.get(v.getRank()).add(v);
        }
        for (int i : partmp.keySet()) {
            partition.put(i, new HashSet<HashSet<SVertex>>());
            partition.get(i).addAll(clusterByattr(partmp.get(i)));
        }

        /*
        Step 5: collapse the partition with the MIN values
         */
        HashSet<SVertex> nVSet;
        if (partitionUni.containsKey(Integer.MIN_VALUE)) {
            int attr = partitionUni.get(Integer.MIN_VALUE).iterator().next().attr;
            nVSet = collapse(compressed, partitionUni.get(Integer.MIN_VALUE), attr, graph);
            System.out.println(partitionUni.get(Integer.MIN_VALUE));

            for (SVertex v : nVSet) {
                buildCrossMap(v, partitionUni.get(Integer.MIN_VALUE), graph);
            }
            /*
            Step 6: Refine the graph
            */
            System.out.println("[Blocks]Before refine\t" + numBlocks(partition));
            System.out.println("[Blocks]Before refine\t" + invert_blocks.size());
            refine(compressed, nVSet, partition, Integer.MIN_VALUE);
            System.out.println("[Blocks]After refine\t" + numBlocks(partition));
            System.out.println("[Blocks]After refine\t" + invert_blocks.size());
        }

        System.out.println("Finish MIN_INTEGER RANK");
        /*
        Step 7: Iteratively refines the node sets with higher rank
         */
        for (int i = 0; i <= maxrank; i++) {
            System.out.println("Rankt\t" + i);
            System.out.println("MaxRank\t" + maxrank);
//            HashSet<HashSet<SVertex>> Di = Refine(partition.get(i), partitionUni.get(i), compressed.parents);

//            partition.put(i, Di);
            HashSet<SVertex> allNVSet = new HashSet<SVertex>();
            int count = 0;
            for (HashSet<SVertex> X : partition.get(i)) {
                count++;
//                System.out.println("Begin\t" + count);
//                System.out.println("Total\t" + partition.get(i).size());
//                System.out.println("Max Rank\t" + maxrank);
//                System.out.println("X\t" + X.size());
                if (X.isEmpty()) {
                    continue;
                }
                if (X.size() == 1) {
                    int xid = X.iterator().next().id;
                    dmaps.put(xid, new ArrayList<Integer>());
                    dmaps.get(xid).add(xid);
                    bmaps.put(xid, xid);
                    continue;
                }
                int attr = X.iterator().next().attr;
                System.out.println("Begin collapse");
                nVSet = collapse(compressed, X, attr, graph);
                System.out.println("Finish collapse");
                for (SVertex v : nVSet) {
                    buildCrossMap(v, X, graph);
                }
                System.out.println("Finish cross_map");
                System.out.println("nVSet size\t" + nVSet.size());
                allNVSet.addAll(nVSet);
                System.out.println("Finish adding");
            }
            System.out.println("Begin Refine");
            System.out.println("[Blocks]Before refine\t" + numBlocks(partition));
            System.out.println("[Blocks]Before refine\t" + invert_blocks.size());
            refine(compressed, allNVSet, partition, i);
            System.out.println("[Blocks]After refine\t" + numBlocks(partition));
            System.out.println("[Blocks]After refine\t" + invert_blocks.size());
            System.out.println("Finish Refine");
        }
        compressed.setLayer(graph.layer + 1);

        System.out.println("Begin Generate RTable");
        compressed.generateRTable();
        System.out.println("Finish Generate RTable");
        compressed.bmaps = bmaps;
        compressed.dmaps = dmaps;
        assert compressed.vertexSize() == compressed.dmaps.size() : "Dmap are not match\t" + compressed.vertexSize() + "\t" + compressed.dmaps.size() + "\n" + compressed;
        return compressed;
    }

    public void buildCrossMap(SVertex nVertex, HashSet<SVertex> block, SGraph origraph) {
        ArrayList<Integer> dmap = new ArrayList<Integer>();
        for (SVertex v : block) {
            SVertex oriVertex = origraph.getVertex(v.id);
            dmap.add(oriVertex.id);
            bmaps.put(oriVertex.id, nVertex.id);
        }
        dmaps.put(nVertex.id, dmap);
    }

    /**
     * compute E^-1
     *
     * @param S    : subset of U
     * @param nset : Universal elements
     * @return pMap : mapping between node and its corresponding parent nodes
     */
    public HashSet<SVertex> cmpRelation(HashSet<SVertex> S, HashSet<SVertex> nset, HashMap<SVertex, Vector<SVertex>> pMap) {
        HashSet<SVertex> Ereverse = new HashSet<SVertex>();
        for (SVertex x : S) {
            Vector<SVertex> pset = pMap.get(x);
            if (pset != null) {
                Ereverse.addAll(pset);
            }
        }
        Ereverse.retainAll(nset);
        if (Ereverse.size() != 0)
            System.out.println("Ereverse size:" + Ereverse.size());
        return Ereverse;
    }


    /**
     * split the initial partition
     * each block is split into two parts
     *
     * @param S    : subset of U
     * @param Q    : initial partition
     * @param nset : Universal elements
     * @param pMap : mapping between node and its corresponding parent nodes
     */
    public void split(HashSet<SVertex> S, HashSet<HashSet<SVertex>> Q, HashSet<SVertex> nset, HashMap<SVertex, Vector<SVertex>> pMap) {
        long t0 = System.currentTimeMillis();
        int size1 = Q.size();
        HashSet<SVertex> Ereverse = this.cmpRelation(S, nset, pMap);
//        HashSet<SVertex> Ereverse = new HashSet<SVertex>();
//        System.out.println("[E]"+(System.currentTimeMillis()-t0));
        HashSet<HashSet<SVertex>> tmp = new HashSet<HashSet<SVertex>>();
        t0 = System.currentTimeMillis();

        for (HashSet<SVertex> B : Q) {
            HashSet<SVertex> B1 = new HashSet<SVertex>();
            HashSet<SVertex> B2 = new HashSet<SVertex>();
            B1.addAll(B);
            B2.addAll(B);
            B1.retainAll(Ereverse);
            B2.removeAll(Ereverse);
            if (!B1.isEmpty()) {
                tmp.add(B1);
                System.out.println("[mark 1]");
            }
            if (!B2.isEmpty()) {
                tmp.add(B2);
//                System.out.println("[mark 2]");
            }
        }
//        System.out.println("[Intersection]"+(System.currentTimeMillis()-t0));
        Q.clear();
        Q.addAll(tmp);
    }


    /**
     * @param Q    : initial partition
     * @param nset : Universal elements
     * @param pMap : mapping between node and its corresponding parent nodes
     */
    public HashSet<HashSet<SVertex>> Refine(HashSet<HashSet<SVertex>> Q,
                                            HashSet<SVertex> nset,
                                            HashMap<SVertex, Vector<SVertex>> pMap) {
        HashSet<HashSet<SVertex>> X = new HashSet<HashSet<SVertex>>();        // another partition
        X.add(nset);        // initially X contains the whole nodes -- only one partition
        while (!Q.equals(X)) {
            HashSet<SVertex> S = new HashSet<SVertex>();                                    // temporary block
            HashSet<SVertex> B = new HashSet<SVertex>();
            Search:
            for (HashSet<SVertex> tmp : X) {
                if (!Q.contains(tmp)) {
                    S.addAll(tmp);
                    for (HashSet<SVertex> block : Q) {
                        if (S.containsAll(block) && block.size() <= S.size() / 2) {
                            B.addAll(block);
                            break Search;
                        }
                    }
                }
            }
            X.remove(S);
            S.removeAll(B);
            if (!B.isEmpty()) {
                X.add(B);
            }
            if (!S.isEmpty()) {
                X.add(S);
            }

            this.split(B, Q, nset, pMap);
            this.split(S, Q, nset, pMap);
//            System.out.println("Time: "+(System.currentTimeMillis()-t0));
        }
        return Q;
    }

    public int numBlocks(HashMap<Integer, HashSet<HashSet<SVertex>>> partition) {
        Set<Integer> keyset = partition.keySet();
        int count = 0;
        for (int key : keyset) {
            count += partition.get(key).size();
        }
        return count;
    }

    /**
     * This refine function is for the vertices with INTEGER_MIN_VALUE
     *
     * @param graph:    original graph
     * @param B:
     * @param partition
     * @return
     */
    public void refine(SGraph graph, HashSet<SVertex> B, HashMap<Integer, HashSet<HashSet<SVertex>>> partition, int curent_rank) {
        if (B.size() == 0 || B.size() == 1)
            return;
//        System.out.println(B.size() + " times refine");
        int count = 0;
        for (SVertex n : B) {
            System.out.println("Times:" + count++);
            if (curent_rank == Integer.MIN_VALUE) {
                for (int i = 0; i <= maxrank; i++) {
                    HashSet<HashSet<SVertex>> partitionCopy = new HashSet<HashSet<SVertex>>();
                    HashSet<HashSet<SVertex>> hPartition = partition.get(i);
                    for (HashSet<SVertex> hBlock : hPartition) {
                        if (hBlock.size() == 1) {
                            partitionCopy.add(hBlock);
                            continue;
                        }
                        HashSet<SVertex> vBlock1 = new HashSet<SVertex>();
                        HashSet<SVertex> vBlock2 = new HashSet<SVertex>();

                        for (SVertex m : hBlock) {
                            if (graph.contains(m.id, n.id)) {
                                vBlock1.add(m);
                                invert_blocks.put(m.id, vBlock1);
                            } else {
                                vBlock2.add(m);
                                invert_blocks.put(m.id, vBlock2);
                            }
                        }
                        partitionCopy.add(vBlock1);
                        partitionCopy.add(vBlock2);
                    }
                    partition.put(i, partitionCopy);
                }
            } else {
                HashSet<Integer> parents = graph.getCopyParents(n.id);
                System.out.println("Number of parents\t" + parents.size());
                Iterator<Integer> pIter = parents.iterator();
                while (pIter.hasNext()) {
                    int p = pIter.next();
                    HashSet<SVertex> hblock = invert_blocks.get(p);
                    if (hblock.size() == 1 || hblock.size() == 0)
                        continue;
                    HashSet<SVertex> vBlock1 = new HashSet<SVertex>();
                    HashSet<SVertex> vBlock2 = new HashSet<SVertex>();
                    int rank = 0;
                    for (SVertex m : hblock) {
                        rank = m.getRank();
                        if (graph.contains(m.id, n.id)) {
                            vBlock1.add(m);
                            invert_blocks.put(m.id, vBlock1);
                        } else {
                            vBlock2.add(m);
                            invert_blocks.put(m.id, vBlock2);
                        }
                        parents.remove(m.id);
                    }
                    parents.remove(p);
                    System.out.println("[Remove]Number of parents\t" + parents.size());
                    partition.get(rank).remove(hblock);
                    partition.get(rank).add(vBlock1);
                    partition.get(rank).add(vBlock2);
                    pIter = parents.iterator();
                }
            }
        }
        return;
    }

    /**
     * @param graph: The graph to be compressed
     * @param B:     The block to collapse
     * @return: collapse graph
     */
    public HashSet<SVertex> collapse(SGraph graph, HashSet<SVertex> B, int attr, SGraph oriGraph) {
        HashSet<SVertex> nVset = new HashSet<SVertex>();
        HashSet<Integer> B_dig = new HashSet<Integer>();
        for (SVertex v : B) {
            B_dig.add(v.id);
        }

        if (B.size() == 0)
            return nVset;
        if (B.size() == 1) {
            return B;
        }
        SVertex nVertex = graph.addVertex(attr);
        nVertex.setRank(B.iterator().next().getRank());
        nVset.add(nVertex);
        invert_blocks.put(nVertex.id, nVset);
        HashSet<Integer> tmpP = new HashSet<Integer>();
        HashSet<Integer> tmpC = new HashSet<Integer>();
        int count = 0;
        long t0, t1 = 0, t2 = 0, t3 = 0;
        for (SVertex vertex : B) {
            count++;
            if (count % 100 == 0) {
                System.out.println("Finish " + count);
                System.out.println("Total\t" + B.size());
                System.out.println("t1\t" + t1);
                System.out.println("t2\t" + t2);
                System.out.println("t3\t" + t3);
            }
            t0 = System.currentTimeMillis();
            Set<Integer> pSet = oriGraph.getParents(vertex.id);
            t1 += System.currentTimeMillis() - t0;
            t0 = System.currentTimeMillis();
            for (int p : pSet) {
                if (B_dig.contains(p) || tmpP.contains(p))
                    continue;
                if (graph.contains(p))
                    graph.addEdge(p, nVertex.id);
                else
                    graph.addEdge(bmaps.get(p), nVertex.id);

                tmpP.add(p);
            }
            t2 += System.currentTimeMillis() - t0;
            Set<Integer> cSet = oriGraph.getChildren(vertex.id);
            for (int c : cSet) {
                if (B_dig.contains(c) || tmpC.contains(c))
                    continue;
                if (graph.contains(c))
                    graph.addEdge(nVertex.id, c);
                else
                    graph.addEdge(nVertex.id, bmaps.get(c));
                tmpC.add(c);
            }
            graph.removeVertex(vertex.getID());
        }
        return nVset;
    }

    /**
     * This algorithm is brute-force method to do bisimulation
     *
     * @param graph: The original graph
     * @return compressed graph
     */
    public SGraph bisimulation(SGraph graph) {
        HashMap<Integer, ArrayList<SVertex>> attr = new HashMap<Integer, ArrayList<SVertex>>();
        for (SVertex v : graph.allVertices()) {
            if (!attr.containsKey(v.getAttr())) {
                attr.put(v.getAttr(), new ArrayList<SVertex>());
            }
            attr.get(v.getAttr()).add(v);
        }

        Set<Integer> label = attr.keySet();
        for (int l : label) {
            ArrayList<SVertex> P = attr.get(l);
            for (SVertex v : P) {
                v.bmap = v.id;
            }
            for (int i = 0; i < P.size(); i++) {
                for (int j = i + 1; j < P.size(); j++) {
                    if (simCheck(P.get(i), P.get(j), graph)) {
                        P.get(j).bmap = P.get(i).bmap;
                    }
                }
            }
        }
        SGraph cgraph = new SGraph();
        for (SVertex v : graph.allVertices()) {
            if (!cgraph.contains(v.bmap)) {
                cgraph.addVertex(v.bmap, graph.getVertex(v.bmap).getAttr());
            }
        }
        for (SVertex v : graph.allVertices()) {
            for (int c : graph.getChildren(v.getID())) {
                int from = bmaps.get(v.getID());
                int to = bmaps.get(c);
                if (!cgraph.contains(from, to)) {
                    cgraph.addEdge(from, to);
                }
            }
        }
        return cgraph;
    }
}
