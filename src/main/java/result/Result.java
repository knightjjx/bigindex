package result;

import graph.SGraph;
import util.IOPack;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by samjjx on 2017/7/21.
 */
public class Result {
    int layer = Integer.MIN_VALUE;
    SGraph answer;

    public void WriteToDisk(String path) throws IOException {
        IOPack ioPack = new IOPack();
        BufferedWriter bw = ioPack.getWritter(path);
        bw.write(this.toString());
        bw.flush();
        bw.close();
    }

    public void loadFromDisk(String path) throws IOException {
    }
}
