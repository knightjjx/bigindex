package result;

import graph.AGraph;
import graph.SGraph;
import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by samjjx on 2017/7/19.
 */
public class BlinksResult extends Result {
    public int vertex;  // root vid
    HashMap<Integer, Integer> dist; // keyword -> distance

    int sumDis;

    int numKeyword;
    HashSet<Integer> keywords;
    HashSet<Integer> content_vertex;
    public AGraph ans;
    int layer;
    public BlinksResult(int vid, int dist, int keyword, int numKeyword, int layer) {
        this.vertex = vid;
        this.dist = new HashMap<Integer, Integer>();
        this.dist.put(keyword, dist);
        this.sumDis = 0;
        this.numKeyword = numKeyword;
        this.keywords = new HashSet<Integer>();
        this.keywords.add(keyword);
        this.answer = new SGraph();
        this.layer = layer;
        this.ans = new AGraph(layer, this.getRoot());
        this.content_vertex = new HashSet<Integer>();
    }

    public int sumLBDist() {
        return sumDis;
    }

    public HashMap<Integer, Integer> getResult() {
        return dist;
    }

    public void add(int keyword, int dist) {
        if (this.keywords.contains(keyword))
            return;
        this.keywords.add(keyword);
        this.sumDis += dist;
    }

    public void setKeywordDistance(int keyword, int distance) {
        this.dist.put(keyword, distance);
    }

    public boolean isFound() {
        return keywords.size() == numKeyword;
    }

    public void setLayer(int layer) {
        this.answer.layer = layer;
    }
    public int getRoot(){
        return this.vertex;
    }
    public int getLayer(){
        return this.layer;
    }

    /*
    layer
    root
    # of vertices
    vid attr
    vid attr
    src1 dst1 dst2 ...
    src2 dst1 dst2 ...
     */
    public String toString(){
        return getLayer()+"\n"+getRoot()+"\n"+ans;
    }

    @Override
    public void loadFromDisk(String path) throws IOException {
        IOPack ioPack = new IOPack();

        BufferedReader br = ioPack.getBufferInstance("path");

        String line = "";
        line = br.readLine();
        int layer = Integer.parseInt(line.trim());
        line = br.readLine();
        int root = Integer.parseInt(line.trim());
        AGraph ans = new AGraph(layer, root);
        int numVertex = Integer.parseInt(line.trim());
        for(int i =0;i<numVertex;i++){
            line = br.readLine();
            String[] data = line.split("\t");
            ans.addVertex(Integer.parseInt(data[0]), Integer.parseInt(data[1].trim()));
        }
        while((line=br.readLine())!=null){
            String[] data = line.split("\t");
            for(int i = 1; i< data.length;i++){
                ans.addEdge(Integer.parseInt(data[0].trim()), Integer.parseInt(data[i].trim()));
            }
        }
    }
}
