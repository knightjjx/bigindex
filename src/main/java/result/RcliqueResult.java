package result;

import graph.AGraph;
import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by samjjx on 2017/8/19.
 */
public class RcliqueResult extends Result {
    int numKeyword;
    HashSet<Integer> keywords;
    public AGraph ans;
    int layer;

    public RcliqueResult(int numKeyword, int layer) {
        keywords = new HashSet<Integer>();
        //FIXME: root
        ans = new AGraph(layer, 0);
        this.layer = layer;
        this.numKeyword = numKeyword;
    }

    public RcliqueResult(String path) throws IOException {
        loadFromDisk(path);
    }
    public HashSet<Integer> getVertex() {
        HashSet<Integer> res = new HashSet<Integer>();
        res.addAll(ans.allvertices.keySet());
        return res;
    }

    /*
    layer
    # of vertices
    vid attr
    vid attr
    src1 dst1 dst2 ...
    src2 dst1 dst2 ...
     */
    @Override
    public String toString() {
        return "layer:\t"+ans.layer+"\n"+ans.toString();
    }

    @Override
    public void loadFromDisk(String path) throws IOException {
        IOPack ioPack = new IOPack();

        BufferedReader br = ioPack.getBufferInstance("path");

        String line = "";
        line = br.readLine();
        int layer = Integer.parseInt(line.trim());
        AGraph ans = new AGraph(layer);
        int numVertex = Integer.parseInt(line.trim());
        for(int i =0;i<numVertex;i++){
            line = br.readLine();
            String[] data = line.split("\t");
            ans.addVertex(Integer.parseInt(data[0]), Integer.parseInt(data[1].trim()));
        }
        while((line=br.readLine())!=null){
            String[] data = line.split("\t");
            for(int i = 1; i< data.length;i++){
                ans.addEdge(Integer.parseInt(data[0].trim()), Integer.parseInt(data[i].trim()));
            }
        }
    }
}
