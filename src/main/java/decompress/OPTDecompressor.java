package decompress;

import graph.AGraph;
import graph.Path;
import graph.SGraph;
import graph.SVertex;
import index.BigIndex;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by samjjx on 2018/1/30.
 */
public class OPTDecompressor {
    private Queue<AGraph> results;
    private Queue<AGraph> ori_answer;
    public Queue<Integer> allAnswers;
    private BigIndex bigIndex;
    public int topk = 50;
    public Logger logger;
    public OPTDecompressor(Queue<AGraph> results, BigIndex bigIndex, Logger logger) {
        this.results = results;
        this.bigIndex = bigIndex;
        ori_answer = new LinkedList<AGraph>();
        allAnswers = new LinkedList<Integer>();
        this.logger = logger;
    }

    public Queue<AGraph> getResults() {
        return results;
    }

    public void decompressor() {
        if (results.isEmpty())
            return;

        while (!results.isEmpty() && ori_answer.size() < topk) {
            AGraph head_ans = results.remove();
            allAnswers.addAll(onePatternDecompressor(head_ans));
        }
    }

    public LinkedList<Integer> onePatternDecompressor(AGraph ans) {
        LinkedList<Integer> res = new LinkedList<Integer>();
        if(ans.layer == 1){
            res.add(ans.root);
            return res;
        }
        SGraph ground = bigIndex.MulGraph.get(1);
        System.out.println("Order begin");
        ans.leafOrder();
        System.out.println("Order finish");
        HashMap<Integer, HashSet<Integer>> dmaps = new HashMap<Integer, HashSet<Integer>>();
        Queue<Integer> order = new LinkedList<Integer>();
        System.out.println("==========dmaps===========\n");
        logger.info("==========dmaps===========\n");
        while (!ans.decompressor_order.isEmpty()) {
            int head = ans.decompressor_order.remove();
            dmaps.put(head, getDmaps(head, ans.layer));
            logger.info("# of dmaps\t"+head+"\t"+dmaps.get(head).size()+"\n");
            order.add(head);
        }
        logger.info("======================");

        /**
         * Pruning the contents by the attributes
         */
        logger.info("# of content vertices\t"+ans.content_vertex.size()+"\n");
        for (int v : ans.content_vertex) {
            HashSet<Integer> dmap = dmaps.get(v);
            int oriAttr = getOriAttr(ans.allvertices.get(v));
            HashSet<Integer> remove = new HashSet<Integer>();
            for (int u : dmap) {
                if (ground.getVertex(u).attr != oriAttr) {
                    remove.add(u);
                }
            }
            dmap.removeAll(remove);
            /**
             * Terminate at early stage
             */
            if (dmap.isEmpty()) {
                return res;
            }
        }
        ArrayList<Path> paths = ans.decompose();
        for (Path path : paths) {
            for (int i = 0; i < path.path.size() - 1; i++) {
                int v = path.path.get(i);
                int u = path.path.get(i + 1);
                if(ans.content_vertex.contains(u)){
                    continue;
                }
                HashSet<Integer> vdmap = dmaps.get(v);
                HashSet<Integer> udmap = dmaps.get(u);
                HashSet<Integer> satisfied = new HashSet<Integer>();
                for (int vd : vdmap) {
                    for (int ud : udmap) {
                        if (ground.contains(ud, vd)) {
                            satisfied.add(ud);
                            continue;
                        }
                    }
                }
                dmaps.put(u, satisfied);
            }
            path.setDroot(dmaps.get(path.root));
        }
        if(dmaps.get(ans.root).isEmpty())
            return new LinkedList<Integer>();
        res.addAll(dmaps.get(ans.root));
        return res;
    }

    public int getOriAttr(int genAttr) {
        int deAttr = genAttr;
        int layer = bigIndex.searchLayer;
        while (layer != 1) {
            deAttr = bigIndex.hierarchy_config.get(layer).get(deAttr);
            layer--;
        }
        return deAttr;
    }

    public HashSet<Integer> getDmaps(int vid, int layer) {
        HashSet<SVertex> dmaps = bigIndex.getGroundVertices(layer, vid);
        HashSet<Integer> res = new HashSet<Integer>();
        for (SVertex v : dmaps) {
            res.add(v.getID());
        }
        return res;
    }
}
