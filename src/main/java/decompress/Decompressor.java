package decompress;

import graph.AGraph;
import graph.SGraph;
import index.BigIndex;

import java.util.*;

/**
 * Created by samjjx on 2017/8/3.
 */
public class Decompressor {
    private Queue<AGraph> results;
    private BigIndex bigIndex;
    public int topk = 300;
    public int result_counter = 0;

    public Decompressor(Queue<AGraph> results, BigIndex bigIndex) {
        this.results = results;
        this.bigIndex = bigIndex;
    }

    public Queue<AGraph> getResults() {
        return results;
    }

    public void DecompressByLayerOri() {
        if (results.isEmpty())
            return;
        int layer = results.peek().layer;
        while (layer != 1) {
            DecompressByLayer();
            layer--;
        }
        Queue<AGraph> finalResult = new LinkedList<AGraph>();
        while (!results.isEmpty()) {
            AGraph result = results.remove();
            System.out.println(result);
            if (result.verification(bigIndex.hierarchy_keyword.get(1)) && simpleVerify(result, bigIndex.MulGraph.get(1))) {
//            if (result.verification(bigIndex.hierarchy_keyword.get(1))) {
                finalResult.add(result);
            }
        }
        results = finalResult;
    }

    public void DecompressByLayer() {
        Queue<AGraph> dResults = new LinkedList<AGraph>();
        while (!results.isEmpty()) {
            AGraph head = results.remove();
            dResults.addAll(DecompressOnePatternByLayer(head));
            if (result_counter >= topk)
                break;
        }
        results = dResults;
    }


    /**
     * This baseline method provide the comparison to the look ahead
     *
     * @param result
     * @param ori
     * @return
     */
    public boolean simpleVerify(AGraph result, SGraph ori) {
        for (int src : result.allvertices.keySet()) {
            for (int dst : result.edges.get(src)) {
                if (!ori.contains(src, dst))
                    return false;
            }
        }
        return true;
    }

    public Queue<AGraph> DecompressOnePatternByLayer(AGraph result) {
        int layer = result.layer - 1;
        System.out.println(layer);
        SGraph top = bigIndex.MulGraph.get(result.layer);
        SGraph lower = top.next;

        Queue<AGraph> all_results = new LinkedList<AGraph>();

        if (result.layer == 1) {
            if (result.verification(bigIndex.hierarchy_keyword.get(1)) && simpleVerify(result, bigIndex.MulGraph.get(1))) {
                all_results.add(result);
                this.result_counter++;
            }
            return all_results;
        }
        HashMap<Integer, ArrayList<Integer>> dmaps = new HashMap<Integer, ArrayList<Integer>>();
        for (int v : result.allvertices.keySet()) {
            dmaps.put(v, DecompressVertexByLayer(v, top));
        }
        Set<Integer> all_compressed = result.allvertices.keySet();

        int first = result.root;
        System.out.println(first);
        for (int df : dmaps.get(first)) {
            AGraph aGraph = new AGraph(layer, result.root);
            aGraph.expandVertex(df, lower.getVertex(df).attr, result, first);
            all_results.add(aGraph);
        }

        for (int compressed_vertex : all_compressed) {
            if (compressed_vertex == result.root)
                continue;
            Queue<AGraph> all_dec_results = new LinkedList<AGraph>();
            while (!all_results.isEmpty()) {
                AGraph head = all_results.remove();
                for (int decompressed_vertex : dmaps.get(compressed_vertex)) {
                    if (head.qualification(decompressed_vertex, result, compressed_vertex, lower)) {
                        AGraph nGraph = head.copy();
                        nGraph.expandVertex(decompressed_vertex, lower.getVertex(decompressed_vertex).attr, result, compressed_vertex);
                        all_dec_results.add(nGraph);
                    }
                }

            }
            all_results.addAll(all_dec_results);
        }
        return all_results;
    }

    public ArrayList<Integer> DecompressVertexByLayer(int vid, SGraph graph) {
        return graph.dmaps.get(vid);
    }

    /*
    TODO: for now, we only consider decompress the result layer by layer. We could still have some future optimization
    TASK 1: decompression between cross mul layer
    TASK 2: ...
     */
    public void DecompressMulLayer() {

    }

    public LinkedList<SGraph> DecompressOnePatternMulLayer(SGraph result) {
        return null;
    }
}
