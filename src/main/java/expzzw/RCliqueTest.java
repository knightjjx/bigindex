package expzzw;

import algorithms.Rclique;
import config.CompressFactor;
import graph.OntGraph;
import graph.SGraph;
import index.BigIndex;
import result.RcliqueResult;
import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by samjjx on 2017/8/24.
 */
public class RCliqueTest {
    static HashSet<Integer> keywords;
    static HashSet<String> keywords_context;

    static void generateNumKeywords(int number) {
        keywords = new HashSet<Integer>();
        for (int i = 0; i < number; i++) {
            keywords.add(i+2);
        }
    }
    static void testQuery(){
        keywords_context = new HashSet<String>();
        keywords_context.add("protein interaction");
        keywords_context.add("graph");
        keywords_context.add("algorithm");
        keywords_context.add("network");
        keywords_context.add("topology");
    }

    public static void main(String[] args) throws CloneNotSupportedException, IOException {
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/synt/synt5", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/synt/synt5.ont", true, 0, true);

//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/blink/blink", false, false);
//        String context_path = "/Users/samjjx/dev/git/kws_zzw/test.attr";

        SGraph graph = new SGraph(args[0], false, false);
        String context_path = args[1];
        int limit = Integer.parseInt(args[2]);

        HashMap<Integer, String> context_attributes = new HashMap<Integer, String>();
        IOPack ioPack = new IOPack();
        BufferedReader br = ioPack.getBufferInstance(context_path);
        String line;
        while((line = br.readLine())!=null){
            String[] data = line.split("\t");
            int id = Integer.parseInt(data[0]);
            String attribute = data[1];
            context_attributes.put(id, attribute);
        }

        System.out.println("Finish Loading all the datasets");
        Logger logger = Logger.getLogger("RClique");
        FileHandler fh = new FileHandler("rclique.log");
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        System.out.println("Original Graph");
//        System.out.println(graph);
        long t0 = System.currentTimeMillis();
        generateNumKeywords(2);

        Rclique rclique = new Rclique(keywords, graph);
        rclique.setLimit(limit);
        rclique.loadingContentAttribute(context_attributes);

        System.out.println("Rclique Index construction time on Original Graph: \t" + (System.currentTimeMillis() - t0));

        t0 = System.currentTimeMillis();
        testQuery();
        ArrayList<RcliqueResult> res = rclique.semantic(keywords_context, graph, true);
        logger.info("# of results in original graphs\t" + res.size());
        System.out.println("# of results in original graphs\t" + res.size());
        System.out.println("Rclique Query time on Original Graph: \t" + (System.currentTimeMillis() - t0));
        for(RcliqueResult res_tmp: res){
            System.out.println(res_tmp);
        }

        for(RcliqueResult res_tmp: res){
            Set<Integer> keyset = res_tmp.ans.allvertices.keySet();
            for(int id: keyset)
                System.out.print(id+"\t");
            System.out.println();
        }

    }
}

