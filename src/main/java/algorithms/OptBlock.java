package algorithms;

import algorithms.blinksIndex.LKNVertex;
import algorithms.blinksIndex.LPNVertex;
import algorithms.blinksIndex.MNKVertex;
import com.google.common.collect.HashBasedTable;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import java.util.*;

/**
 * Created by samjjx on 2018/3/20.
 */
public class OptBlock {
    public MutableGraph<Integer> graph;
    int bid;
    int num_edges_;

    public HashMap<Integer, ArrayList<LKNVertex>> LKN = new HashMap<Integer, ArrayList<LKNVertex>>(); // keyword => vertex list

    public HashMap<Integer, ArrayList<LPNVertex>> LPN = new HashMap<Integer, ArrayList<LPNVertex>>(); // portal -> vertex sets

    public HashBasedTable<Integer, Integer, MNKVertex> MNK = HashBasedTable.create(); // v -> keyword -> MNKVertex

    public HashSet<Integer> in_portal;
    public HashSet<Integer> out_portal;

    public HashMap<Integer, Integer> DNP;

    public HashSet<Integer> keywords;

    public HashMap<Integer, Integer> global_info_vertices;

    public HashMap<Integer, HashSet<Integer>> keyword2vertex;

    public long index_size = 0;

    public OptBlock(int bid) {
        this.bid = bid;
        graph = GraphBuilder.directed().allowsSelfLoops(false).build();
        in_portal = new HashSet<Integer>();
        out_portal = new HashSet<Integer>();
        DNP = new HashMap<Integer, Integer>();
        keywords = new HashSet<Integer>();
        keyword2vertex = new HashMap<Integer, HashSet<Integer>>();
    }

    public Set<Integer> getNode() {
        return graph.nodes();
    }

    public void setGlobal_info_vertices(HashMap<Integer, Integer> global_vertices) {
        this.global_info_vertices = global_vertices;
    }

    public void addNode(int v) {
        graph.addNode(v);
    }

    public void addEdge(int src, int dst) {
        graph.putEdge(src, dst);
        num_edges_++;
    }

    public void addKeyword(int k) {
        this.keywords.add(k);
    }

    public boolean containsKeyword(int k) {
        return keywords.contains(k);
    }

    public void addInvertKeyword(int keyword, int vid) {
        if (!keyword2vertex.containsKey(keyword)) {
            keyword2vertex.put(keyword, new HashSet<Integer>());
        }
        keyword2vertex.get(keyword).add(vid);
    }

    public void addInPortal(int v) {
        in_portal.add(v);
    }

    public void addOutPortal(int v) {
        out_portal.add(v);
    }

    public boolean hasNode(int v) {
        return graph.addNode(v);
    }

    public int numEdge() {
        return num_edges_;
    }

    public int numNode() {
        return graph.nodes().size();
    }


    /**
     *
     *
     *
     *
     *
     *
     *
     *
     */

    public void constructIndex() {
        System.out.println("Construct compute LKN");
        buildLKN();
        System.out.println("Finish compute LKN");
        buildMNK();
        System.out.println("Finish compute MNK");
        buildLPN();
        System.out.println("Finish compute LPN");
        buildDNP();
        System.out.println("Finish compute DNP");

        for(int key: LKN.keySet()){
            index_size += LKN.get(key).size();
        }

//        index_size += MNK.size();

//        for(int p : out_portal){
//            index_size += LPN.get(p).size();
//        }

        index_size += DNP.size();
    }

    public long getIndexSize(){
        return index_size;
    }

    public void buildLKN() {
        // Loop on the keywords
        for (int w : keywords) {

            ArrayList<LKNVertex> lkn = new ArrayList<LKNVertex>();
            Queue<LKNVertex> queue = new LinkedList<LKNVertex>();
            HashSet<Integer> visited = new HashSet<Integer>();

            // Pack the origin
            HashSet<Integer> origin = keyword2vertex.get(w);
            for (int ori : origin) {
                queue.add(new LKNVertex(0, ori, -1, ori));
            }

            while (!queue.isEmpty()) {
                LKNVertex head = queue.remove();
                if (visited.contains(head.getNode()))
                    continue;
                lkn.add(head);
                visited.add(head.getNode());

                for (int parent : graph.predecessors(head.getNode())) {
                    if (visited.contains(parent))
                        continue;
                    queue.add(new LKNVertex(head.getDist() + 1, parent, head.getNode(), head.getKnode()));
                }
            }
            LKN.put(w, lkn);
        }
    }

    public void buildMNK() {
        // Loop on the keywords
        for (int w : LKN.keySet()) {
            ArrayList<LKNVertex> lkn = LKN.get(w);
            for (LKNVertex vlkn : lkn) {
                int v = vlkn.getNode();
                MNK.put(v, w, vlkn.createMNK());
            }
        }
    }

    public void buildLPN() {
        for (int p : out_portal) {
            ArrayList<LPNVertex> lpn = new ArrayList<LPNVertex>();
            Queue<LPNVertex> queue = new LinkedList<LPNVertex>();
            HashSet<Integer> visited = new HashSet<Integer>();
            queue.add(new LPNVertex(0, p, -1));
            while (!queue.isEmpty()) {
                LPNVertex head = queue.remove();
                if (visited.contains(head.getNode()))
                    continue;
                lpn.add(head);
                visited.add(head.getNode());
                for (int parent : graph.predecessors(head.getNode())) {
                    if (visited.contains(parent))
                        continue;
                    queue.add(new LPNVertex(head.getDist() + 1, parent, head.getNode()));
                }

            }
            LPN.put(p, lpn);
        }
    }

    public void buildDNP() {
        HashMap<Integer, Integer> nearest_portal = new HashMap<Integer, Integer>();
        for (int u : graph.nodes()) {
            DNP.put(u, Integer.MAX_VALUE);
            nearest_portal.put(u, Integer.MAX_VALUE);
        }

        // Loop on out portal
        for (int p : LPN.keySet()) {
            ArrayList<LPNVertex> lpn = LPN.get(p);

            // Loop on lpn
            for (LPNVertex lpnVertex : lpn) {
                int u = lpnVertex.getNode();
                int newdist = lpnVertex.getDist();
                if (newdist < DNP.get(u)) {
                    DNP.put(u, newdist);
                    nearest_portal.put(u, p);
                }
            }
        }
    }

    public ArrayList<LKNVertex> getLKN(int w) {
        return LKN.get(w);
    }

    public ArrayList<LPNVertex> getLPN(int portal) {
        return LPN.get(portal);
    }

    /**
     * @param u a vertex
     * @return The distance between the vertex and its nearest portal
     */
    public int getDNP(int u) {
        return DNP.get(u);
    }

    /**
     * @param u: the vertex uid
     * @param w: the keyword w
     * @return the distance of u to w in the block b
     */
    public int getMNK(int u, int w) {
        if (!MNK.contains(u, w)) {
            return Integer.MAX_VALUE;
        }
        return MNK.get(u, w).getDist();
    }

    /**
     *
     *
     *
     *
     *
     *
     *
     *
     */
    public void showInfo() {
        System.out.println("=========================\tBlock\t" + bid + "\t=========================");
        System.out.println("# of vertex\t" + numNode());
        System.out.println("# of edge\t" + numEdge());
        System.out.println("# of out-portal\t" + out_portal.size());
        System.out.println("# of keywords\t" + keywords.size());

//        showMidPriInfo();
        showHighPriInfor();
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public void showHighPriInfor() {
        System.out.println("++++++================+++++++++");
        System.out.println("LPN INFO");
        int count = 0;
        for (int p : out_portal) {
            count += LPN.get(p).size();
        }
        System.out.println("LPN Size\t" + count);

        System.out.println("++++++================+++++++++");
        System.out.println("DNP INFO");
        System.out.println("DNP Size\t" + DNP.size());

        System.out.println("++++++================+++++++++");
        System.out.println("LKN INFO");
        count = 0;
        for (int w : LKN.keySet()) {
            count += LKN.get(w).size();
        }
        System.out.println("LKN Size\t" + count);

        System.out.println("++++++================+++++++++");
        System.out.println("MNK INFO");
    }

    public void showMidPriInfo() {
        System.out.println("++++++================+++++++++");
        System.out.println("LPN INFO");
        for (int p : out_portal) {
            System.out.println("p\t" + p + "\t" + LPN.get(p));
        }

        System.out.println("++++++================+++++++++");
        System.out.println("DNP INFO");
        for (int p : DNP.keySet()) {
            System.out.println(p + "\t" + DNP.get(p));
        }

        System.out.println("++++++================+++++++++");
        System.out.println("LKN INFO");
        for (int w : LKN.keySet()) {
            System.out.println(w + "\t" + LKN.get(w));
        }

        System.out.println("++++++================+++++++++");
        System.out.println("MNK INFO");
        for (int row : MNK.rowKeySet()) {
            for (int col : MNK.columnKeySet()) {
                if (MNK.contains(row, col))
                    System.out.println("vertex\t" + row + "\tkeyword\t" + col + "\tvalue\t" + MNK.get(row, col));
            }
        }
    }

}

