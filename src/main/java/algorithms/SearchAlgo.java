package algorithms;

import graph.SGraph;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by samjjx on 2017/7/14.
 */
public interface SearchAlgo<R> {
    ArrayList<R> semantic(HashSet<Integer> keywords, SGraph graph);
}
