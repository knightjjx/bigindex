package algorithms;

import graph.SVertex;

import java.util.Iterator;

/**
 * Created by samjjx on 2017/7/19.
 */
public class Cursor {
    public Iterator<SVertex> cursor;
    public int distance;
    public int pID;
    public int keyword;

    public Cursor(Iterator<SVertex> cursor, int distance, int b, int keyword){
        this.cursor = cursor;
        this.distance = distance;
        this.pID = b;
        this.keyword = keyword;
    }

    public Iterator<SVertex> getCursor(){
        return cursor;
    }

    public int getDist(){
        return distance;
    }

    public int peekDist(){
        if(!cursor.hasNext()){
            return Integer.MAX_VALUE;
        }

        return 0;
    }
}
