package algorithms.CC;

import com.google.common.graph.MutableGraph;

import java.util.*;

public class CCComputation {
    private MutableGraph<Integer> graph;
    private HashSet<Integer> visited;
    private HashMap<Integer, HashSet<Integer>> cc;

    public CCComputation(MutableGraph<Integer> graph) {
        this.graph = graph;
        cc = new HashMap<Integer, HashSet<Integer>>();
    }

    public void computeCC() {
        Set<Integer> nodes = graph.nodes();
        visited = new HashSet<Integer>();
        for (int n : nodes) {
            if (visited.contains(n))
                continue;
            HashSet<Integer> component = new HashSet<Integer>();
            Queue<Integer> q = new LinkedList<Integer>();
            q.add(n);
            while (!q.isEmpty()) {
                int head = q.remove();
                if (component.contains(head) || visited.contains(head))
                    continue;
                component.add(head);
                visited.add(head);

                for (int p : graph.adjacentNodes(head)) {
                    if (component.contains(p) || visited.contains(p))
                        continue;
                    q.add(p);
                }
            }
            cc.put(n, component);
        }
    }

    public void computePortal(HashSet<Integer> portal_set, MutableGraph<Integer> graph_complete){
        for(int key: cc.keySet()){
            if(cc.get(key).size() > 100){
                computeOnePoral(key, portal_set,graph_complete);
            }
        }
    }

    public HashMap<Integer, HashSet<Integer>> cc_to_portal = new HashMap<Integer, HashSet<Integer>>();
    public void computeOnePoral(int root, HashSet<Integer> portal_set,MutableGraph<Integer> graph_complete){
        HashSet<Integer> cc = this.cc.get(root);

        HashSet<Integer> portal = new HashSet<Integer>();
        for(int v:cc){
            for(int u: graph_complete.adjacentNodes(v)){
                if(portal.contains(u))
                    continue;
                if(portal_set.contains(u))
                    portal.add(u);
            }
        }

        cc_to_portal.put(root, portal);
    }

    public void printBasic() {
        System.out.println("Basic INFO");
        System.out.println(" # of CC\t" + cc.keySet().size());
        System.out.println("===============Print Each CC===============");
        System.out.println("CCID \t SIZE\t Portal Size");
        for(int key: cc.keySet()){
            if(cc.get(key).size() > 100)
                System.out.println(key+"\t" +cc.get(key).size()+"\t"+cc_to_portal.get(key).size());
        }
        System.out.println();
    }
}
