package algorithms.CC;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;
import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public class CC {
    public static void main(String[] args) throws IOException {
        String graph_path = args[0];
        String nodecut_path = args[1];

        boolean remove_cutnode = Boolean.parseBoolean(args[2]);

        IOPack ioPack = new IOPack();

        MutableGraph<Integer> graph = GraphBuilder.undirected().allowsSelfLoops(false).build();

        MutableGraph<Integer> graph_complete = GraphBuilder.undirected().allowsSelfLoops(false).build();

        HashSet<Integer> cut_node = new HashSet<Integer>();

        BufferedReader bufferedReader = ioPack.getBufferInstance(nodecut_path);
        String line;


        if (remove_cutnode) {
            while ((line = bufferedReader.readLine()) != null) {
                cut_node.add(Integer.parseInt(line));
            }
        }

        BufferedReader gReader = ioPack.getBufferInstance(graph_path);
        int count = 0;
        while ((line = gReader.readLine()) != null) {
            String[] data = line.split("\t");
            int src = Integer.parseInt(data[0]);
            int dst = Integer.parseInt(data[1]);
            if (src == dst)
                continue;

            graph_complete.putEdge(src,dst);

            if (cut_node.contains(src) || cut_node.contains(dst))
                continue;

            graph.putEdge(src, dst);
        }
        System.out.println("Finish Loading");
        CCComputation cc = new CCComputation(graph);
        cc.computeCC();
        System.out.println("CCID \t Portal Size");
        cc.computePortal(cut_node, graph_complete);
        cc.printBasic();
    }
}
