package algorithms;

import config.Pattern;
import config.Query;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by samjjx on 2018/3/20.
 */
public class TestOptBlinks {
    public static void main(String[] args) throws IOException {
        if (args.length < 4) {
            System.out.println("[Error]+Wrong parameters");
            System.out.println("=================Running Example==============");
            System.out.println("java -Xmx304800m -XX:-UseGCOverheadLimit -jar BigIndex-1.0-SNAPSHOT-jar-with-dependencies.jar /tmp/jxjian/index_gen/dbpedia3 /tmp/jxjian/query/vary_query_size/ optblinks-example.log -1");
            System.out.println("==============================================");
            System.exit(-1);
        }
        OptBlinks blinks = new OptBlinks(args[0]);
        String query_path = args[1];


        /** Init the logger **/
        String log_path = args[2];

        int threshold = Integer.parseInt(args[3]);

        if(threshold == -1)
            threshold = 10;

        Logger logger = Logger.getLogger("OptBlinks");
        FileHandler fh = new FileHandler(log_path);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);


        Query query = new Query(query_path);

        // Init the searching procedures
        OptSearchBlinks osb = new OptSearchBlinks(blinks, 100);

        logger.info("Index Size\t" + blinks.getIndexSize());

        logger.info("Out_portal Size\t" + blinks.LPB.keySet().size());

        for (Pattern ptn : query.batchQuery) {
//            try {
                logger.info("===============================================\n");
                logger.info("Pattern\t" + ptn.path);
                long t0 = System.currentTimeMillis();
                osb.searchBlinks(ptn.keywords, threshold);
                logger.info("Threshold\t"+threshold);
                logger.info("Searching time\t" + (System.currentTimeMillis() - t0));

                t0 = System.currentTimeMillis();
                osb.searchBlinks(ptn.keywords, threshold+10);
                logger.info("Threshold\t"+(threshold + 10));
                logger.info("Searching time\t" + (System.currentTimeMillis() - t0));

//            } catch (java.lang.NullPointerException e) {
//                logger.info(e.getStackTrace().toString());
//            }
        }


        /**
         HashSet<Integer> keywords = new HashSet<Integer>();
         keywords.add(64);
         keywords.add(113);
         keywords.add(54);

         //        OptBlinks blinks = new OptBlinks("/Users/samjjx/dev/git/grape-dev/grape/dataset/p2p-31/p2p-31");
         HashSet<Integer> keywords1 = new HashSet<Integer>();
         //        OptBlinks blinks = new OptBlinks("/Users/samjjx/dev/git/bigindex/vldb2014_1/graph");
         keywords.add(4);
         keywords.add(5);
         keywords.add(0);

         keywords1.add(4);
         keywords1.add(5);


         long t0 = System.currentTimeMillis();
         for (int i = 5; i < 11; i++)
         osb.searchBlinks(keywords, i);
         for (int i = 5; i < 11; i++)
         osb.searchBlinks(keywords1, i);


         System.out.println("Searching time\t" + (System.currentTimeMillis() - t0));
         **/

    }
}
