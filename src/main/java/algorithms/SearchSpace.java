package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class SearchSpace {
    HashMap<Integer, HashSet<Integer>> sp; // keywords -> vertices
    HashSet<Integer> query_keywords; //keywords

    HashMap<String, HashSet<Integer>> sp_context; // keywords -> vertices
    HashSet<String> query_keywords_context; //keywords

    /**
     * @param keywords: Query keywords
     * @param inverted: inverted index
     */
    public SearchSpace(HashSet<Integer> keywords, HashMap<Integer, HashSet<Integer>> inverted) {
        sp = new HashMap<Integer, HashSet<Integer>>();
        for (int keyword : keywords) {
            HashSet<Integer> tmp = new HashSet<Integer>();
            tmp.addAll(inverted.get(keyword));
            sp.put(keyword, tmp);
        }
        query_keywords = keywords;
    }

    public SearchSpace(HashSet<String> keywrods, HashMap<Integer, String> attributes, boolean context) {
        sp_context = new HashMap<String, HashSet<Integer>>();

        for (String keyword : keywrods) {
            HashSet<Integer> tmp = new HashSet<Integer>();
            sp_context.put(keyword, tmp);
        }

        Set<Integer> ids = attributes.keySet();
        for (int id : ids) {
            String ctx = attributes.get(id);
            for (String keyword : keywrods) {
                if (ctx.toUpperCase().contains(keyword.toUpperCase()))
                    sp_context.get(keyword).add(id);
            }
        }
        query_keywords_context = keywrods;
    }


    public SearchSpace(HashSet<String> keywrods, boolean context, HashMap<String, HashSet<Integer>> ssp){
        query_keywords_context = keywrods;
        sp_context = ssp;
    }

    public SearchSpace(HashMap<Integer, HashSet<Integer>> sp, HashSet<Integer> keywords) {
        this.sp = sp;
        this.query_keywords = keywords;
    }

    public ArrayList<SearchSpace> getSubSpace(HashSet<Integer> S0) {
        ArrayList<SearchSpace> subSpace = new ArrayList<SearchSpace>();
        l:
        for (int vid : S0) {
            //Generate a subspace
            HashMap<Integer, HashSet<Integer>> ssp = new HashMap<Integer, HashSet<Integer>>();

            for (int keyword : sp.keySet()) {
                HashSet<Integer> dimension = sp.get(keyword);
                if (dimension.contains(vid)) {
                    HashSet<Integer> newdimension = new HashSet<Integer>();
                    newdimension.addAll(dimension);
                    newdimension.remove(vid);
                    if (newdimension.size() == 0)
                        continue l;
                    for (int key : sp.keySet()) {
                        if (key == keyword) {
                            ssp.put(key, newdimension);
                        } else {
                            ssp.put(key, new HashSet<Integer>());
                            ssp.get(key).addAll(this.sp.get(key));
                        }
                    }
                }
            }
            SearchSpace ASapce = new SearchSpace(this.query_keywords, ssp);
            subSpace.add(ASapce);
        }
        return subSpace;
    }


    public ArrayList<SearchSpace> getContextSubSpace(HashSet<Integer> S0) {
        ArrayList<SearchSpace> subSpace = new ArrayList<SearchSpace>();
        l:
        for (int vid : S0) {
            //Generate a subspace
            HashMap<String, HashSet<Integer>> ssp = new HashMap<String, HashSet<Integer>>();

            for (String keyword : sp_context.keySet()) {
                HashSet<Integer> dimension = sp_context.get(keyword);
                if (dimension.contains(vid)) {
                    HashSet<Integer> newdimension = new HashSet<Integer>();
                    newdimension.addAll(dimension);
                    newdimension.remove(vid);
                    if (newdimension.size() == 0)
                        continue l;
                    for (String key : sp_context.keySet()) {
                        if (key.toUpperCase().contains(keyword.toUpperCase())) {
                            ssp.put(key, newdimension);
                        } else {
                            ssp.put(key, new HashSet<Integer>());
                            ssp.get(key).addAll(this.sp_context.get(key));
                        }
                    }
                }
            }
            SearchSpace ASapce = new SearchSpace(this.query_keywords_context, true, ssp);

            subSpace.add(ASapce);
        }
        return subSpace;
    }

    public HashSet<Integer> getDimension(int keyword) {
        return sp.get(keyword);
    }

    public HashSet<Integer> getContextDimension(String keyword) {
        return sp_context.get(keyword);
    }
}