package algorithms.blinksIndex;

/**
 * Created by samjjx on 2018/3/20.
 */
public class LKNVertex {
    int dist_;
    int node_;
    int first_;
    int knode_;

    public LKNVertex(int dist, int node, int first, int knode) {
        this.dist_ = dist;
        this.node_ = node;
        this.first_ = first;
        this.knode_ = knode;
    }

    public int getDist() {
        return dist_;
    }

    public int getNode() {
        return node_;
    }

    public int getFirst() {
        return first_;
    }

    public int getKnode() {
        return knode_;
    }

    public MNKVertex createMNK(){
        return new MNKVertex(dist_, node_, first_, knode_);
    }

    public String toString() {
        String res = "";
        res = res + dist_ + " " + node_ + " " + first_ + " " + knode_;
        return res;
    }
}
