package algorithms.blinksIndex;

public class LPNVertex{
    int dist_;
    int node_;
    int first_;

    public LPNVertex(int dist, int node, int first){
        this.dist_ = dist;
        this.node_ = node;
        this.first_ = first;
    }

    public int getDist(){
        return dist_;
    }

    public int getNode(){
        return node_;
    }

    public int getFirst(){
        return first_;
    }

    public String toString(){
        String res = "";
        res = res + dist_ + " " + node_ + " " + first_;
        return res;
    }
}