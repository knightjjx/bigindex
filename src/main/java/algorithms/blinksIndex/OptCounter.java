package algorithms.blinksIndex;

public class OptCounter implements Comparable<OptCounter> {
    int keyword;
    int counter;

    public OptCounter(int keyword) {
        this.keyword = keyword;
        this.counter = 0;
    }

    public int compareTo(OptCounter other) {
        return this.counter - other.counter;
    }

    public int getKeyword(){
        return keyword;
    }

    public int getCounter(){
        return counter;
    }

    public void addOne(){
        counter++;
    }
}