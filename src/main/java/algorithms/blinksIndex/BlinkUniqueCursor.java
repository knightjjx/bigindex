package algorithms.blinksIndex;

/**
 * Created by samjjx on 2018/3/27.
 */
public class BlinkUniqueCursor {
    public int u_;
    public int distance_;
    public BlinkUniqueCursor(int u, int distance){
        this.u_ = u;
        this.distance_ = distance;
    }

    public int getNode(){
        return u_;
    }

    public int getDist(){
        return distance_;
    }
}
