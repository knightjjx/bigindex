package algorithms.blinksIndex;

import java.util.PriorityQueue;

/**
 * Created by samjjx on 2018/3/27.
 */
public class QueueCursor {
    public int keyword_;

    // Each cursor records the vertices in the same block could reach the same keyword
    public PriorityQueue<OptCursor> cursor;


    public QueueCursor(int keyword) {
        this.keyword_ = keyword;
        cursor = new PriorityQueue<OptCursor>();
    }

    public void addCursor(OptCursor opc) {
        this.cursor.add(opc);
    }

    public OptCursor getCursor() {
        OptCursor tmp = cursor.remove();
        if (tmp.peekDist() != Integer.MAX_VALUE)
            cursor.add(tmp);
        return tmp;
    }

    public boolean isEmpty() {
        return cursor.isEmpty();
    }

    public int peekDist() {
        if (cursor.isEmpty())
            return Integer.MAX_VALUE;
        return cursor.peek().peekDist();
    }
}
