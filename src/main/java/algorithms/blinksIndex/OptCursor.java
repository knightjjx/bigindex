package algorithms.blinksIndex;


import java.util.ArrayList;

/**
 * Created by samjjx on 2017/7/19.
 */
public class OptCursor implements Comparable<OptCursor> {
    public ArrayList<LKNVertex> LKNCursor;
    public ArrayList<LPNVertex> LPNCursor;
    public int keyword;

    public int cursor_type_ = 0;
    public int top;

    public int distance_;

    public OptCursor(ArrayList<LKNVertex> cursor, int keyword, int distance) {
        this.LKNCursor = cursor;
        this.keyword = keyword;
        top = 0;
        cursor_type_ = 0;
        this.distance_ = distance;
    }

    public OptCursor(ArrayList<LPNVertex> cursor, int distance) {
        this.cursor_type_ = 1;
        this.LPNCursor = cursor;
        this.distance_ = distance;
        top = 0;
    }

    public BlinkUniqueCursor next() {
        if (cursor_type_ == 0) {
            LKNVertex lkn = LKNCursor.get(top);
            top++;
            return new BlinkUniqueCursor(lkn.getNode(), lkn.getDist());
        } else {
            LPNVertex lpn = LPNCursor.get(top);
            top++;
            return new BlinkUniqueCursor(lpn.getNode(), lpn.getDist());
        }
    }

    public int peekDist() {
        if (cursor_type_ == 0) {
            if (top == LKNCursor.size() - 1) {
                return Integer.MAX_VALUE;
            }
            return LKNCursor.get(top).getDist();
        } else {
            if (top == LPNCursor.size() - 1) {
                return Integer.MAX_VALUE;
            }
            return LPNCursor.get(top).getDist();
        }
    }

    public int get_extra_distance(){
        return distance_;
    }

    public int compareTo(OptCursor o) {
        return  o.peekDist() - this.peekDist();
    }
}
