package algorithms.blinksIndex;

import java.util.HashMap;

/**
 * Created by samjjx on 2018/3/27.
 */
public class BlinkResult {

    HashMap<Integer, Integer> R;   // w -> distance
    int u_;

    int num_keyword_;

    public BlinkResult(int u, int numKeyword){
        this.u_ = u;
        R = new HashMap<Integer, Integer>();
        this.num_keyword_ = numKeyword;
    }

    /**
     * The distance of current answer. Return Max value when some keywords are not found
     * @return the sum of ths distance between the vertex and the keywords
     */
    public int sumDist(){
        if(R.size() != num_keyword_)
            return Integer.MAX_VALUE;
        if(u_ == 9){
            System.out.println();
        }
        int sum =0;
        for(int w: R.keySet()){
            sum += R.get(w);
        }
        return sum;
    }

    /**
     * Find one more keyword
     * @param w
     * @param distance
     */
    public void addKeyword(int w, int distance){
        if(R.containsKey(w))
            return;
        R.put(w, distance);
    }

    public boolean hasKeyword(int w){
        return R.containsKey(w);
    }

    public String toString(){
        String res = "root "+u_;
        return res;
    }
}
