package algorithms;

import algorithms.blinksIndex.LKNVertex;
import algorithms.blinksIndex.LPNVertex;
import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by samjjx on 2018/3/20.
 */
public class OptBlinks {

    public HashMap<Integer, Integer> vertices;

    // LKB records the lists of blocks contains w. keyword -> blocks set
    public HashMap<Integer, HashSet<Integer>> LKB;

    // LPB records the lists of blocks take p as out-portal. portal -> blocks set
    public HashMap<Integer, HashSet<Integer>> LPB;

    public HashMap<Integer, OptBlock> blocks;

    public HashMap<Integer, Integer> rTable;

    public long index_size;

    public OptBlinks(String pathBase) throws IOException {
        LKB = new HashMap<Integer, HashSet<Integer>>();
        LPB = new HashMap<Integer, HashSet<Integer>>();
        vertices = new HashMap<Integer, Integer>();
        blocks = new HashMap<Integer, OptBlock>();
        rTable = new HashMap<Integer, Integer>();
        index_size = 0;
        loadFromDisk(pathBase);
    }

    public void loadFromDisk(String pathBase) throws IOException {
        String rPath = pathBase + ".r";
        String vPath = pathBase + ".v";
        String ePath = pathBase + ".e";
        // Loading
        loadRTable(rPath);
        loadVertex(vPath);
        loadEdge(ePath);
        System.out.println("Finish Loading");
        setGlobal();

        computeAttribute();
        System.out.println("Finish compute Attributes");
        constructIndex();
        System.out.println("Finish compute Index");
        showImportantInfo();
//        showIndex();
    }

    public long getIndexSize(){
        index_size = 0;
        for (int bid : blocks.keySet()) {
            OptBlock block = blocks.get(bid);
            index_size += block.getIndexSize();
        }

        for(int key: LPB.keySet()){
            index_size += LPB.get(key).size();
        }

        for(int key: LKB.keySet()){
            index_size += LKB.get(key).size();
        }

        return index_size;
    }

    public void setGlobal() {
        for (int bid : blocks.keySet()) {
            OptBlock block = blocks.get(bid);
            block.setGlobal_info_vertices(this.vertices);
        }
    }

    public void computeAttribute() {
        for (int bid : blocks.keySet()) {
            OptBlock block = blocks.get(bid);
            for (int v : block.getNode()) {
                int keyword = vertices.get(v);
                if (!block.containsKeyword(keyword))
                    block.addKeyword(keyword);
                block.addInvertKeyword(keyword, v);
            }
        }
    }

    public void constructIndex() {
        buildLKB();
        buildLPB();
        System.out.println("Finish LKB and LPB");
        for (int bid : blocks.keySet()) {
            OptBlock block = blocks.get(bid);
            block.constructIndex();
            System.out.println("Finish compute block\t" + bid);
        }
    }


    public void buildLKB() {
        // Loop all the partition
        for (int bid : blocks.keySet()) {
            OptBlock block = blocks.get(bid);

            // Loop on the vertices set
            for (int v : block.getNode()) {
                int attr = vertices.get(v);
                if (!LKB.containsKey(attr)) {
                    LKB.put(attr, new HashSet<Integer>());
                }
                LKB.get(attr).add(bid);
            }
        }
    }

    public void buildLPB() {
        // Loop all the partition
        for (int bid : blocks.keySet()) {
            OptBlock block = blocks.get(bid);

            // Loop on the out portal set
            for (int p : block.out_portal) {
                if (!LPB.containsKey(p)) {
                    LPB.put(p, new HashSet<Integer>());
                }
                LPB.get(p).add(bid);
            }
        }
    }

    public void showImportantInfo() {
        for (int bid : blocks.keySet()) {
            blocks.get(bid).showInfo();
        }
    }

    public void showIndex() {
        System.out.println("===========Show LKB=========");
        for (int attr : LKB.keySet()) {
            System.out.println(attr + "\t" + LKB.get(attr));
        }

        System.out.println("===========Show LPB=========");
        for (int p : LPB.keySet()) {
            System.out.println(p + "\t" + LPB.get(p));
        }
    }

    /**
     * @param b: the id of the block
     * @param w: the keyword
     * @return the list which can achieve w within b. ascending distance
     */
    public ArrayList<LKNVertex> getLKN(int b, int w) {
        return blocks.get(b).getLKN(w);
    }

    /**
     * @param b: the block
     * @param u: the vertex uid
     * @param w: the keyword w
     * @return the distance of u to w in the block b
     */
    public int getMNK(OptBlock b, int u, int w) {
        return b.getMNK(u, w);
    }

    /**
     * @param b: the block
     * @param p: the out node
     * @return: the nodes which could achieve p within block b. ascending order
     */
    public ArrayList<LPNVertex> getLPN(int b, int p) {
        return blocks.get(b).getLPN(p);
    }

    /**
     * @param b the block
     * @param u a vertex u
     * @return the shortest distance between u and the nearest portal
     */
    public int getDNP(OptBlock b, int u) {
        return b.getDNP(u);
    }

    /**
     * @param w the keyword
     * @return the blocks containing the keyword w
     */
    public HashSet<Integer> getLKB(int w) {
        return LKB.get(w);
    }

    /**
     * @param portal A portal
     * @return The blocks taking the portal as out-portal
     */
    public HashSet<Integer> getLPB(int portal) {
        return LPB.get(portal);
    }

    /**
     * @param u A vertex
     * @return The block which u belongs
     */
    public OptBlock getVertexBlock(int u) {
        return blocks.get(rTable.get(u));
    }


    public boolean isPortal(int u) {
        return LPB.containsKey(u);
    }

    public void loadRTable(String path) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        rTable = new HashMap<Integer, Integer>();
        while ((line = reader.readLine()) != null) {
            String[] data = line.split("\t");
            int vid = Integer.parseInt(data[0]);
            int bid = Integer.parseInt(data[1]);
            if (!blocks.keySet().contains(bid)) {
                OptBlock block = new OptBlock(bid);
                blocks.put(bid, block);
            }
            rTable.put(vid, bid);
        }
    }

    public boolean loadVertex(String path) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        int count = 0;
        while ((line = reader.readLine()) != null) {
            count++;
            String[] data = line.split("\t");
            int vid = Integer.parseInt(data[0]);
            int attr = Integer.parseInt(data[1]);
            int bid = rTable.get(vid);
            vertices.put(vid, attr);
            blocks.get(bid).addNode(vid);
            if (count++ % 10000 == 0) {
                System.out.println("[Vertex Loading]\t" + count);
            }
        }
        return true;
    }

    public boolean loadEdge(String path) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        int count = 0;
        while ((line = reader.readLine()) != null) {
            count++;
            String[] data = line.split("\t");
            int src = Integer.parseInt(data[0]);
            int dst = Integer.parseInt(data[1]);
            assignEdge(src, dst);
        }
        return true;
    }

    public void assignEdge(int src, int dst) {
        if (src == dst)
            return;
        int s_bid = rTable.get(src);
        int d_bid = rTable.get(dst);
        if (s_bid == d_bid) {
            blocks.get(s_bid).addEdge(src, dst);
            return;
        }

        if (s_bid != d_bid) {
            /** With in edges
             blocks.get(s_bid).addOutPortal(src);
             blocks.get(d_bid).addInPortal(src);
             blocks.get(d_bid).addEdge(src, dst);
             **/
            /** With out edges **/
            blocks.get(s_bid).addOutPortal(dst);
            blocks.get(d_bid).addInPortal(dst);
            blocks.get(s_bid).addEdge(src, dst);

            return;
        }
    }
}
