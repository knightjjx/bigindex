package algorithms;

import graph.*;
import result.BlinksResult;
import util.VertexCounter;

import java.util.*;

/**
 * Created by samjjx on 2017/5/6.
 * SIMGMOD 2007: BLINKS: Ranked Keyword Searches on Graphs
 */
public class Blinks implements SearchAlgo<BlinksResult> {
    HashMap<Integer, Partition> partitions = new HashMap<Integer, Partition>();
    HashMap<Integer, HashSet<Integer>> LKB = new HashMap<Integer, HashSet<Integer>>();
    HashMap<Integer, HashSet<Integer>> LPB = new HashMap<Integer, HashSet<Integer>>(); // u(out vertices) -> block
    public SGraph graph;
    public ArrayList<BlinksResult> Answers;
    int numBlocks, numKeywords;
    public HashSet<SVertex> out_vertices;

    int threshold;

    int topk = 50;

    int layer;

    VertexCounter vc = new VertexCounter();

    HashSet<Integer> keywords;
    HashMap<Integer, BlinksResult> results = new HashMap<Integer, BlinksResult>();

    HashMap<Integer, HashMap<Integer, Boolean>> crossed = new HashMap<Integer, HashMap<Integer, Boolean>>(); // keyword -> (portal -> boolean)

    PriorityQueue<Counter> counters;

    // founded root answers
    HashSet<Integer> founded_root;

    public Blinks(int numBlocks, SGraph graph, int threshold) {
        this.numBlocks = numBlocks;
        this.threshold = threshold;
        this.Answers = new ArrayList<BlinksResult>();
        founded_root = new HashSet<Integer>();
        for (int i = 0; i < numBlocks; i++) {
            partitions.put(i+1, new Partition(i+1, graph));
        }
        PartitionConverter converter = new PartitionConverter(partitions);
        partitions = converter.convert(graph);

        out_vertices = new HashSet<SVertex>();
        for (int i = 0; i < numBlocks; i++) {
            out_vertices.addAll(partitions.get(i+1).out);
        }
        this.counters = new PriorityQueue<Counter>();
        ConsIndex(graph);
        this.layer = graph.layer;
        this.graph = graph;
    }

    public void parInfo() {
        for (int pid : partitions.keySet()) {
            System.out.println(partitions.get(pid));
        }
    }

    public ArrayList<BlinksResult> semantic(HashSet<Integer> keywords, SGraph graph) {
        this.keywords = keywords;
        this.numKeywords = keywords.size();

        counters = new PriorityQueue<Counter>();
        for (int keyword : keywords) {
            counters.add(new Counter(keyword));
        }

        HashMap<Integer, Queue<Cursor>> Q = new HashMap<Integer, Queue<Cursor>>();
        for (int keyword : keywords) {
            HashMap<Integer, Boolean> cross = new HashMap<Integer, Boolean>();

            for (SVertex v : this.out_vertices) {
                cross.put(v.id, false);
            }
            crossed.put(keyword, cross);
        }

        //Init cursors in different blocks
        for (int keyword : keywords) {
            Q.put(keyword, new LinkedList<Cursor>());
//            System.out.println("Keyword:"+ keyword);
            if(!LKB.containsKey(keyword)){
                return this.Answers;
            }
            for (int b : LKB.get(keyword)) {
                System.out.println(b);
                System.out.println(partitions.get(b));
                Q.get(keyword).add(partitions.get(b).getCursor(keyword));
            }
        }

        while (isHalt(Q)) {
            Queue<Cursor> Qi = pickKeyword(Q);
            if (Qi.isEmpty())
                continue;
            Cursor c = Qi.remove();

            SVertex u = c.cursor.next();
            int distance = c.getDist();
            int keyword = c.keyword;
            visitNode(keyword, u, distance, c);
            if (this.out_vertices.contains(u)) {
                if (!crossed.get(keyword).get(u.id) && !LPB.get(u.id).isEmpty()) {
                    HashSet<Integer> blocks = LPB.get(u.id);
                    for (int pid : blocks) {
                        int d = 0;
                        distance += u.getMNK().get(keyword);
                        Q.get(keyword).add(new Cursor(partitions.get(pid).LPN.get(u).iterator(), d, pid, keyword));
                    }
                    crossed.get(keyword).put(u.id, true);
                }
            }
            //FIXME
            if (c.peekDist() != Integer.MAX_VALUE)
                Q.get(keyword).add(c);

            if (this.Answers.size() >= topk) {
                System.out.println("[Total Visit]" + vc.getCount());
                return this.Answers;
            }
        }
        System.out.println("[Total Visit]" + vc.getCount());
        return this.Answers;
    }

    public int getVisitedNode(){
        return vc.getCount();
    }
    public boolean isHalt(HashMap<Integer, Queue<Cursor>> Q) {
        for (Map.Entry<Integer, Queue<Cursor>> Qi : Q.entrySet()) {
            if (!Qi.getValue().isEmpty())
                return true;
        }
        return false;
    }

    /**
     * @param i: keyword
     * @param u: vertex to be visited
     * @param d: distance between u and the keyword i
     */
    public void visitNode(int i, SVertex u, int d, Cursor c) {
        vc.add();
        if (!results.containsKey(u.id)) {
            results.put(u.id, new BlinksResult(u.id, d, i, this.numKeywords, layer));
            BlinksResult Ru = results.get(u.id);

            Ru.setKeywordDistance(i, d);

            Partition p = partitions.get(c.pID);

            for (int keyword : keywords) {
                if (keyword == i)
                    continue;
                int MNKdistance = Integer.MAX_VALUE;
                if (u.getMNK().containsKey(keyword)) {
                    MNKdistance = u.getMNK().get(keyword);
                }
                if (u.DNP >= MNKdistance)
                    Ru.setKeywordDistance(keyword, u.getMNK().get(keyword));
            }
        } else if (results.get(u.id).sumLBDist() > threshold) {
            return;
        } else {
            results.get(u.id).add(i, d);
        }
        BlinksResult u_res = results.get(u.id);
        if (u_res.isFound()) {
            u_res.setLayer(layer);
            //FIXME Induce Result shouldn't be located in a single partition
            Set<Integer> pkey = partitions.keySet();
            for (int pid : pkey) {
                Partition p = partitions.get(pid);
                int root = u_res.getRoot();
                if (p.contains(root)) {
                    u_res.ans = induceResult(p, u_res);
                }
            }
            this.Answers.add(u_res);
        }
        return;
    }

    public Queue<Cursor> pickKeyword(HashMap<Integer, Queue<Cursor>> Q) {
        Counter head = counters.remove();
        head.counter++;
        counters.add(head);
        return Q.get(head.keyword);
    }

    public void ConsIndex(SGraph graph) {
        for (SVertex v : graph.allvertices) {
            if (!LKB.containsKey(v.attr)) {
                LKB.put(v.attr, new HashSet<Integer>());
            }
            LKB.get(v.attr).add(graph.rTable.get(v.id));
        }

        Set<Integer> keyset = partitions.keySet();
        for (int key : keyset) {
            Partition par = partitions.get(key);
            par.BuildIndex();
            for (SVertex p : par.out) {
                int pID = par.pID;
                if (!LPB.containsKey(pID)) {
                    LPB.put(p.id, new HashSet<Integer>());
                }
                LPB.get(p.id).add(pID);
            }
        }
    }

    public AGraph induceResult(Partition graph, BlinksResult res) {
        SVertex root = graph.getVertex(res.getRoot());
        AGraph ans = new AGraph(res.getLayer(), res.getRoot());
        ans.addVertex(root.getID(), root.getAttr());
        for (int keyword : keywords) {
            SVertex tmp = root;
            while (tmp.attr != keyword) {
                SVertex next = tmp.getMNKFirst().get(keyword);
                ans.addVertex(next.getID(), next.getAttr());
                ans.addEdge(tmp.getID(), next.getID());
                tmp = next;
                if(tmp.attr == keyword){
                    ans.content_vertex.add(tmp.getID());
                }
            }
        }

        return ans;
    }
}

class Counter implements Comparable<Counter> {
    int keyword;
    int counter;

    public Counter(int keyword) {
        this.keyword = keyword;
        this.counter = 0;
    }

    public int compareTo(Counter other) {
        return this.counter - other.counter;
    }
}