package algorithms;

import config.CompressFactor;
import graph.OntGraph;
import graph.SGraph;
import index.BigIndex;

import java.io.IOException;

/**
 * Created by samjjx on 2018/4/10.
 */
public class GenBiGindex {
    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        if (args.length < 3) {
            System.out.println("[Error]+Wrong parameters");
            System.out.println("=================Running Example==============");
            System.out.println("java -Xmx304800m -XX:-UseGCOverheadLimit -cp BigIndex-2.0-SNAPSHOT-jar-with-dependencies.jar algorithms.GenBiGindex /tmp/jxjian/yago/test_yago/yago_fact_data /tmp/jxjian/dbpedia/exp_dbpedia/yago_ont_invert /tmp/jxjian/yago_bigindex/yago");
            System.out.println("==============================================");
            System.exit(-1);
        }
        String graph_path = args[0];
        String ont_path = args[1];
        String index_path = args[2];
        SGraph graph = new SGraph(graph_path, true);
        OntGraph ont = new OntGraph(ont_path, true, 0, true);
        long t0 = System.currentTimeMillis();
        BigIndex bi = new BigIndex(graph, ont, CompressFactor.CR);
        System.out.println("Construction Time"+(System.currentTimeMillis() - t0));
        bi.WriteToDisk(index_path);
    }
}
