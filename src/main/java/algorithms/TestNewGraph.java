package algorithms;

/**
 * Created by samjjx on 2018/3/20.
 */
public class TestNewGraph {
    public static void main(String[] args) {
        OptBlock b = new OptBlock(1);

        for (int i = 0; i < 10; i++) {
            b.addNode(i);
        }
        System.out.println(b.graph.nodes().size());
        System.out.println(b.graph.putEdge(9, 11));
        System.out.println(b.graph.nodes().size());
//
//        Random rand = new Random(15);
//
//        for(int i=0;i<1000000 ;i++){
//            int src = rand.nextInt(10);
//            int dst = rand.nextInt(10);
//            if(src == dst)
//                continue;
//            b.addEdge(src, dst);
//
//            if(i%10000 == 0){
//                System.out.println(i);
//            }
//        }
//
//        System.out.println(b.graph.edges());
    }
}
