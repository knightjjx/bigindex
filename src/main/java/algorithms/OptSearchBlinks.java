package algorithms;

import algorithms.blinksIndex.*;
import com.google.common.collect.HashBasedTable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;

/**
 * Created by samjjx on 2018/3/22.
 */
public class OptSearchBlinks {

    public HashSet<Integer> keywords;
    public OptBlinks blinks;
    public int threshold;

    public PriorityQueue<OptCounter> counters;

    public HashMap<Integer, QueueCursor> Q;

    public HashBasedTable<Integer, Integer, Boolean> crossed; // keyword -> portal -> crossed?

    public HashMap<Integer, BlinkResult> visited;

    public HashSet<BlinkResult> qualified_ans;

    public int topk;

    public int num_keywords;

    public OptSearchBlinks(OptBlinks blinks, int k) {
        this.blinks = blinks;
        topk = k;
    }

    public void searchBlinks(HashSet<Integer> keywords, int threshold) {
        this.keywords = keywords;
        init(threshold);
        search();
    }

    /**
     * @param keyword expansion the keyword
     * @param u       the visited vertex
     * @param d       the distance from the vertex to the keyword
     */
    public void visitNode(int keyword, int u, int d) {
        if (!this.visited.containsKey(u)) {
            // Not yet visited
            BlinkResult u_blink_result = new BlinkResult(u, num_keywords);
            u_blink_result.addKeyword(keyword, d);

            OptBlock b = blinks.getVertexBlock(u);

            for (int k : keywords) {
                if (k == keyword)
                    continue;
                int u_mnk_dist = blinks.getMNK(b, u, k);
                if (blinks.getDNP(b, u) > u_mnk_dist)
                    u_blink_result.addKeyword(k, u_mnk_dist);
            }
            this.visited.put(u, u_blink_result);

            // TODO There should be a pruning. Just skip it for now
        } else {
            BlinkResult par_u = this.visited.get(u);
            if (!par_u.hasKeyword(keyword)) {
                par_u.addKeyword(keyword, d);
            }
        }

        // Verify the answer

        int sum_dist =this.visited.get(u).sumDist();
        if (sum_dist < threshold) {
        //if (sum_dist != Integer.MAX_VALUE && sum_dist < threshold) {
            // Answer Found
            qualified_ans.add(this.visited.get(u));
        }
    }


    public void search() {
        while (!judgeTerminate()) {
            OptCounter pointer = pickKeyword();
            int keyword = pointer.getKeyword();
            OptCursor opc = Q.get(keyword).getCursor();
            /**
             *  line 8~14
             */

            BlinkUniqueCursor uni_cur = opc.next();

            int u = uni_cur.getNode();
            int d = uni_cur.getDist()+ opc.get_extra_distance();

            visitNode(keyword, u, d);

            if (blinks.isPortal(u)) {
                try {
                    if (!crossed.get(keyword, u)) {
                        // Loop on the blocks
                        for (int b : blinks.getLPB(u)) {
                            // d + the old distance.
                            Q.get(keyword).addCursor(new OptCursor(blinks.getLPN(b, u), d));
                        }
                        crossed.put(keyword, u, true);
                    }
                }catch (java.lang.NullPointerException e){
                    System.out.println(keyword+"\t"+u);
                    System.exit(-1);
                }
            }

            /**
             * line 15~18
             */
            // TODO there should be pruning here. Ignore now
            // Top-k found
            if (qualified_ans.size() >= topk || sumTopQ() > threshold) {
                System.out.println(sumTopQ());
                printAns();
                return;
            }
        }
        printAns();
    }


    public void printAns() {
        System.out.println("=============Answer Info =============");
        System.out.println("=============Answer Size =============");
        System.out.println(qualified_ans.size());
        System.out.println("=============Answer Details =============");
        System.out.println(qualified_ans);
        return;
    }

    public int sumTopQ() {
        int sum = 0;
        for (int keyword : Q.keySet()) {
            int q_peek_distance = Q.get(keyword).peekDist();
            if(q_peek_distance == Integer.MAX_VALUE)
                continue;
            sum += q_peek_distance;
        }
        return sum;
    }

    /**
     * line 1~5
     *
     * @param threshold: for pruning
     */
    public void init(int threshold) {
        this.crossed = HashBasedTable.create();
        this.visited = new HashMap<Integer, BlinkResult>();
        this.qualified_ans = new HashSet<BlinkResult>();
        this.counters = new PriorityQueue<OptCounter>();
        this.Q = new HashMap<Integer, QueueCursor>();

        this.threshold = threshold;
        this.num_keywords = keywords.size();
        // Total size of the following structure: O(|P||w|)
        // Loop on the keyword
        System.out.println("Init the crossed -----> line 11");
        System.out.println(keywords);
        for (int keyword : keywords) {
            // Loop on the portal
            for (int p : blinks.LPB.keySet()) {
                // Loop on the block
                crossed.put(keyword, p, false);
//                System.out.println(crossed);
            }
        }

        for (int p : blinks.LPB.keySet()) {
            int w = blinks.vertices.get(p);
            crossed.put(w, p, true);
        }
        System.out.println("Finish the crossed -----> line 11");

        for (int keyword : keywords) {
            counters.add(new OptCounter(keyword));
            Q.put(keyword, new QueueCursor(keyword));
        }

        // Loop on the keyword
        for (int keyword : keywords) {
            // Loop on the block
            for (int b : blinks.getLKB(keyword)) {
                OptCursor opc = new OptCursor(blinks.getLKN(b, keyword), keyword, 0);
                Q.get(keyword).addCursor(opc);
            }
        }

    }

    public boolean judgeTerminate() {
        for (int keyword : Q.keySet()) {
            if (!Q.get(keyword).isEmpty())
                return false;
        }
        return true;
    }

    public OptCounter pickKeyword() {
        OptCounter tmp;
        while(true) {
            tmp = counters.remove();
            tmp.addOne();
            counters.add(tmp);
            if(!Q.get(tmp.getKeyword()).isEmpty())
                break;
        }
        return tmp;
    }
}
