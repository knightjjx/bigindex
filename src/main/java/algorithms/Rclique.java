package algorithms;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import graph.AGraph;
import graph.SGraph;
import graph.SVertex;
import result.RcliqueResult;
import util.IOPack;

import java.util.*;

/**
 * Created by samjjx on 2017/5/6.
 */
public class Rclique implements SearchAlgo<RcliqueResult> {
    public ArrayList<RcliqueResult> Answers;
    public HashSet<Integer> keywords;

    public HashMap<Integer, HashSet<Integer>> inverted;
    public HashMap<Integer, HashSet<Integer>> neighbor_index; // vid -> R hops labels
    public Table<Integer, Integer, Integer> dist; // vid -> (vid -> distance)
    public int R = 2;
    public int layer;
    public int r = 2;
    public HashMap<Integer, String> context_attributes;

    public int topk = 5;

    public int limit;

    public Rclique(HashSet<Integer> keywords, SGraph graph){
        this.keywords = keywords;
        neighbor_index = new HashMap<Integer, HashSet<Integer>>();
        dist = HashBasedTable.create();
        ConsIndex(graph, R);
        this.layer = graph.layer;
        context_attributes = new HashMap<Integer, String>();
    }
    public void setLimit(int limit){
        this.limit = limit;
    }
    public Rclique(HashSet<Integer> keywords, SGraph graph, int R){
        this.R = R;
        this.keywords = keywords;
        neighbor_index = new HashMap<Integer, HashSet<Integer>>();
        dist = HashBasedTable.create();
        ConsIndex(graph, this.R);
        this.layer = graph.layer;
        context_attributes = new HashMap<Integer, String>();
    }

    public Rclique(SGraph graph){
        neighbor_index = new HashMap<Integer, HashSet<Integer>>();
        dist = HashBasedTable.create();
        ConsIndex(graph, R);
        this.layer = graph.layer;
        context_attributes = new HashMap<Integer, String>();
    }

    public ArrayList<RcliqueResult> semantic(HashSet<Integer> keywords, SGraph graph) {
        this.keywords = keywords;
        for (int keyword : keywords) {
            if(!inverted.containsKey(keyword))
                inverted.put(keyword, new HashSet<Integer>());
        }

        ArrayList<RcliqueResult> res = new ArrayList<RcliqueResult>();
        // Init Search Space
        SearchSpace sp = new SearchSpace(this.keywords, inverted);

        RcliqueResult A = FindTopRankedAnswer(sp, graph, keywords, r);
        HashMap<RcliqueResult, SearchSpace> ans2sp = new HashMap<RcliqueResult, SearchSpace>();
        ans2sp.put(A, sp);
        Queue<RcliqueResult> qResult = new LinkedList<RcliqueResult>();

        if (A.ans.resultSize() != 0)
            qResult.add(A);
        int count = 0;
        while (!qResult.isEmpty()) {
            RcliqueResult head = qResult.remove();
            res.add(head);
            count++;
            if (count >= topk)
                break;
            HashSet<Integer> S = head.getVertex();

            ArrayList<SearchSpace> subspace = ans2sp.get(head).getSubSpace(S);

            for (SearchSpace subp : subspace) {
                RcliqueResult subres = FindTopRankedAnswer(subp, graph, keywords, r);
                if (subres == null)
                    continue;
                ans2sp.put(subres, subp);
                qResult.add(subres);
            }
        }
        return res;
    }

    public ArrayList<RcliqueResult> semantic(HashSet<String> keywords, SGraph graph, boolean context) {

        System.out.println("Begin the r-clique query");
        ArrayList<RcliqueResult> res = new ArrayList<RcliqueResult>();
        // Init Search Space
        SearchSpace sp = new SearchSpace(keywords, this.context_attributes,context);

        RcliqueResult A = FindTopRankedAnswer(sp, graph, keywords, r,context);
        HashMap<RcliqueResult, SearchSpace> ans2sp = new HashMap<RcliqueResult, SearchSpace>();
        ans2sp.put(A, sp);
        Queue<RcliqueResult> qResult = new LinkedList<RcliqueResult>();

        if (A.ans.resultSize() != 0) {
            qResult.add(A);
        }

        System.out.println("Init done, first result");

        int count = 0;
        while (!qResult.isEmpty()) {
            RcliqueResult head = qResult.remove();
            res.add(head);
            count++;
            if (count >= topk)
                break;
            HashSet<Integer> S = head.getVertex();

            ArrayList<SearchSpace> subspace = ans2sp.get(head).getContextSubSpace(S);

            for (SearchSpace subp : subspace) {
                RcliqueResult subres = FindTopRankedAnswer(subp, graph, keywords, r, true);
                if (subres == null)
                    continue;
                ans2sp.put(subres, subp);
                qResult.add(subres);
            }
        }
        return res;
    }

    public AGraph induceResult(RcliqueResult res) {

        return null;
    }

    public void ConsIndex(SGraph graph, int R) {
        // Inverted list
        inverted = new HashMap<Integer, HashSet<Integer>>();
        for (SVertex v : graph.allvertices) {
            if (!inverted.containsKey(v.attr)) {
                inverted.put(v.attr, new HashSet<Integer>());
            }
            inverted.get(v.attr).add(v.id);
        }
        System.out.println("Number keywords");
        // Neighbor lists
        System.out.println(graph.allvertices.size());
        int count = 0;
        for (SVertex v : graph.allvertices) {
            computeNeighbor(v.id, graph, R);
//            count++;
//            if(count % 1000 == 0 ){
//                System.out.println(count);
//            }
        }

    }

    public void computeNeighbor(int vid, SGraph graph, int R) {
        HashSet<Integer> neighbor = new HashSet<Integer>();
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(vid);
        int hop = 0;

        while (hop < R) {
            Queue<Integer> next_hop = new LinkedList<Integer>();
            while (!queue.isEmpty()) {
                int head = queue.remove();
                if (neighbor.contains(head)||!graph.contains(head))
                    continue;
                neighbor.add(head);
                dist.put(vid, head, hop);
                next_hop.addAll(graph.getChildren(head));
            }
            queue = next_hop;
            hop++;
            if (queue.isEmpty())
                break;
        }
        this.neighbor_index.put(vid, neighbor);
    }

    public RcliqueResult FindTopRankedAnswer(SearchSpace S, SGraph graph, HashSet<Integer> keywords, int r) {

        Table<Integer, Integer, Integer> d = HashBasedTable.create(); // d is the shortest distance. (vid, keyword) -> distance
        Table<Integer, Integer, Integer> n = HashBasedTable.create(); // n is the neighbor between vertex and the adjacent. (vid, keyword) -> vid
        for (int dim : S.query_keywords) {
            if (S.getDimension(dim).isEmpty())
                return new RcliqueResult(keywords.size(), this.layer);
        }
        // line 3-6 init
        for (int keyword : keywords) {
            for (int j : S.getDimension(keyword)) {
                d.put(j, keyword, 0);
                n.put(j, keyword, j);
            }
        }

        for (int keyword : keywords) {
            for (int j : S.getDimension(keyword)) { // j-> s_i^j
                for (int k : keywords) {
                    if (k == keyword)
                        continue;
                    int dist = Integer.MAX_VALUE;
                    int nearest = Integer.MAX_VALUE;

                    for (int vid : S.getDimension(k)) {
                        if (!this.dist.contains(j, vid))
                            continue;
                        int tmp = this.dist.get(j, vid);
                        if (neighbor_index.get(j).contains(vid) && dist > tmp && tmp <= r) {
                            dist = this.dist.get(j, vid);
                            nearest = vid;
                        }
                    }
                    d.put(j, k, dist);
                    n.put(j, k, nearest);
                }
            }
        }

        int leastWeight = Integer.MAX_VALUE;
        RcliqueResult ans = new RcliqueResult(keywords.size(), this.layer);

        for (int keyword : keywords) {
            l:
            for (int j : S.getDimension(keyword)) {
                for (int k : keywords) {
                    if (d.get(j, k) > r)
                        continue l;
                }
                int weight = 0;
                for (int k : keywords) {
                    weight += d.get(j, k);
                }
                if (weight < leastWeight) {
                    leastWeight = weight;
                }

                //FIXME: root
                ans.ans = new AGraph(this.layer, 0);
                for (int k : keywords) {
                    SVertex nearest = graph.getVertex(n.get(j, k));
                    ans.ans.addVertex(nearest.id, nearest.attr);
                }
            }
        }
        return ans;
    }


    public RcliqueResult FindTopRankedAnswer(SearchSpace S, SGraph graph, HashSet<String> keywords, int r, boolean context) {

        Table<Integer, String, Integer> d = HashBasedTable.create(); // d is the shortest distance. (vid, keyword) -> distance
        Table<Integer, String, Integer> n = HashBasedTable.create(); // n is the neighbor between vertex and the adjacent. (vid, keyword) -> vid
        for (String dim : S.query_keywords_context) {
            if (S.getContextDimension(dim).isEmpty())
                return new RcliqueResult(keywords.size(), this.layer);
        }
        // line 3-6 init
        for (String keyword : keywords) {
            for (int j : S.getContextDimension(keyword)) {
                d.put(j, keyword, 0);
                n.put(j, keyword, j);
            }
        }
        System.out.println("Top Init");

        for (String keyword : keywords) {
            System.out.println(keyword+"\t"+S.getContextDimension(keyword).size());
        }

        for (String keyword : keywords) {
            for (int j : S.getContextDimension(keyword)) { // j-> s_i^j
                int count = 0;
                for (String k : keywords) {
                    if (k == keyword)
                        continue;
                    int dist = Integer.MAX_VALUE;
                    int nearest = Integer.MAX_VALUE;
                    int cnt = 0;
                    for (int vid : S.getContextDimension(k)) {
                        cnt++;
                        if(cnt>limit) // TODO: limit is for zhiwei
                            break;
                        if (!this.dist.contains(j, vid))
                            continue;
                        int tmp = this.dist.get(j, vid);
                        if (neighbor_index.get(j).contains(vid) && dist > tmp && tmp <= r) {
                            dist = this.dist.get(j, vid);
                            nearest = vid;
                        }
                        break; //FIXME: here for zhiwei
                    }
                    d.put(j, k, dist);
                    n.put(j, k, nearest);
                }

                count++;
                if(count % 100 ==0)
                    System.out.println("Finish\t" + count);
//                if(count > 1000)
//                    break;
            }
        }
        System.out.println("Init Finish");
        int leastWeight = Integer.MAX_VALUE;
        RcliqueResult ans = new RcliqueResult(keywords.size(), this.layer);

        for (String keyword : keywords) {
            l:
            for (int j : S.getContextDimension(keyword)) {
                for (String k : keywords) {
                    if (d.get(j, k) > r)
                        continue l;
                }
                int weight = 0;
                for (String k : keywords) {
                    weight += d.get(j, k);
                }
                if (weight < leastWeight) {
                    leastWeight = weight;
                }

                //FIXME: root
                ans.ans = new AGraph(this.layer, 0);
                for (String k : keywords) {
                    SVertex nearest = graph.getVertex(n.get(j, k));
                    ans.ans.addVertex(nearest.id, nearest.attr);
                }
            }
        }
        System.out.println("Results");

        Set<Integer> keyset = ans.ans.allvertices.keySet();
        for(int id: keyset)
            System.out.print(id+"\t");
        System.out.println();
        return ans;
    }

    public void loadingContentAttribute(HashMap<Integer, String> context_attributes){
        this.context_attributes = context_attributes;
    }
}

