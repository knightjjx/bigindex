package bigindex_config;

import graph.SGraph;
import util.IOPack;

import java.io.BufferedWriter;
import java.io.IOException;

public class SampleSubGraph {
    public static void main(String[] args) throws IOException {
        String path = args[0];
        String sample_store_path = args[1];
        int hop = Integer.parseInt(args[2]);
        int number = Integer.parseInt(args[3]);
        SGraph graph = new SGraph(path, false);
        GraphSample sample = new GraphSample(graph, number, hop);
        String rootTable = sample_store_path + "all.root";

        IOPack ioPack = new IOPack();
        BufferedWriter config = ioPack.getWritter(rootTable);
        
        for(int key: sample.samples.keySet()){
            SGraph subgraph = sample.samples.get(key);
            subgraph.WriteToDisk(sample_store_path + key, true);
            config.write(key+"\n");
        }
        config.flush();
        config.close();
    }
}
