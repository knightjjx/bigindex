package bigindex_config;

import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by samjjx on 2018/4/2.
 */
public class Config {
    public HashMap<Integer, HashMap<Integer, Integer>> configs;
    public int gen_layer = 0;

    public Config(String path) throws IOException {
        this.configs = new HashMap<Integer, HashMap<Integer, Integer>>();
        load_from_disk(path);
    }

    public void load_from_disk(String path) throws IOException {
        IOPack ioPack = new IOPack();
        int layer = 0;
        BufferedReader confbr = ioPack.getBufferInstance(path + ".config");
        String line;
        this.configs = new HashMap<Integer, HashMap<Integer, Integer>>();
        while ((line = confbr.readLine()) != null) {
            String[] data = line.split("\t");
            layer = Integer.parseInt(data[0]);
            int config_size = Integer.parseInt(data[1]);
            HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
            for (int i = 0; i < config_size; i++) {
                line = confbr.readLine();
                data = line.split("\t");
                config.put(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
            }
            configs.put(layer, config);
            System.out.println("Finish Loading\t" + layer);
        }
    }
    int num_gen =3;
    public HashSet<Integer> generalize(HashSet<Integer> ori) {
        HashSet<Integer> res = ori;
        int current_layer = 1;
        HashSet<Integer> tmp;
        System.out.println();
        int num_gen = this.num_gen;

        for(int i = 0;i<num_gen;i++){
            tmp = new HashSet<Integer>();
            HashMap<Integer, Integer> config = configs.get(current_layer);
            System.out.println("[Hierarchy]\t" + res);
            System.out.println();
            for (int k : res) {
                if (config.containsKey(k)) {
                    tmp.add(config.get(k));
                }
            }
            if (tmp.size() < ori.size()) {
                System.out.println("[Breaking]\t" + tmp);
                System.out.println("Begin print map");
                for (int k : res) {
                    System.out.println(k + "\t" + config.get(k));
                }
                System.out.println("End print map");
                break;
            }
            res = tmp;
            current_layer++;
            gen_layer = current_layer;
        }
        System.out.println("[Hierarchy]\t" + res);
        return res;
    }
    public void setNum_get(int num_gen){
        this.num_gen = num_gen;
    }
}
