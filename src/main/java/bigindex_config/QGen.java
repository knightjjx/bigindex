package bigindex_config;

import config.Pattern;
import config.Query;

import java.io.IOException;

/**
 * Created by samjjx on 2018/4/2.
 */
public class QGen {
    public static void main(String[] args) throws IOException {
        String config_path = args[0];
        String ori_query_directory = args[1];
        Config config = new Config(config_path);
        int num_gen = Integer.parseInt(args[2]);
        config.setNum_get(num_gen);
        if(args.length < 3){
            System.out.println("===============INFO===================");
            System.out.println("java -jar BigIndex-1.0-SNAPSHOT-jar-with-dependencies.jar /tmp/jxjian/index_gen/dbpedia /tmp/jxjian/query/vary_query_size/ 3");
            System.exit(-1);
        }
        Query query = new Query(ori_query_directory);

        for (Pattern ptn : query.batchQuery) {
            System.out.println();
            ptn.genKeywords(config);
            System.out.println(ptn.path);
            System.out.println("Gen layer\t" + config.gen_layer);
            System.out.println("keywords\t"+ptn.gen_keywords);
            System.out.println();
        }
    }
}
