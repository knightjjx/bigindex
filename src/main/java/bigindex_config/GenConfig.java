package bigindex_config;

import bsim.PaigeTarjan;
import graph.OntGraph;
import graph.SCC;
import graph.SGraph;
import util.IOPack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GenConfig {
    public static HashMap<Integer, Integer> config;

    static Logger logger;

    static BufferedWriter statistic;

    public static SGraph compress(SGraph graph) {
        graph.reset();
        SCC scc = new SCC(graph);
        graph.initRank(scc);
        System.out.println("[Max]" + graph.maxRank);
        PaigeTarjan pt = new PaigeTarjan();
        SGraph compressed = pt.bisimulationGeneral(graph);
        return compressed;
    }

    public static void computeOnegraph(String path) throws IOException {
        SGraph graph = new SGraph(path, false);
        logger.info("Samples\t" + graph.edgeSize() + "\t" + graph.vertexSize());
        graph.generalization(config);
        SGraph compress = compress(graph);
        logger.info("CompressSample\t" + compress.edgeSize() + "\t" + compress.vertexSize());
        statistic.write(graph.vertexSize() + "\t" + graph.edgeSize() + "\n");
        statistic.write(compress.vertexSize() + "\t" + compress.edgeSize() + "\n");
    }

    public static void computeAllGraph(String basePath) throws IOException {
        IOPack ioPack = new IOPack();
        BufferedReader br = ioPack.getBufferInstance(basePath + "all.root");
        String line = "";
        while ((line = br.readLine()) != null) {
            logger.info("Root\t" + line);
            try {
                computeOnegraph(basePath + line);
            } catch (java.lang.NullPointerException e) {
                logger.info("Fail");
            }
        }
    }


    public static void main(String[] args) throws IOException {
        String graph_path = args[0];
        String ont_path = args[1];
        String config_path = args[2];
        String sample_path = args[3];
        String log_path = args[4];
        String statistic_path = args[5];

        IOPack ioPack = new IOPack();
        BufferedWriter bw = ioPack.getWritter(config_path + ".config");

        statistic = ioPack.getWritter(statistic_path);

        logger = Logger.getLogger("BsimSample");

        FileHandler fh = new FileHandler(log_path);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);


        SGraph graph = new SGraph(graph_path, false);
        OntGraph ont = new OntGraph(ont_path, false, 0, true);

        config = ont.generateConfigRandom(graph);


        computeAllGraph(sample_path);

        /**
         * Compute the origianl graph
         */
        logger.info("OriginalWhole\t" + graph.edgeSize() + "\t" + graph.vertexSize());
        statistic.write(graph.edgeSize() + "\t" + graph.vertexSize() +"\n");
        graph.generalization(config);
        SGraph tmp = compress(graph);
        logger.info("CompressWhole\t" + tmp.edgeSize() + "\t" + tmp.vertexSize());
        statistic.write(tmp.edgeSize() + "\t" + tmp.vertexSize() +"\n");


        statistic.flush();
        statistic.close();
        /**
         * write config to the disk
         */
        for (int key : config.keySet()) {
            bw.write(key + "\t" + config.get(key) + "\n");
        }
        bw.flush();
        bw.close();
    }
}
