package bigindex_config;

import bsim.PaigeTarjan;
import graph.SCC;
import graph.SGraph;

import java.io.IOException;
import java.util.*;

public class GraphSample {
    public HashMap<Integer, SGraph> samples;
    public int sample_hop;
    public SGraph graph;
    public int ori_size;

    public GraphSample(SGraph graph, int number, int hop) {
        samples = new HashMap<Integer, SGraph>();
        this.sample_hop = hop;
        this.graph = graph;
        ori_size = 0;
        for (int i = 0; i < number; i++) {
            oneSample();
        }
    }

    public void oneSample() {
        Random random = new Random();

        int root = random.nextInt();

        int size = graph.vertexSize();
        while (samples.containsKey(root) || !graph.contains(root)) {
            root = random.nextInt(size);
        }

        SGraph sample = new SGraph();

        HashSet<Integer> visited = new HashSet<Integer>();

        Queue<Integer> queue = new LinkedList<Integer>();

        queue.add(root);
        sample.addVertex(root, graph.getAttribute(root));

        for (int i = 0; i < sample_hop; i++) {
            Queue<Integer> next_hop_queue = new LinkedList<Integer>();
            while (!queue.isEmpty()) {
                int head = queue.remove();
                if (visited.contains(head))
                    continue;

                HashSet<Integer> children = graph.getChildren(head);

                for (int c : children) {
                    sample.addVertex(c, graph.getAttribute(c));
                    sample.addEdge(head, c);
                    if (visited.contains(c))
                        continue;
                    next_hop_queue.add(c);
                }
                visited.add(head);
            }
            queue = next_hop_queue;
        }
        if(sample.vertexSize() < 250 || sample.edgeSize() < 500){
            oneSample();
            return;
        }
        if(sample.vertexSize() > 1000 || sample.edgeSize() > 2000){
            oneSample();
            return;
        }
        samples.put(root, sample);
        ori_size += sample.graphSize();
    }

    public void computeBsim(HashMap<Integer, Integer> config) {
        int compress_size = 0;
        for (int key : samples.keySet()) {
            SGraph sample = samples.get(key);
            SGraph tmp = sample.clone();
            tmp.generalization(config);
            tmp = compress(tmp);
            compress_size += tmp.graphSize();
        }
        System.out.println("Compress size\t" + compress_size);
        System.out.println("Compress ratio\t" + (double) compress_size / ori_size);
    }

    public SGraph compress(SGraph graph) {
        graph.reset();
        SCC scc = new SCC(graph);
        graph.initRank(scc);
        PaigeTarjan pt = new PaigeTarjan();
        SGraph compressed = pt.bisimulationGeneral(graph);
        return compressed;
    }

    public int number() {
        return 0;
    }
}
