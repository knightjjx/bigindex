package util;

/**
 * Created by samjjx on 2017/9/27.
 */
public class VertexCounter {
    public int count = 0;
    public VertexCounter(){
        count = 0;
    }
    public void add(){
        count++;
    }
    public int getCount(){
        return count;
    }
}
