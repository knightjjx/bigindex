package util;

import config.KWFactor;
import graph.OntGraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by samjjx on 2017/9/21.
 */
public class KWSelector {
    public OntGraph ont;
    public KWFactor kwf = null;
    public int numKewwords = 0;
    public int distance = 5;

    public KWSelector(OntGraph ont, KWFactor kwf, int numKewwords) {
        this.ont = ont;
        this.kwf = kwf;
        this.numKewwords = numKewwords;
    }

    public HashSet<Integer> SelectKW() {
        assert kwf != null;
        if (kwf == KWFactor.SIMPLE)
            return SimpleKeywords();
        if (kwf == KWFactor.ONE_BRANCH)
            return OneBranchKeywords();
        if (kwf == KWFactor.DISTANCE_LIMIT)
            return DistKeywords();
        if (kwf == KWFactor.TEST_CASE)
            return testKeywords();
        return null;
    }

    public HashSet<Integer> testKeywords() {
        HashSet<Integer> keywords = new HashSet<Integer>();
        keywords.add(1);
        keywords.add(4);
//        for (int i = 1; i <= numKewwords; i++) {
//            keywords.add(i);
//        }
        return keywords;
    }

    public HashSet<Integer> SimpleKeywords() {
        HashSet<Integer> keywords = new HashSet<Integer>();
        ArrayList<Integer> allKeywords = new ArrayList<Integer>();
        allKeywords.addAll(ont.attr2ID.keySet());
        Random random = new Random();
        while (keywords.size() < numKewwords) {
            keywords.add(allKeywords.get(random.nextInt(allKeywords.size())));
        }
        return keywords;
    }

    public HashSet<Integer> OneBranchKeywords() {
        HashSet<Integer> keywords = new HashSet<Integer>();
        ArrayList<Integer> allKeywords = new ArrayList<Integer>();
        allKeywords.addAll(ont.attr2ID.keySet());

        while (keywords.size() < numKewwords) {

        }
        return keywords;
    }

    public HashSet<Integer> DistKeywords() {
        HashSet<Integer> keywords = new HashSet<Integer>();
        ArrayList<Integer> allKeywords = new ArrayList<Integer>();
        allKeywords.addAll(ont.attr2ID.keySet());
        Random random = new Random();
        while (keywords.size() < numKewwords) {
            boolean judge = true;
            int nKw = allKeywords.get(random.nextInt(allKeywords.size()));
            int nKwID = ont.attr2ID.get(nKw);
            for (int kw : keywords) {
                int kwID = ont.attr2ID.get(kw);
                int dist = ont.computeDist(kwID, nKwID);
                if (dist > distance) {
                    judge = false;
                    break;
                }
            }
            if (judge)
                keywords.add(nKw);
        }
        return keywords;
    }
}
