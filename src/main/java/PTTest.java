import config.CompressFactor;
import decompress.Decompressor;
import graph.AGraph;
import graph.OntGraph;
import graph.SGraph;
import index.BigIndex;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Created by samjjx on 2017/8/24.
 */
public class PTTest {
    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/fund/synt0", true);
        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/fund/synt0.ont", true, 0, true);
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/synt/synt6", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/synt/synt6.ont", true, 0, true);
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/scc-test/test", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/scc-test/test.ont", true, 0, true);

//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/test/testscc", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/test/test.ont", true, 0, true);
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/testpt/test", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/testpt/test.ont", true, 0, true);
        long t0 = System.currentTimeMillis();
        BigIndex bi = new BigIndex(graph, ont, CompressFactor.CR);
        System.out.println("[BigIndex]"+ (System.currentTimeMillis()-t0));
        System.out.println(bi);
        Set<Integer> keySet = bi.MulGraph.keySet();

        for (int layer : keySet) {
            bi.MulGraph.get(layer).printGraph();
        }
//
//        for (SVertex v : bi.top.allvertices) {
//            System.out.println(v);
//        }
//        System.out.println(bi.top);
//        bi.top.printGraph();
//        for (SEdge e : bi.top.allEdges()) {
//            System.out.println(e.from() + "\t" + e.to());
//        }

        System.out.println("===============");
//        for (SVertex v : bi.top.allvertices) {
//            System.out.print(v + "\t");
//            for (SVertex c : bi.top.getChildren(v)) {
//                System.out.print(c + "\t");
//            }
//            System.out.println();
//        }

        System.out.println("===============");

        AGraph test = new AGraph(3, 0);
        test.addVertex(75, 14);
        test.addVertex(1093, 8);
        test.addVertex(1082, 1);
        test.addVertex(1078, 18);

        test.addEdge(75, 1093);
        test.addEdge(1093, 1082);
        test.addEdge(1082, 1078);
        Queue<AGraph> Lcres = new LinkedList<AGraph>();

//        Lcres.add(test);
        Decompressor dc = new Decompressor(Lcres, bi);
        dc.DecompressByLayer();
        Queue<AGraph> dr = dc.getResults();
        while (!dr.isEmpty()) {
            System.out.println(dr.remove());
        }

        System.out.println("=============================================");

        SGraph l2 = bi.MulGraph.get(2);
//        for (int vid : test.allvertices.keySet()) {
//            System.out.println(vid + "\t->l2 dmap->\t" + l2.getVertex(vid).dmap);
//        }



// The following is backup codes

//        for (int vid : test.allvertices.keySet()) {
//            System.out.println(vid + "\t->children->\t" + l2.getChildren(vid));
//        }
//
//
//        System.out.println("===================layer3==========================");
//        SGraph l3 = bi.MulGraph.get(3);
//        for (int vid : test.allvertices.keySet()) {
//            System.out.println(vid + "\t->dmap->\t" + l3.getVertex(vid).dmap);
//        }
//        for (int vid : test.allvertices.keySet()) {
//            System.out.println(vid + "\t->children->\t" + l3.getChildren(vid));
//        }
//
//        for (int vid : test.allvertices.keySet()) {
//            System.out.println(vid + "\t->groundMap->\t" + bi.getGroundVertices(3, vid));
//        }
//
//        for (int vid : test.allvertices.keySet()) {
//            System.out.println(vid + "\t->2Map->\t" + bi.MulDMap(3,2, vid));
//        }
////
////        System.out.println(bi.MulGraph.get(2).getParents(1013));
////        System.out.println(bi.MulGraph.get(2).getParents(376));
////        System.out.println(bi.MulGraph.get(2).getParents(1098));
////        System.out.println(bi.MulGraph.get(2).getParents(1005));
//        System.out.println(bi.MulGraph.get(3).getVertex(1103) + "\t->groundMap->\t" + bi.getGroundVertices(3, 1103));
//        System.out.println(bi.MulGraph.get(3).getVertex(1049) + "\t->groundMap->\t" + bi.getGroundVertices(3, 1049));
//        System.out.println(bi.MulGraph.get(3).getVertex(1034) + "\t->groundMap->\t" + bi.getGroundVertices(3, 1034));
    }
}