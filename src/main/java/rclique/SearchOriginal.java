package rclique;

import algorithms.Rclique;
import graph.SGraph;
import result.RcliqueResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by samjjx on 2018/3/13.
 */
public class SearchOriginal {
    static HashSet<Integer> keywords;

    static void generateNumKeywords(int number) {
        keywords = new HashSet<Integer>();
        for (int i = 0; i < number; i++) {
            keywords.add(i+2);
        }
    }
    public static void main(String[] args) throws IOException {
        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/synt/synt5", true, false);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/synt/synt5.ont", true, 0, true);
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/blink/blink", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/blink/blink.ont", true, 0, true);
        Logger logger = Logger.getLogger("RClique");
        FileHandler fh = new FileHandler("rclique.log");
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        System.out.println("Original Graph");
//        System.out.println(graph);
        long t0 = System.currentTimeMillis();
        generateNumKeywords(5);

        Rclique rclique = new Rclique(keywords, graph);
        System.out.println("Rclique Index construction time on Original Graph: \t" + (System.currentTimeMillis() - t0));

        t0 = System.currentTimeMillis();
        ArrayList<RcliqueResult> res1 = rclique.semantic(keywords, graph);

        logger.info("# of results in original graphs\t" + res1.size());
        System.out.println("# of results in original graphs\t" + res1.size());
        int count = 0;
        for(RcliqueResult r: res1){
            System.out.print(">" + count++ +"\tResult pattern\n"+r);
        }
        System.out.println("Rclique Query time on Original Graph: \t" + (System.currentTimeMillis() - t0));

    }
}
