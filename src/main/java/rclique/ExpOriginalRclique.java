package rclique;

import algorithms.OptBlinks;
import algorithms.Rclique;
import config.Pattern;
import config.Query;
import graph.SGraph;
import result.RcliqueResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ExpOriginalRclique {
    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.out.println("[Error]+Wrong parameters");
            System.out.println("=================Running Example==============");
            System.out.println("java -Xmx304800m -XX:-UseGCOverheadLimit -cp BigIndex-2.0-SNAPSHOT-jar-with-dependencies.jar rclique.ExpOriginalRclique /tmp/jxjian/yago/test_yago/yago_fact_data /tmp/jxjian/dbpedia/exp_dbpedia/yago_ont_invert /tmp/jxjian/yago_bigindex/yago");
            System.out.println("==============================================");
            System.exit(-1);
        }

        String graph_path = args[0];
        SGraph graph = new SGraph(graph_path, true, false);
        String query_path = args[1];


        /** Init the logger **/
        String log_path = args[2];
        Logger logger = Logger.getLogger("Rclique");
        FileHandler fh = new FileHandler(log_path);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        Query query = new Query(query_path);

        Rclique rclique = new Rclique(graph);
        for (Pattern ptn : query.batchQuery) {
            logger.info("===============================================\n");
            logger.info("Pattern\t" + ptn.path);
            long t0 = System.currentTimeMillis();
            logger.info("Rclique Index construction time on Original Graph: \t" + (System.currentTimeMillis() - t0));

            t0 = System.currentTimeMillis();
            ArrayList<RcliqueResult> res1 = rclique.semantic(ptn.keywords, graph);
            logger.info("Rclique Query time on Original Graph: \t" + (System.currentTimeMillis() - t0));
            logger.info("Results size:\t"+res1.size());
        }
    }
}
