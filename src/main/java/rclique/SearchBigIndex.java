package rclique;

import algorithms.Rclique;
import config.CompressFactor;
import graph.OntGraph;
import graph.SGraph;
import index.BigIndex;
import result.RcliqueResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by samjjx on 2017/8/24.
 */
public class SearchBigIndex {
    static HashSet<Integer> keywords;

    static void generateNumKeywords(int number) {
        keywords = new HashSet<Integer>();
        for (int i = 0; i < number; i++) {
            keywords.add(i + 2);
        }
    }

    public static void main(String[] args) throws CloneNotSupportedException, IOException {
        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/synt/synt5", true);
        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/synt/synt5.ont", true, 0, true);
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/blink/blink", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/blink/blink.ont", true, 0, true);
        Logger logger = Logger.getLogger("RClique");
        FileHandler fh = new FileHandler("rclique.log");
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        System.out.println("Original Graph");
        System.out.println(graph);
        long t0 = System.currentTimeMillis();
        generateNumKeywords(2);

        BigIndex bi = new BigIndex(graph, ont, CompressFactor.CR);
        System.out.println("Building BigIndex Index time:" + (System.currentTimeMillis() - t0));

        t0 = System.currentTimeMillis();
        Rclique rc = new Rclique(keywords, bi.top);
        System.out.println("Rclique Index building time on Compressed Graph:" + (System.currentTimeMillis() - t0));

        t0 = System.currentTimeMillis();
        ArrayList<RcliqueResult> res2 = rc.semantic(keywords, bi.top);
        System.out.println("Rclique query time on Compressed Graph:" + (System.currentTimeMillis() - t0));

        System.out.println("After decompression: " + res2.size());
    }
}

