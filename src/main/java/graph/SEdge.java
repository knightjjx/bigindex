package graph;

import java.io.Serializable;

/**
 * Created by samjjx on 2017/5/5.
 * This class stores the edge information
 */
public class SEdge implements Serializable {
    private static final long serialVersionUID = 1L;
    private int fromNode;
    private int toNode;

    private float attr;
    public SEdge(){

    }
    public SEdge(int fromNode, int toNode, SEdge hlink, SEdge tlink){
        this.fromNode = fromNode;
        this.toNode = toNode;
    }

    public void SetAttr(int attr){
        this.attr=attr;
    }
    public void SetFromNode(int fnode) {
        this.fromNode = fnode;
    }

    public void SetToNode(int tnode) {
        this.toNode = tnode;
    }


    public int from() {
        return fromNode;
    }

    public int to() {
        return this.toNode;
    }
    public boolean match(Object o) {
        return false;
    }
    public String toString(){
        String str = "source: "+ this.fromNode+"\t"+" dst: "+ this.toNode+"\t"+this.toNode;
        return str;
    }

}
