package graph;

public interface Vertex {

	boolean match(Object other);

	int getID();
}