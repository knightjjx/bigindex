package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by samjjx on 2017/7/24.
 */
public class PartitionConverter {
    HashMap<Integer, Partition> partitions;

    public PartitionConverter(HashMap<Integer, Partition> partitions) {
        this.partitions = partitions;
    }

    public HashMap<Integer, Partition> convert(SGraph graph) {
        Set<Integer> pIDs = partitions.keySet();
        for (int pID : pIDs) {
            Partition p = partitions.get(pID);
            HashSet<SVertex> add = new HashSet<SVertex>();

            for (SVertex v : p.out) {
                for (int to : graph.getChildren(v.id)) {
                    int toPID = graph.rTable.get(to);
                    Partition toP = partitions.get(toPID);
                    if (pID == toPID)
                        continue;
                    if (p.vertexSize() < toP.vertexSize()) {
                        SVertex shiftOut = new SVertex(to, graph.getVertex(to).attr);
                        p.addVertex(shiftOut);
                        p.addEdge(v.id, to);
                        add.add(shiftOut);
                    } else {
                        SVertex shiftIn = new SVertex(v.id, v.attr);
                        toP.addVertex(shiftIn);
                        toP.addEdge(v.id, to);
                        toP.in.add(shiftIn);
                    }
                }
            }
            p.out.addAll(add);
//            for(SVertex v: add){
//                p.addVertex(v.id, v.attr);
//                HashSet<Integer> aParents = graph.getParents(v.id);
//                for(int incIncome: aParents) {
//                    if (p.contains(incIncome)){
//                        p.addEdge(incIncome, v.id);
//                    }
//                }
//                p.out.add(p.getVertex(v.id));
//            }
        }

        for (int pID : pIDs) {
            HashSet<SVertex> delete = new HashSet<SVertex>();
            Partition p = partitions.get(pID);
            for (SVertex v : p.out) {
                if (!isOut(graph, v, pID)) {
                    delete.add(v);
                }
            }
            p.out.removeAll(delete);
        }

        for (int pID : pIDs) {
            HashSet<SVertex> delete = new HashSet<SVertex>();
            Partition p = partitions.get(pID);
            for (SVertex v : p.in) {
                if (!isIn(graph, v, pID)) {
                    delete.add(v);
                }
            }
            p.in.removeAll(delete);
        }
        return this.partitions;
    }

    public boolean isOut(SGraph graph, SVertex vertex, int pid) {
        Partition p = partitions.get(pid);
        for (int to : graph.getChildren(vertex.id)) {
            if (!p.allDigVertices.contains(to)) {
                return true;
            }
        }
        return false;
    }

    public boolean isIn(SGraph graph, SVertex vertex, int pid) {
        Partition p = partitions.get(pid);
        for (int from : graph.getParents(vertex.id)) {
            if (!p.allDigVertices.contains(from)) {
                return true;
            }
        }
        return false;
    }
}
