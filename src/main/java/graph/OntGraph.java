package graph;

import util.IOPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by samjjx on 2017/7/29.
 */
public class OntGraph extends SGraph {
    HashSet<SVertex> markers;
    public HashMap<Integer, Integer> attr2ID;
    int root = 0; //root indicates the root vertex "Thing"


    public OntGraph(String basePath, boolean partition, int root, boolean directed) throws IOException {
        this.vertices = new HashMap<Integer, SVertex>();
        this.allvertices = new HashSet<SVertex>();
        this.allDigVertices = new HashSet<Integer>();
        this.visited = new HashSet<SVertex>();
        this.parents = new HashMap<Integer, HashSet<Integer>>();
        this.attr2ID = new HashMap<Integer, Integer>();
        this.markers = new HashSet<SVertex>();
        this.root = root;
        loadGraphFromVEFile(basePath, partition, directed);
    }

    public boolean loadVertex(String path) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        int count = 0;
        while ((line = reader.readLine()) != null) {
            count++;
            String[] data = line.split("\t");
            OVertex v = new OVertex(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
            this.addVertex(v);

            attr2ID.put(v.attr, v.id);
            if (count % 10000 == 0)
                System.out.println("[Vertex Loading] " + count);
        }
        return true;
    }

    public boolean loadGraphFromVEFile(String basePath, boolean partition, boolean directed) throws IOException {
        loadVertex(basePath + ".v");
        loadEdge(basePath + ".e", directed);
        return true;
    }

    public OntGraph() {
        super();
    }


    public void genMarker(SGraph graph) {
        for (SVertex vertex : graph.allvertices) {
            if (attr2ID.containsKey(vertex.attr)) {
                markers.add(this.getVertex(attr2ID.get(vertex.attr)));
            } else {
                markers.add(this.getVertex(root));
            }
        }
    }

    public OntGraph genTree(int root) {
        OntGraph graph = new OntGraph();
        graph.attr2ID = new HashMap<Integer, Integer>();
        HashSet<Integer> visited = new HashSet<Integer>();
        Queue<Integer> bfsQ = new LinkedList<Integer>();

        bfsQ.add(root);
        OVertex rVertex = new OVertex(root, this.getVertex(root).attr);
        graph.addVertex(rVertex);
        graph.attr2ID.put(rVertex.attr, rVertex.id);

        while (!bfsQ.isEmpty()) {
            int head = bfsQ.remove();
            System.out.println(head);
            if (visited.contains(head))
                continue;
            for (int c : this.getChildren(head)) {
                if(visited.contains(head))
                    continue;
                bfsQ.add(c);
                OVertex cVertex = new OVertex(c, this.getVertex(c).attr);
                graph.addVertex(cVertex);
                graph.attr2ID.put(cVertex.attr, cVertex.id);
                graph.addEdge(head, c);
            }
            visited.add(head);
        }
        return graph;
    }


    /**
    Some config function from bigindex
     */

    public HashMap<Integer, Integer> getSubTree(int root, SGraph top) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        OntGraph ontGraph = this;
        for (int keyword : allK) {
            if (!ontGraph.attr2ID.containsKey(keyword)) {
                config.put(keyword, 0);
            } else {
                ontGraph.attr2ID.get(keyword);
            }
        }
        Queue<Integer> bfsQ = new LinkedList<Integer>();
        bfsQ.add(root);
        HashSet<Integer> visited = new HashSet<Integer>();
        HashSet<Integer> foundedKeywords = new HashSet<Integer>();
        while (!bfsQ.isEmpty()) {
            int head = bfsQ.remove();
            if (visited.contains(head))
                continue;
            for (int c : ontGraph.getChildren(head)) {
                if (visited.contains(c))
                    continue;
            }
            bfsQ.addAll(ontGraph.getChildren(head));
            if (allK.contains(ontGraph.getAttribute(head))) {
                config.put(ontGraph.getAttribute(head), ontGraph.getAttribute(root));
                foundedKeywords.add(ontGraph.getAttribute(head));
            }
            visited.add(head);
        }
        for (int keyword : allK) {
            if (foundedKeywords.contains(keyword))
                continue;
            if (ontGraph.getParents(ontGraph.attr2ID.get(keyword)).isEmpty())
                config.put(keyword, 1);
            config.put(keyword, keyword);
        }
        return config;
    }

    public HashMap<Integer, Integer> allRoot(SGraph top) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        for (int keyword : allK)
            config.put(keyword, 0);
        return config;
    }

    public HashMap<Integer, Integer> generateConfigWithHighDegree(int MulLayer, SGraph top) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        OntGraph ontGraph = this;
        for (int i = 0; i < MulLayer; i++) {
            //System.out.println("[Warn Size]" + allK.size());
            HashSet<Integer> allK_tmp = new HashSet<Integer>();
            for (int keyword : allK) {
                int vid = ontGraph.attr2ID.get(keyword);
                Set<SVertex> parents = ontGraph.getParents(ontGraph.getVertex(vid));
                if (parents.isEmpty()) {
                    config.put(keyword, keyword);
                    config.put(keyword, 1);
//                    allK_tmp.add(keyword);
                } else {
                    int highVid = 0;
                    int max = Integer.MIN_VALUE;
                    for (SVertex p : parents) {
                        int pid = p.id;
                        if (max < ontGraph.outDegree(pid)) {
                            max = ontGraph.outDegree(pid);
                            highVid = pid;
                        }
                    }
                    int attr = ontGraph.getVertex(highVid).attr;
                    config.put(keyword, attr);
//                    config.put(keyword, 1);
                    allK_tmp.add(attr);
                }
            }
            allK = allK_tmp;
        }
        return config;
    }

    public HashMap<Integer, Integer> generateConfigSimple(int MulLayer, SGraph top) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        OntGraph ontGraph = this;
        for (int i = 0; i < MulLayer; i++) {
            HashSet<Integer> allK_tmp = new HashSet<Integer>();
            for (int keyword : allK) {
                int vid = ontGraph.attr2ID.get(keyword);

                Set<SVertex> parents = ontGraph.getParents(ontGraph.getVertex(vid));
                if (parents.isEmpty()) {
                    config.put(keyword, keyword);
                    allK_tmp.add(keyword);
                } else {
                    int p = parents.iterator().next().attr;
                    config.put(keyword, p);
                    allK_tmp.add(p);
                }
            }
            allK = allK_tmp;
        }
        return config;
    }


    public HashMap<Integer, Integer> generateConfigRandom(SGraph top){
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        Random random = new Random();

        for(int keyword: allK){
            int rand_hop = random.nextInt(3);
            int gen_keyword = generateMulOne(keyword, rand_hop);
            config.put(keyword, gen_keyword);
        }
        return config;
    }
    public int generateMulOne(int keyword, int layer){
        if(layer == 0)
            return keyword;
        int vid = this.attr2ID.get(keyword);
        Set<SVertex> parents = this.getParents(this.getVertex(vid));
        int tmp_keyword;
        if (parents.isEmpty()) {
            tmp_keyword = keyword;
        } else {
            int p = parents.iterator().next().attr;
            tmp_keyword = p;
        }
        layer--;
        return generateMulOne(tmp_keyword, layer);
    }
}
