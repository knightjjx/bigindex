package graph;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by samjjx on 2017/5/5.
 * This class is for vertices information
 */
public class SVertex implements Vertex, Serializable {
    public int id; // node ID
    public int attr;
    public int attr_ori;

    // MNK and DNP are for Blinks

    //TODO: revise following codes
    private HashMap<Integer, Integer> MNK; //keywords - > distance
    private HashMap<Integer, SVertex> MNKFirst; //Keywords - > vertex first
    public int DNP;

    protected SEdge firstin = null; // first in node
    protected SEdge firstout = null; // first out node
    private int rank;
    private boolean visited;
    public int bmap = -1; // map to the vertex in the compressed graph
    public SVertex smap = null; // map to the vertex of the SCC
    public HashSet<Integer> dmap = null; // map to the vertex of the original graph. null -> already in the original graph
    private int index = Integer.MAX_VALUE;
    private int lindex = Integer.MAX_VALUE;
    private boolean onStack = false;

    public SVertex(int id, int attr) {
        this.id = id;
        this.attr = attr;
        this.rank = Integer.MIN_VALUE;
        visited = false;
        this.dmap = new HashSet<Integer>();
        this.dmap.add(this.id);
        this.bmap = this.id;
        MNK = new HashMap<Integer, Integer>();
        MNKFirst = new HashMap<Integer, SVertex>();
    }

    public SVertex() {
    }
    public HashMap<Integer, Integer> getMNK(){
        return this.MNK;
    }
    public HashMap<Integer, SVertex> getMNKFirst(){
        return this.MNKFirst;
    }
    public int getRank(){
        return rank;
    }
    public void setRank(int rank){
        this.rank = rank;
    }
    public boolean isVisited(){
        return visited;
    }
    public void setVisited(boolean visited){
        this.visited = visited;
    }

    public boolean isOnStack(){
        return onStack;
    }

    public void setOnStack(boolean isStack){
        this.onStack = isStack;
    }
    public int getIndex(){
        return index;
    }

    public void setIndex(int index){
        this.index = index;
    }

    public int getLindex(){
        return lindex;
    }
    public void setLindex(int lindex){
        this.lindex = lindex;
    }
    public void reset() {
        this.rank = Integer.MIN_VALUE;
        this.visited = false;
        this.onStack = false;
        this.index = Integer.MAX_VALUE;
        this.lindex = Integer.MAX_VALUE;
        this.bmap = -1;
        this.smap = null;
    }

    public boolean match(Object other) {
        return false;
    }

    public SEdge GetFirstIn() {
        return this.firstin;
    }

    public SEdge GetFirstOut() {
        return this.firstout;
    }

    public void SetID(int ID) {
        this.id = ID;
    }

    public void SetFirstIn(SEdge firstin) {
        this.firstin = firstin;
    }

    public void SetFirstOut(SEdge firstout) {
        this.firstout = firstout;
    }

    public int getID() {
        return this.id;
    }

    public int getAttr() {
        return this.attr;
    }

    public void setAttr(int gAttr) {
        this.attr = gAttr;
    }

    public String toString() {
        return "" + this.id + "\t" + this.attr;
    }

    public SVertex copy() {
        return new SVertex(id, attr);
    }
}
