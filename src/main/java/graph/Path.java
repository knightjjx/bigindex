package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Path{
    public ArrayList<Integer> path;
    public HashMap<Integer, Integer> content2root;
    public HashSet<Integer> droot;
    public int root;
    public Path(ArrayList<Integer> path){
        this.path = path;
        content2root = new HashMap<Integer, Integer>();
        droot = new HashSet<Integer>();
    }

    public void addVertex(int vid){
        path.add(vid);
    }

    public void setRoot(int root){
        this.root = root;
    }

    public void setDroot(HashSet<Integer> droot){
        this.droot = droot;
    }
}