package graph;

import bsim.PaigeTarjan;

import java.util.ArrayList;

/**
 * Created by samjjx on 2017/5/5.
 * This class is for the hierarchy index
 */
public class MGraph {
    ArrayList<SGraph> hierarchy = new ArrayList<SGraph>();
    public MGraph(SGraph graph, int layer) throws CloneNotSupportedException {
        hierarchy.add(graph);
        PaigeTarjan pt = new PaigeTarjan();
        for(int i=0;i<layer;i++) {
            SGraph oriGraph = hierarchy.get(i);
            oriGraph.generalization(oriGraph.genGenralization());
            hierarchy.add(pt.bisimulationGeneral(hierarchy.get(i)));
        }
    }
}