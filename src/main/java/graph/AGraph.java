package graph;

import java.util.*;

/**
 * Created by samjjx on 2017/8/3.
 */
public class AGraph {
    public int layer;
    public HashMap<Integer, Integer> allvertices; // id -> attr
    public HashMap<Integer, Integer> v2prev; // vid -> prev.vid
    public HashMap<Integer, Integer> prev2v;
    public HashMap<Integer, HashSet<Integer>> edges;
    public HashMap<Integer, HashSet<Integer>> invert_edges;
    public int root = 0;
    public Queue<Integer> decompressor_order;
    public HashSet<Integer> content_vertex;

    public AGraph(int layer, int root) {
        this.layer = layer;
        allvertices = new HashMap<Integer, Integer>();
        edges = new HashMap<Integer, HashSet<Integer>>();
        invert_edges = new HashMap<Integer, HashSet<Integer>>();
        v2prev = new HashMap<Integer, Integer>();
        prev2v = new HashMap<Integer, Integer>();
        this.root = root;
        content_vertex = new HashSet<Integer>();
    }

    public AGraph(int layer){
        this.layer = layer;
        allvertices = new HashMap<Integer, Integer>();
        edges = new HashMap<Integer, HashSet<Integer>>();
        invert_edges = new HashMap<Integer, HashSet<Integer>>();
        v2prev = new HashMap<Integer, Integer>();
        prev2v = new HashMap<Integer, Integer>();
        content_vertex = new HashSet<Integer>();
    }
    public void bfsOrder() {
        decompressor_order = new LinkedList<Integer>();
        Queue<Integer> traversal = new LinkedList<Integer>();
        traversal.add(root);
        decompressor_order.add(root);
        while (!traversal.isEmpty()) {
            int head = traversal.remove();
            traversal.addAll(edges.get(head));
            decompressor_order.add(head);
        }
    }

    public void leafOrder() {
        decompressor_order = new LinkedList<Integer>();
        Queue<Integer> traversal = new LinkedList<Integer>();
        HashSet<Integer> visited = new HashSet<Integer>();
        for (int v : allvertices.keySet()) {
            if (edges.get(v).isEmpty()) {
                traversal.add(v);
            }
        }
        while (!traversal.isEmpty() && decompressor_order.size() != allvertices.size()) {
            int head = traversal.remove();
            if (visited.contains(head))
                continue;
            visited.add(head);
            traversal.addAll(invert_edges.get(head));
            decompressor_order.add(head);
        }
    }

    public ArrayList<Path> decompose() {
        ArrayList<Path> res = new ArrayList<Path>();
        for (int v : content_vertex) {
            Path path = new Path(new ArrayList<Integer>());
            while (v != root) {
                path.addVertex(v);
                v = this.invert_edges.get(v).iterator().next();
            }
            path.addVertex(root);
            path.setRoot(root);
            res.add(path);
        }
        return res;
    }

    public void addEdge(int src, int dst) {
        edges.get(src).add(dst);
        invert_edges.get(dst).add(src);
    }

    public void addVertex(int v, int attr) {
        allvertices.put(v, attr);
        if (!edges.containsKey(v)) {
            edges.put(v, new HashSet<Integer>());
            invert_edges.put(v, new HashSet<Integer>());
        }
    }

    public int resultSize() {
        return allvertices.size();
    }

    public AGraph copy() {
        AGraph nGraph = new AGraph(layer, this.root);
        nGraph.allvertices.putAll(this.allvertices);
        nGraph.prev2v.putAll(this.prev2v);
        nGraph.v2prev.putAll(this.v2prev);

        for (int src : edges.keySet()) {
            HashSet<Integer> tmp = new HashSet<Integer>();
            tmp.addAll(this.edges.get(src));
            nGraph.edges.put(src, tmp);
        }

        for (int src : invert_edges.keySet()) {
            HashSet<Integer> tmp = new HashSet<Integer>();
            tmp.addAll(this.invert_edges.get(src));
            nGraph.invert_edges.put(src, tmp);
        }
        return nGraph;
    }

    /**
     * This method is for pruning
     *
     * @param vid        decompress vertex
     * @param prevAns    compressed answer graph
     * @param prevVertex the vertex in compressed graph
     * @param graph      lower layer
     * @return true if it is candidate answer
     */
    public boolean qualification(int vid, AGraph prevAns, int prevVertex, SGraph graph) {
        HashSet<Integer> preEdges = prevAns.edges.get(prevVertex);
        HashSet<Integer> lowerEdges = graph.getChildren(vid);
        for (int dst : preEdges) {
            if (!prev2v.containsKey(dst))
                continue;
            if (!lowerEdges.contains(prev2v.get(dst))) {
                return false;
            }
        }

        preEdges = prevAns.invert_edges.get(prevVertex);
        lowerEdges = graph.getParents(vid);
        for (int dst : preEdges) {
            if (!prev2v.containsKey(dst))
                continue;
            if (!lowerEdges.contains(prev2v.get(dst))) {
                return false;
            }
        }

        return true;
    }

    public void expandVertex(int vid, int attr, AGraph prevAns, int prevVertex) {
        addVertex(vid, attr);
        v2prev.put(vid, prevVertex);
        prev2v.put(prevVertex, vid);
        if (this.allvertices.size() == 1)
            return;
        HashSet<Integer> oriEdges = prevAns.edges.get(prevVertex);
        for (int dst : oriEdges) {
            if (!prev2v.containsKey(dst))
                continue;
            this.addEdge(vid, prev2v.get(dst));
        }

        oriEdges = prevAns.invert_edges.get(prevVertex);
        for (int dst : oriEdges) {
            if (!prev2v.containsKey(dst))
                continue;
            this.addEdge(prev2v.get(dst), vid);
        }
    }

    public boolean verification(HashSet<Integer> keywords) {
        Set<Integer> ids = allvertices.keySet();

        for (int key : keywords) {
            boolean mark = false;
            for (int id : ids) {
                if (allvertices.get(id) == key) {
                    mark = true;
                    break;
                }
            }
            if (!mark) {
                return false;
            }
        }
        return true;
    }


    public String print() {
        String res = "===============Answer Graph information==========\nEdges:\n";
        res += "Layer\t" + layer + "\n";
        Set<Integer> keyset = edges.keySet();
        for (int src : keyset) {
            res += (src + "\t");
            for (int dst : edges.get(src)) {
                res += (dst + "\t");
            }
            res += "\n";
        }
        res += "\nVertices:\n";
        keyset = allvertices.keySet();
        for (int v : keyset) {
            res += (v + "\t" + allvertices.get(v) + "\n");
        }
        return res;
    }

    public String toString(){
        String res = "===============Answer Graph information==========\nVertices:\n";
        res += allvertices.size()+"\n";
        for (int v : allvertices.keySet()) {
            res += v + "\t" + allvertices.get(v)+"\n";
        }
        res = res + "\nEdges:\n";
        for(int src: edges.keySet()){
            res += src;
            for(int dst: edges.get(src)){
                res += "\t" + dst;
            }
            res += "\n";
        }
        return res;
    }
}


