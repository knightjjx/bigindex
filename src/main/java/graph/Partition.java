package graph;

import algorithms.Cursor;

import java.util.*;

/**
 * Created by samjjx on 2017/7/17.
 */
public class Partition extends SGraph {
    public int pID;
    public HashSet<SVertex> out = new HashSet<SVertex>();
    public HashSet<SVertex> in = new HashSet<SVertex>();
    public HashMap<Integer, Queue<SVertex>> LKN = new HashMap<Integer, Queue<SVertex>>(); // keyword => vertex list
    HashMap<Integer, HashSet<SVertex>> LKN_Dis = new HashMap<Integer, HashSet<SVertex>>();

    public HashMap<SVertex, ArrayList<SVertex>> LPN = new HashMap<SVertex, ArrayList<SVertex>>(); // vertex => portal
    HashMap<SVertex, HashSet<SVertex>> LPN_Dis = new HashMap<SVertex, HashSet<SVertex>>();

    public Partition(int pID, SGraph graph) {
        this.pID = pID;
        this.rTable = graph.rTable;
        for (SVertex v : graph.allvertices) {
            if (rTable.get(v.getID()) == pID) {
                SVertex nVertex = new SVertex(v.id, v.attr);
                this.addVertex(nVertex);
            }
        }
        HashMap<Integer, HashSet<Integer>> allEdge = graph.children;
        for(int from: allEdge.keySet()){
            for(int to: allEdge.get(from)){
                if (this.allDigVertices.contains(from) && !this.allDigVertices.contains(to)) {
                    out.add(this.getVertex(from));
                }
                if (!this.allDigVertices.contains(from) && this.allDigVertices.contains(to)) {
                    in.add(this.getVertex(to));
                }
                if (this.allDigVertices.contains(from) && this.allDigVertices.contains(to)) {
                    this.addEdge(from, to);
                }
            }
        }
    }

    public String toString() {
        String res = "";
        res += "Partition ID\t=>\t" + pID + "\n";
        res += "Partition information\n";
        res += "Partition all vertices\n";
        for (SVertex v : this.allvertices) {
            res += "vid\t" + v.id + "\t" + v.attr + "\n";
        }
        res += "Partition out vertices\n";
        for (SVertex v : this.out) {
            res += "vid\t" + v.id + "\t" + v.attr + "\n";
        }
        res += "Partition in vertices\n";
        for (SVertex v : this.in) {
            res += "vid\t" + v.id + "\t" + v.attr + "\n";
        }
        return res;
    }

    public void BuildIndex() {
        for(SVertex v: allvertices){
            v.DNP = Integer.MAX_VALUE;
            if (!this.LKN.containsKey(v.attr)) {
                LKN.put(v.attr, new LinkedList<SVertex>());
                LKN_Dis.put(v.attr, new HashSet<SVertex>());
            }
            LKN.get(v.attr).add(v);
            LKN_Dis.get(v.attr).add(v);
        }

        for (int k : allKeywords) {
            for (SVertex v : this.allvertices) {
                v.getMNK().put(k, Integer.MAX_VALUE);
            }
            //init the origin set
            Queue<SVertex> kReachable = LKN.get(k);
            for (SVertex tmp : kReachable) {
                tmp.getMNK().put(k, 0);
                tmp.getMNKFirst().put(k,tmp);
            }

            HashSet<SVertex> kReachable_Dis = LKN_Dis.get(k);
            Queue<SVertex> tmp = new LinkedList<SVertex>();
            while(!kReachable.isEmpty()) {
                SVertex head = kReachable.remove();
                tmp.add(head);
                for (SVertex v : this.getParents(head)) {
                    int v_MNK = v.getMNK().get(k);

                    if(v_MNK > (head.getMNK().get(k) + 1)) {
                        v_MNK = head.getMNK().get(k) + 1;
                        v.getMNKFirst().put(k,head);
                    }
                    v.getMNK().put(k,v_MNK);

                    if (kReachable_Dis.contains(v))
                        continue;
                    kReachable.add(v);
                    kReachable_Dis.add(v);
                }
            }
            LKN.put(k,tmp);
        }

        for (SVertex p : out) {
            if (!LPN.containsKey(p)) {
                LPN.put(p, new ArrayList<SVertex>());
                LPN_Dis.put(p, new HashSet<SVertex>());
            }
            ArrayList<SVertex> pReachable = LPN.get(p);
            HashSet<SVertex> pReachable_Dis = LPN_Dis.get(p);

            pReachable.add(p);
            p.DNP = 0;
            for (int i = 0; i < pReachable.size(); i++) {
                SVertex head = pReachable.get(i);
                for (SVertex v : this.getParents(head)) {
                    v.DNP = v.DNP < (head.DNP + 1) ? v.DNP : head.DNP + 1;
                    if (pReachable_Dis.contains(v))
                        continue;
                    pReachable.add(v);
                    pReachable_Dis.add(v);
                }
            }
        }
    }

    public void remove(SVertex vertex) {
        this.removeVertex(vertex.getID());
        if (this.out.contains(vertex)) {
            this.out.remove(vertex);
        }
        if (this.in.contains(vertex)) {
            this.in.remove(vertex);
        }
    }

    public Cursor getCursor(int keyword) {
        return new Cursor(LKN.get(keyword).iterator(), 0, pID, keyword);
    }
}