package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Created by samjjx on 2017/5/13.
 */
public class SCC extends SGraph {
    public HashMap<Integer, HashSet<SVertex>> scc2ori = new HashMap<Integer, HashSet<SVertex>>();
    public HashMap<Integer, Integer> ori2scc = new HashMap<Integer, Integer>();
    public HashSet<SVertex> scc = new HashSet<SVertex>();
    public SGraph origraph = null;
    public int timer = 0;

    public SCC(SGraph graph) {
        this.origraph = graph;
        computeSCC();
    }

    private void computeSCC() {
        for (SVertex v : origraph.allVertices()) {
            v.reset();
        }
        for (SVertex v : origraph.allVertices()) {
            if (v.isOnStack())
                continue;
            if (v.getIndex() != Integer.MAX_VALUE)
                continue;
            compute(v);
        }

        HashSet<String> edges = new HashSet<String>();
        Set<Integer> vertices = origraph.children.keySet();
        for (int v : vertices) {
            for (int u : origraph.children.get(v)) {
                if (origraph.getVertex(v).smap.getID() == origraph.getVertex(u).smap.getID()) {
                    continue;
                }
                edges.add(origraph.getVertex(v).smap.getID() + "#" + origraph.getVertex(u).smap.getID());
            }
        }
        for (String e : edges) {
            String[] data = e.split("#");
            this.addEdge(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
        }
    }

    private Stack<SVertex> stack = new Stack<SVertex>();
    private Stack<SVertex> sStack = new Stack<SVertex>();

    private void compute(SVertex v) {
        v.setIndex(timer);
        v.setLindex(timer);
        timer++;
        stack.push(v);
        sStack.push(v);
        v.setOnStack(true);
        l:
        while (!sStack.empty()) {
            SVertex top = sStack.peek();
            for (SVertex w : origraph.getChildren(top)) {
                if (w.getIndex() == Integer.MAX_VALUE) {
                    w.setIndex(timer);
                    w.setLindex(timer);
                    timer++;
                    sStack.push(w);
                    stack.push(w);
                    continue l;
                }
                if (w.isVisited())
                    continue;
                top.setLindex(w.getLindex() < top.getLindex() ? w.getLindex() : top.getLindex());
                if (w.isOnStack()) {
                    top.setLindex(w.getIndex() < top.getLindex() ? w.getIndex() : top.getLindex());
                }
            }

            if (top.getLindex() == top.getIndex()) {
                SVertex w;
                HashSet<SVertex> component = new HashSet<SVertex>();
                SVertex hyperVertex = new SVertex(top.getID(), top.getAttr());
                this.addVertex(hyperVertex);
                do {
                    w = stack.pop();
                    w.setVisited(true);
                    w.setOnStack(false);
                    w.smap = top;
                    ori2scc.put(w.id, hyperVertex.id);
                    component.add(w);
                } while (w != top);
                scc2ori.put(hyperVertex.id, component);
                if (component.size() > 1) {
                    scc.add(hyperVertex);
                }
            }
            sStack.pop();
        }
    }

    public String toString() {
        String res = "";
        res = res + "================Graph information=============\n";
        res = res + "layer\t"+layer+"\n";
        res = res + "Vertices size:\t" + vertexSize() + "\n";
        for (SVertex v : allvertices) {
            res += ("vid\t" + v.id + "\tattr\t" + v.attr + "\tdmaps\t" + v.dmap + "\n");
        }

        for (int src : vertices.keySet()) {
            for (int dst : getChildren(src)) {
                res += ("src\t" + src + "\tdst\t" + dst + "\n");
            }
        }
        res += ("[vindex]" + this.vindex);
        return res;
    }
}
