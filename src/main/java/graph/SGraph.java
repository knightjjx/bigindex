package graph;

import config.SgraphIndex;
import util.IOPack;

import java.io.*;
import java.util.*;

/**
 * Created by samjjx on 2017/5/5.
 * This class stores a single graph
 */
public class SGraph implements Serializable, Cloneable {
    public HashMap<Integer, SVertex> vertices;
    public HashSet<SVertex> allvertices;
    public HashSet<Integer> allDigVertices;
    public HashSet<SVertex> visited;
    private int edgeSize = 0;
    private HashSet<SVertex> WF;
    public int maxRank = Integer.MIN_VALUE;
    public int minRank = Integer.MAX_VALUE;

    public HashMap<Integer, Integer> bmaps;
    public HashMap<Integer, ArrayList<Integer>> dmaps;

    public HashMap<Integer, HashSet<Integer>> parents = new HashMap<Integer, HashSet<Integer>>();
    public HashMap<Integer, HashSet<Integer>> children = new HashMap<Integer, HashSet<Integer>>();

    public HashMap<Integer, Integer> rTable = new HashMap<Integer, Integer>();
    public HashSet<Integer> allKeywords = new HashSet<Integer>();
    public HashMap<Integer, Integer> freq_keyword = new HashMap<Integer, Integer>();
    public int vindex = 0;

    public SGraph prev = null, next = null;
    public int layer;
    public int global_count = 0;
    public int total = 0;

    public HashMap<Integer, HashSet<Integer>> one_hop_index;

    public SgraphIndex index_config = SgraphIndex.Neighbors;


    public SGraph(String basePath, boolean partition) throws IOException {
        this.vertices = new HashMap<Integer, SVertex>();
        this.allvertices = new HashSet<SVertex>();
        this.allDigVertices = new HashSet<Integer>();
        this.visited = new HashSet<SVertex>();
        this.WF = new HashSet<SVertex>();
        this.parents = new HashMap<Integer, HashSet<Integer>>();
        dmaps = new HashMap<Integer, ArrayList<Integer>>();
        bmaps = new HashMap<Integer, Integer>();
        loadGraphFromVEFile(basePath, partition, true);
        layer = 1;
        if (index_config == SgraphIndex.Neighbors) {
            construct_one_hop_index();
            System.out.println("Done");
            System.out.println(one_hop_index.size());
        }
        computeFreq();
    }
    public SGraph(String basePath, boolean partition, boolean directed) throws IOException {
        this.vertices = new HashMap<Integer, SVertex>();
        this.allvertices = new HashSet<SVertex>();
        this.allDigVertices = new HashSet<Integer>();
        this.visited = new HashSet<SVertex>();
        this.WF = new HashSet<SVertex>();
        this.parents = new HashMap<Integer, HashSet<Integer>>();
        dmaps = new HashMap<Integer, ArrayList<Integer>>();
        bmaps = new HashMap<Integer, Integer>();
        loadGraphFromVEFile(basePath, partition, directed);
        layer = 1;
        if (index_config == SgraphIndex.Neighbors) {
            construct_one_hop_index();
            System.out.println("Done");
            System.out.println(one_hop_index.size());
        }
        computeFreq();
    }

    public void construct_one_hop_index() {
        one_hop_index = new HashMap<Integer, HashSet<Integer>>();
        for (int v : allDigVertices) {
            HashSet<Integer> index_tmp = new HashSet<Integer>();

            for (int u : getChildren(v)) {
                index_tmp.add(getAttribute(u));
            }
            one_hop_index.put(v, index_tmp);
        }
    }

    public SGraph() {
        this.vertices = new HashMap<Integer, SVertex>();
        this.allvertices = new HashSet<SVertex>();
        this.allDigVertices = new HashSet<Integer>();
        this.visited = new HashSet<SVertex>();
        this.WF = new HashSet<SVertex>();
        this.parents = new HashMap<Integer, HashSet<Integer>>();
    }

    public void reset() {
        for (SVertex v : allvertices) {
            v.reset();
        }
        this.maxRank = Integer.MIN_VALUE;
        this.minRank = Integer.MAX_VALUE;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public void computeFreq() {
        for (SVertex v : allvertices) {
            if (!freq_keyword.containsKey(v.attr)) {
                freq_keyword.put(v.attr, 1);
            } else {
                freq_keyword.put(v.attr, freq_keyword.get(v.attr) + 1);
            }
        }
    }

    public int getAttribute(int vid) {
        return this.getVertex(vid).attr;
    }

    /**
     * @return
     */
    public void generalization(HashMap<Integer, Integer> config) {
        Set<Integer> vSet = vertices.keySet();
        for (int v : vSet) {
            SVertex vertex = this.getVertex(v);
            if (config.containsKey(vertex.getAttr())) {
                vertex.attr_ori = vertex.attr;
                vertex.setAttr(config.get(vertex.getAttr()));
            } else {
                vertex.attr_ori = vertex.attr;
            }
        }
        allKeywords = new HashSet<Integer>();
        for (int v : vSet) {
            SVertex vertex = this.getVertex(v);
            allKeywords.add(vertex.attr);
        }
    }

    public HashMap<Integer, Integer> genGenralization() {
        return new HashMap<Integer, Integer>();
    }

    public boolean addVertex(SVertex vertex) {
        if (this.allDigVertices.contains(vertex.id))
            return false;
        this.vertices.put(vertex.getID(), vertex);
        this.allvertices.add(vertex);
        this.allDigVertices.add(vertex.getID());
        if (!this.allKeywords.contains(vertex.attr)) {
            this.allKeywords.add(vertex.attr);
        }
        this.parents.put(vertex.id, new HashSet<Integer>());
        this.children.put(vertex.id, new HashSet<Integer>());
        this.vindex++;
        return true;
    }

    public SVertex addVertex(int attr) {
        SVertex v = new SVertex(this.vindex + 1, attr);
        addVertex(v);
        return v;
    }

    public boolean addVertex(int vid, int attr) {
        SVertex v = new SVertex(vid, attr);
        return this.addVertex(v);
    }

    public boolean addEdge(int from, int to) {
        this.edgeSize++;
        if (!this.parents.containsKey(to)) {
            this.parents.put(to, new HashSet<Integer>());
        }
        if (!this.children.containsKey(from)) {
            this.children.put(from, new HashSet<Integer>());
        }
        this.parents.get(to).add(from);
        this.children.get(from).add(to);
        return true;
    }

    public int getRadius(SVertex vertex) {
        return 0;
    }

    public int getDiameter() {
        return 0;
    }


    public HashSet<Integer> getChildren(int vertex) {
        return this.children.get(vertex);
    }

    public HashSet<SVertex> getChildren(SVertex vertex) {
        HashSet<SVertex> res = new HashSet<SVertex>();
        for (int v : this.children.get(vertex.id))
            res.add(vertices.get(v));
        return res;
    }

    public Set<SVertex> getParents(SVertex vertex) {
        HashSet<SVertex> pSet = new HashSet<SVertex>();
        HashSet<Integer> dpSet = getParents(vertex.id);
        for (int vid : dpSet) {
            pSet.add(vertices.get(vid));
        }
        return pSet;
    }

    public int outDegree(int vid) {
        return children.get(vid).size();
    }

    public int inDegree(int vid) {
        return parents.get(vid).size();
    }

    public HashSet<Integer> getParents(int vertexID) {
        return parents.get(vertexID);
    }

    public HashSet<Integer> getCopyParents(int vertexID) {
        HashSet<Integer> res = new HashSet<Integer>();
        res.addAll(parents.get(vertexID));
        return res;
    }

    public Set<SVertex> getParentsByID(int vid) {
        return getParents(getVertex(vid));
    }

    public HashSet<Integer> getNeighbours(int vertex) {
        HashSet<Integer> neighbours = getChildren(vertex);
        neighbours.addAll(this.getParents(vertex));
        return neighbours;
    }


    public void display(int limit) {

    }

    public int edgeSize() {
        return this.edgeSize;
    }

    public int vertexSize() {
        return this.vertices.size();
    }

    public int graphSize(){
        return this.vertices.size() + this.edgeSize;
    }

    public SVertex getVertex(int vID) {
        return this.vertices.get(vID);
    }

    public SVertex getRandomVertex() {
        return null;
    }

    public HashMap<Integer, HashSet<Integer>> allEdges() {
        return children;
    }

    public HashSet<SVertex> allVertices() {
        return allvertices;
    }


    public void initRank(SCC scc) {
        computeWF(scc);
        System.out.println("[WF size]" + this.WF.size());
        System.out.println("Finish computing Well Founded case.");
        total = scc.vertexSize();
        for (SVertex v : scc.allvertices) {
            if (v.isVisited())
                continue;
            computeRank(scc, v);
        }
        computeAllRank(scc);
        System.out.println("Finish computing rank");
    }

    public void computeWF(SCC scc) {
        Set<Integer> keyset = scc.scc2ori.keySet();
        Queue<SVertex> iniNWF = new LinkedList<SVertex>();
        HashSet<SVertex> allNWF = new HashSet<SVertex>();
        iniNWF.addAll(scc.scc);
        for (int key : keyset) {
            if (scc.scc2ori.get(key).size() != 1) {
                iniNWF.add(scc.getVertex(key));
                allNWF.add(scc.getVertex(key));
            }
        }
        while (!iniNWF.isEmpty()) {
            SVertex head = iniNWF.remove();
            if (scc.parents.containsKey(head)) {
                for (int v : scc.parents.get(head)) {
                    if (allNWF.contains(v))
                        continue;
                    iniNWF.add(scc.getVertex(v));
                    allNWF.add(scc.getVertex(v));
                }
            }
        }

        this.WF = (HashSet<SVertex>) scc.allvertices.clone();
        this.WF.removeAll(allNWF);
    }

    public void printRank() {
        for (SVertex v : allVertices()) {
            System.out.println(v.getID() + "\t" + v.getRank());
        }
    }

    public int computeRank(SCC scc, SVertex top) {
        int max = Integer.MIN_VALUE;
        global_count++;
        if (global_count % 10000 == 0) {
            System.out.println("[Total]" + total);
            System.out.println("[Currentt]" + global_count);
        }
        if (!scc.hasChildren(top)) {
            if (this.WF.contains(top)) {
                top.setRank(0);
                max = max > top.getRank() + 1 ? max : top.getRank() + 1;
                top.setVisited(true);
                return max;
            } else {
                top.setRank(Integer.MIN_VALUE);
            }
        } else {
            for (SVertex child : scc.getChildren(top)) {
                int childRank;
                if (child.isVisited()) {
                    childRank = child.getRank() + 1;
                } else {
                    childRank = computeRank(scc, child);
                }
                max = max > childRank ? max : childRank;
            }
            top.setRank(top.getRank() > max ? top.getRank() : max);
            max = this.WF.contains(top) ? top.getRank() + 1 : top.getRank();
        }
        top.setVisited(true);
        return max;
    }

    public void computeAllRank(SCC scc) {
        for (SVertex u : scc.allvertices) {
            for (SVertex ori : scc.scc2ori.get(u.id)) {
                ori.setRank(u.getRank());
                maxRank = maxRank > u.getRank() ? maxRank : u.getRank();
                minRank = minRank < u.getRank() ? minRank : u.getRank();
            }
        }
    }

    public boolean loadRTable(String path) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split("\t");
            rTable.put(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
//            rTable.put(Integer.parseInt(data[0]), 1);
        }
        return true;
    }


    public boolean loadVertex(String path) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        int count = 0;
        while ((line = reader.readLine()) != null) {
            count++;
            String[] data = line.split("\t");
            SVertex v = new SVertex(Integer.parseInt(data[0]), Integer.parseInt(data[1]));

//            SVertex v = new SVertex(Integer.parseInt(data[0]), 1);
            this.addVertex(v);
//            if (count % 10000 == 0) {
//                System.out.println("[Vertex loading] " + count);
//            }
        }
        return true;
    }

    public boolean loadEdge(String path, boolean directed) throws IOException {
        IOPack io = new IOPack();
        BufferedReader reader = io.getBufferInstance(path);
        String line = "";
        int count = 0;
        while ((line = reader.readLine()) != null) {
            count++;
            String[] data = line.split("\t");
            SVertex source = this.getVertex(Integer.parseInt(data[0]));
            SVertex target = this.getVertex(Integer.parseInt(data[1]));
            if (directed)
                try {
                    this.addEdge(source.id, target.id);
                } catch (java.lang.NullPointerException e) {
                    System.out.println(data[0] + "\t" + data[1] + "\n");
                }
            else {
                this.addEdge(source.id, target.id);
                this.addEdge(target.id, source.id);
            }
//            if (count % 100000 == 0)
//                System.out.println("[Edge loading] " + count);
        }
        return true;
    }

    public boolean loadGraphFromVEFile(String basePath, boolean partition, boolean directed) throws IOException {
        if (partition) {
            loadRTable(basePath + ".r");
        }
        loadVertex(basePath + ".v");
        System.out.println("finish loading vertices");
        //TODO:read the .e file
        loadEdge(basePath + ".e", directed);
        System.out.println("finish loading edges");
        return true;
    }

    public void clear() {

    }

    public void clearEdges() {

    }

    public boolean contains(int vertexID) {
        return contains(this.getVertex(vertexID));
    }

    public boolean contains(SVertex v) {
        return this.allvertices.contains(v);
    }

    public boolean contains(SVertex from, SVertex to) {
        return contains(from.getID(), to.getID());
    }

    public boolean contains(int fromID, int toID) {
        if (!this.parents.containsKey(toID) || !this.children.containsKey(fromID)) {
            return false;
        }
        return this.children.get(fromID).contains(toID);
    }


    public void setVertexVisited(SVertex sVertex) {
        sVertex.setVisited(true);
        this.visited.add(sVertex);
    }

    public boolean allVisited() {
        return this.visited.size() == this.vertexSize();
    }

    public int degree(SVertex vertex) {
        return 0;
    }

    public Set<SEdge> getEdges(SVertex vertex1, SVertex vertex2) {
        return null;
    }

    public SEdge getEdge(SVertex vertex1, SVertex vertex2) {
        return null;
    }

    public boolean hasCycles() {
        return false;
    }

    public boolean hasChildren(SVertex v) {
        if (!children.containsKey(v.id) || children.get(v.id).size() == 0)
            return false;
        return true;
    }

    public boolean removeEdge(SEdge edge) {
        removeEdge(edge.from(), edge.to());
        return true;
    }

    public boolean removeEdge(int from, int to) {
        this.parents.get(to).remove(from);
        this.children.get(from).remove(to);
        edgeSize--;
        return true;
    }

    public boolean removeVertex(int vid) {
        SVertex vertex = this.getVertex(vid);
        this.vertices.remove(vertex.getID());
        this.allvertices.remove(vertex);
        for (int dst : this.children.get(vid)) {
            this.parents.get(dst).remove(vid);
            edgeSize--;
        }

        for (int src : this.parents.get(vid)) {
            this.children.get(src).remove(vid);
            edgeSize--;
        }
        this.parents.remove(vid);
        this.children.remove(vid);
        return true;
    }

    public int computeDist(int vid, int uid) {
        SVertex v = this.getVertex(vid);
        SVertex u = this.getVertex(uid);
        int dist = 0;
        Queue<SVertex> reachVset = new LinkedList<SVertex>();
        HashSet<SVertex> visited = new HashSet<SVertex>();
        reachVset.add(v);
        while (!reachVset.isEmpty()) {
            Queue<SVertex> tmp = new LinkedList<SVertex>();
            while (!reachVset.isEmpty()) {
                SVertex head = reachVset.remove();
                visited.add(head);
                for (SVertex c : getChildren(head)) {
                    if (!visited.contains(c))
                        tmp.add(c);
                    if (c == u)
                        return dist;
                }
            }
            reachVset = tmp;
            dist++;
        }
        return Integer.MIN_VALUE;
    }

    public SGraph clone() {
        SGraph nGraph = new SGraph();
        nGraph.layer = this.layer;
        for (SVertex v : this.allvertices) {
            nGraph.addVertex(v.copy());
        }

        nGraph.edgeSize = this.edgeSize;
        nGraph.maxRank = this.maxRank;
        nGraph.minRank = this.minRank;
        nGraph.vindex = this.vindex;

        nGraph.parents = new HashMap<Integer, HashSet<Integer>>();
        nGraph.children = new HashMap<Integer, HashSet<Integer>>();
        nGraph.rTable = new HashMap<Integer, Integer>();

        for (Map.Entry<Integer, HashSet<Integer>> entry : this.parents.entrySet()) {
            nGraph.parents.put(entry.getKey(), new HashSet<Integer>());
            nGraph.parents.get(entry.getKey()).addAll(entry.getValue());
        }

        for (Map.Entry<Integer, HashSet<Integer>> entry : this.children.entrySet()) {
            nGraph.children.put(entry.getKey(), new HashSet<Integer>());
            nGraph.children.get(entry.getKey()).addAll(entry.getValue());
        }
        nGraph.rTable.putAll(this.rTable);

        return nGraph;
    }

    public SGraph bsim() {
        return this;
    }

    public void finalizeGraph() {

    }

    public String toString() {
        String res = "";
        res = res + "================Graph information=============\n";
        res = res + "layer\t" + layer + "\n";
        res = res + "Vertices size:\t" + vertexSize() + "\n";
//        res = res + "dmap size:\t" + dmaps.size() + "\n";
        for (SVertex v : allvertices) {
            res += ("vid\t" + v.id + "\tattr\t" + v.attr + "\tdmaps\t" + v.dmap + "\n");
        }
        res = res + "Edge size:\t" + edgeSize + "\n";
        for (int src : vertices.keySet()) {
            for (int dst : getChildren(src)) {
                res += ("src\t" + src + "\tdst\t" + dst + "\n");
            }
        }
        res += ("[vindex]" + this.vindex);
        return res;
    }

    public void WriteToDisk(String path) throws IOException {
        IOPack ioPack = new IOPack();
        String vPath = path + ".v";
        String rPath = path + ".r";
        BufferedWriter vbf = ioPack.getWritter(vPath);
        BufferedWriter rbf = ioPack.getWritter(rPath);
        for (SVertex v : allvertices) {
            vbf.write(v.id + "\t" + v.attr + "\n");
            rbf.write(v.id + "\t" + 1 + "\n");
        }
        vbf.flush();
        vbf.close();
        rbf.flush();
        rbf.close();

        String ePath = path + ".e";
        BufferedWriter ebf = ioPack.getWritter(ePath);
        for (int dst : parents.keySet()) {
            for (int src : parents.get(dst)) {
                ebf.write(src + "\t" + dst + "\t" + 1 + "\n");
            }
        }
        ebf.flush();
        ebf.close();

        String dPath = path + ".dmap";
        BufferedWriter dbf = ioPack.getWritter(dPath);
        for (int comV : dmaps.keySet()) {
            dbf.write("" + comV);
            for (int ori : dmaps.get(comV)) {
                dbf.write("\t" + ori);
            }
            dbf.write("\n");
        }
        dbf.flush();
        dbf.close();

        String bPath = path + ".bmap";
        BufferedWriter bbf = ioPack.getWritter(bPath);
        for (int current : bmaps.keySet()) {
            bbf.write(current + "\t" + bmaps.get(current) + "\n");
        }
        bbf.flush();
        bbf.close();
    }

    public void WriteToDisk(String path, boolean graph_only) throws IOException {
        IOPack ioPack = new IOPack();
        String vPath = path + ".v";
        String rPath = path + ".r";
        BufferedWriter vbf = ioPack.getWritter(vPath);
        BufferedWriter rbf = ioPack.getWritter(rPath);
        for (SVertex v : allvertices) {
            vbf.write(v.id + "\t" + v.attr + "\n");
            rbf.write(v.id + "\t" + 1 + "\n");
        }
        vbf.flush();
        vbf.close();
        rbf.flush();
        rbf.close();

        String ePath = path + ".e";
        BufferedWriter ebf = ioPack.getWritter(ePath);
        for (int dst : parents.keySet()) {
            for (int src : parents.get(dst)) {
                ebf.write(src + "\t" + dst + "\t" + 1 + "\n");
            }
        }
        ebf.flush();
        ebf.close();

        if(graph_only)
            return;
        String dPath = path + ".dmap";
        BufferedWriter dbf = ioPack.getWritter(dPath);
        for (int comV : dmaps.keySet()) {
            dbf.write("" + comV);
            for (int ori : dmaps.get(comV)) {
                dbf.write("\t" + ori);
            }
            dbf.write("\n");
        }
        dbf.flush();
        dbf.close();

        String bPath = path + ".bmap";
        BufferedWriter bbf = ioPack.getWritter(bPath);
        for (int current : bmaps.keySet()) {
            bbf.write(current + "\t" + bmaps.get(current) + "\n");
        }
        bbf.flush();
        bbf.close();
    }


    public void ReadFromDisk(String path) throws IOException {
        IOPack ioPack = new IOPack();
        String vPath = path + ".v";
        String line;
        this.dmaps = new HashMap<Integer, ArrayList<Integer>>();
        this.bmaps = new HashMap<Integer, Integer>();
        BufferedReader vbr = ioPack.getBufferInstance(vPath);
        while ((line = vbr.readLine()) != null) {
            String[] data = line.split("\t");
            this.addVertex(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
        }
        vbr.close();

        String ePath = path + ".e";
        BufferedReader ebr = ioPack.getBufferInstance(ePath);
        while ((line = ebr.readLine()) != null) {
            String[] data = line.split("\t");
            this.addEdge(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
        }
        ebr.close();

        String rPath = path + ".r";
        BufferedReader rbr = ioPack.getBufferInstance(rPath);
        while ((line = rbr.readLine()) != null) {
            String[] data = line.split("\t");
            rTable.put(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
        }
        rbr.close();

        String dPath = path + ".dmap";
        BufferedReader dbr = ioPack.getBufferInstance(dPath);
        while ((line = dbr.readLine()) != null) {
            String[] data = line.split("\t");

            if(data.length == 0){
                continue;
            }
            ArrayList<Integer> dmap = new ArrayList<Integer>();
            for(int i=1; i < data.length;i++){
                dmap.add(Integer.parseInt(data[i]));
            }
            dmaps.put(Integer.parseInt(data[0]), dmap);
        }
        dbr.close();

        String bPath = path + ".bmap";
        BufferedReader bbr = ioPack.getBufferInstance(bPath);
        while ((line = bbr.readLine()) != null) {
            String[] data = line.split("\t");
            if(data.length == 0)
                continue;
            bmaps.put(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
        }
        bbr.close();
    }

    public void printGraph() {
        System.out.println("===============SGraph information==============");
        System.out.println("===============Layer" + layer + "==============");
        System.out.println("Vertices size:\t" + vertexSize() + "\n");
        System.out.println("Edge size:\t" + edgeSize + "\n");
        System.out.println("VIndex\t" + this.vindex);
    }

    public void generateRTable() {
        rTable = new HashMap<Integer, Integer>();
        for (int v : allDigVertices) {
            rTable.put(v, 1);
        }
    }
}
