package config;

/**
 * Created by samjjx on 2017/9/21.
 */
public enum KWFactor {
    SIMPLE, ONE_BRANCH, DISTANCE_LIMIT, TEST_CASE
}
