package config;

/**
 * Created by samjjx on 2017/7/31.
 */
public enum CompressFactor {
    CR, DT, ENTROPY, NOTCR, NOTDT, NOTENTROPY, All;
}
