package config;

/**
 * Created by samjjx on 2017/10/23.
 */

public enum GenFactor {
    SIMPLE, SubTree, HighDegree, Entropy, Distortion, CompressionRatio, AllRoot
}
