package config;

import util.IOPack;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by samjjx on 2018/1/7.
 */
public class Query {
    public ArrayList<Pattern> batchQuery;

    public Query(int nKeyWords, KWFactor kf) {
        //TODO To support the selectors
    }

    /**
     * To support a set of pre-defined queries with a specific location
     * @param path: the query location
     * @throws IOException
     */
    public Query(String path) throws IOException {
        batchQuery = new ArrayList<Pattern>();
        File folder = new File(path);
        File[] allfile = folder.listFiles();
        for (File file : allfile) {
            if (file.isFile()) {
                batchQuery.add(getAQuery(path + file.getName()));
            }
        }
    }

    public Pattern getAQuery(String path) throws IOException {
        HashSet<Integer> query = new HashSet<Integer>();
        IOPack io = new IOPack();
        BufferedReader br = io.getBufferInstance(path);
        String line;
        while ((line = br.readLine()) != null) {
            String[] data = line.split("\t");
            for (String k : data) {
                query.add(Integer.parseInt(k));
            }
        }
        Pattern ptn = new Pattern(path, query);
        System.out.println(ptn.keywords);
        return ptn;
    }

    public Query() {
        //TODO To support random query keywords
    }

    public static void main(String[] args) throws IOException {
        Query test = new Query("/Users/samjjx/dev/dataset/synt/");
    }
}

