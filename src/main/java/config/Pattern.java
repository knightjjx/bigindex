package config;

import bigindex_config.Config;

import java.util.HashSet;

public class Pattern {
    public String path;
    public HashSet<Integer> keywords;

    public HashSet<Integer> gen_keywords;

    public Pattern(String path, HashSet<Integer> keywords) {
        this.path = path;
        this.keywords = keywords;
    }

    public void genKeywords(Config configs){
        gen_keywords = configs.generalize(keywords);
    }
}