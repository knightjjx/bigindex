package gen;

import graph.SGraph;
import graph.SVertex;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by samjjx on 2017/10/30.
 */
public class GraphDistrotion {
    private SGraph graph;
    private HashMap<Integer, Integer> freq;

    public GraphDistrotion(SGraph graph) {
        this.graph = graph;
        freq = new HashMap<Integer, Integer>();
        DTComputationWithFreq();
    }

    public double DTComputation(HashMap<Integer, Integer> config) {
        HashMap<Integer, HashSet<Integer>> invert = new HashMap<Integer, HashSet<Integer>>();
        Set<Integer> keyset = config.keySet();
        for (int orikeyword : keyset) {
            int genkeyword = config.get(orikeyword);
            if (!invert.containsKey(genkeyword)) {
                invert.put(genkeyword, new HashSet<Integer>());
            }
            invert.get(genkeyword).add(orikeyword);
        }
        double sumDT = 0;
        keyset = invert.keySet();
        for (int key : keyset) {
            HashSet<Integer> tmp = invert.get(key);
            for (int oriKeyword : tmp) {
                sumDT += ((double) 1 / tmp.size()) * this.freq.get(oriKeyword);
            }
        }
        return sumDT;
    }
    public int countFreq(){
        int sum = 0;
        for(int key: freq.keySet())
            sum += freq.get(key);
        return sum;
    }
    private void DTComputationWithFreq() {
        for (SVertex v : graph.allvertices) {
            if (!freq.containsKey(v.attr)) {
                freq.put(v.attr, 0);
            }
            freq.put(v.attr, freq.get(v.attr) + 1);
        }
    }
}
