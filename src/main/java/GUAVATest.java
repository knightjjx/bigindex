import com.google.common.graph.EndpointPair;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import java.util.Set;

/**
 * Created by samjjx on 2017/10/16.
 */
public class GUAVATest {
    public static void main(String[] args){
        MutableGraph<Integer> graph = GraphBuilder.directed().allowsSelfLoops(false).build();

        graph.putEdge(1,2);
        graph.putEdge(3,5);
        graph.putEdge(3,6);
        Set<Integer> nodes= graph.nodes();

        for(int v:nodes){
            System.out.println(v);
        }
        Set<EndpointPair<Integer>> edges= graph.edges();
//        graph.removeNode(3);
        for(EndpointPair e:edges){
            System.out.println(e.source());
            System.out.println(e.target());
        }
//        graph.removeEdge(1,2);
    }
}
