package test;

import graph.SVertex;

import java.util.HashSet;

/**
 * Created by samjjx on 2017/8/24.
 */
public class HashSetTest {
    public static void main(String[] args){
        HashSet<HashSet<SVertex>> test1 = new HashSet<HashSet<SVertex>>();
        HashSet<HashSet<SVertex>> test2 = new HashSet<HashSet<SVertex>>();

        HashSet<SVertex> t1 = new HashSet<SVertex>();
        HashSet<SVertex> t2 = new HashSet<SVertex>();
        SVertex v1 = new SVertex(1, 3);
        t1.add(v1);
        t2.add(v1);
        test1.add(t1);
        test2.add(t1);
        test2.remove(t2);

//        HashSet<SVertex> s1 = new HashSet<SVertex>();
//        HashSet<SVertex> s2 = new HashSet<SVertex>();
//        test1.add(s1);
//        test2.add(s2);
        SVertex v2 = new SVertex(1, 3);

        System.out.println(test2.size());

    }
}
