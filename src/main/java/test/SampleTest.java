package test;

import bigindex_config.GraphSample;
import graph.SGraph;

public class SampleTest {
    public static void main(String[] args){
        SGraph graph = new SGraph();
        graph.addVertex(1,1);
        graph.addVertex(2,1);
        graph.addVertex(3,1);
        graph.addVertex(4,2);
        graph.addVertex(5,2);
        graph.addVertex(6,2);
        graph.addVertex(7,2);
        graph.addEdge(1,2);
        graph.addEdge(1,3);
        graph.addEdge(1,4);
        graph.addEdge(2,4);
        graph.addEdge(2,7);
        graph.addEdge(3,4);
        graph.addEdge(3,5);
        graph.addEdge(3,6);
        graph.addEdge(4,7);
        GraphSample samples = new GraphSample(graph, 3,2);

        for(int key: samples.samples.keySet()){
            System.out.println("root\t" + key);
            System.out.println(samples.samples.get(key));
        }
    }
}
