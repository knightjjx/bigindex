package test;

import graph.SGraph;

/**
 * Created by samjjx on 2017/9/3.
 */
public class CloneTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        SGraph graph = new SGraph();
        graph.addVertex(1,0);
        graph.addVertex(2,0);
        SGraph nGraph = graph.clone();
        nGraph.addVertex(3, 0);
        System.out.println(graph.getVertex(3).attr);
    }
}
