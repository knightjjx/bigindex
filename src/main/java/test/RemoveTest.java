package test;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by samjjx on 2018/1/22.
 */
public class RemoveTest {
    public static void main(String[] args){
        HashSet<Integer> tmp = new HashSet<Integer>();
        for(int i =0;i<10;i++){
            tmp.add(i);
        }
        System.out.println(tmp);
        Iterator<Integer> ite = tmp.iterator();

        while(ite.hasNext()){
            int tt = ite.next();
            System.out.println(tt);
            tmp.remove(tt);
            tmp.remove(15);
            ite = tmp.iterator();
        }
    }
}
