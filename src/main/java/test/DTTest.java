package test;

import gen.GraphDistrotion;
import graph.SGraph;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by samjjx on 2017/11/12.
 */
public class DTTest {
    public static void main(String[] args) throws IOException {
        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/fund/synt1", true);
        GraphDistrotion dt = new GraphDistrotion(graph);

        HashMap<Integer, Integer> config1 = new HashMap<Integer, Integer>();

        for(int keyword: graph.allKeywords){
            config1.put(keyword, 1);
        }

        double dt_value= dt.DTComputation(config1);
        System.out.println(dt_value);
        System.out.println(dt.countFreq());
        System.out.println(graph.allKeywords.size());
    }
}
