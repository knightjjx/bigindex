package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Created by samjjx on 2017/7/24.
 */
public class PQueuetest {
    public static void main(String[] args){
        PriorityQueue<Counter> queue = new PriorityQueue<Counter>();
        Counter c1 = new Counter(5);
        c1.counter=5;
        Counter c2 = new Counter(2);
        c2.counter=2;
        Counter c3 = new Counter(3);
        c3.counter=3;
        Counter c4 = new Counter(4);
        c4.counter=4;
        queue.add(c1);
        queue.add(c2);
        queue.add(c3);
        queue.add(c4);
        System.out.println(queue.peek().keyword);
        c4.counter=1;
        Counter tmp = queue.remove();
        tmp.counter = -1;
        queue.add(tmp);
        System.out.println(queue.peek().keyword);

        ArrayList<Integer> test = new ArrayList<Integer>();
        test.add(5);
        test.add(8);
        test.add(12);
        Iterator<Integer> ttt = test.iterator();
        ttt.next();
        ttt.next();
        ttt.next();

        System.out.println(test.size());

        HashSet<Integer> hs = new HashSet<Integer>();
        hs.add(1);
        hs.add(5);
        hs.add(9);
        HashSet<Integer> hs2 = new HashSet<Integer>();
        hs2.add(1);

        hs.retainAll(hs2);
        System.out.println(hs);
    }
}
