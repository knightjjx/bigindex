package test;

import graph.SCC;
import graph.SGraph;

import java.io.IOException;

/**
 * Created by samjjx on 2017/10/26.
 */
public class SCCTest {
    public static void main(String[] args) throws IOException {
        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/test/testscc", true);
        SCC scc = new SCC(graph);
        System.out.println(graph);
        System.out.println(scc);

    }
}
