package test;

class Counter implements Comparable<Counter>{
    int keyword;
    int counter;
    public Counter(int keyword){
        this.keyword = keyword;
        this.counter = 0;
    }

    public int compareTo(Counter other) {
        return this.counter-other.counter;
    }
}