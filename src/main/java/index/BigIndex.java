package index;

import bigindex_config.GraphSample;
import bsim.PaigeTarjan;
import config.CompressFactor;
import config.GenFactor;
import gen.GraphDistrotion;
import graph.OntGraph;
import graph.SCC;
import graph.SGraph;
import graph.SVertex;
import util.IOPack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by samjjx on 2017/7/30.
 */
public class BigIndex {
    public SGraph graph;
    public OntGraph ontGraph;
    public SGraph top;

    public int nLayers = 0;
    public CompressFactor cf;
    public HashMap<Integer, SGraph> MulGraph = new HashMap<Integer, SGraph>();

    public HashMap<Integer, HashMap<Integer, Integer>> configs;  // layer -> config(keyword -> supertype)

    public int searchLayer = 0;
    private GenFactor gf = GenFactor.SIMPLE;
    //FIXME: This is quick for implementation. Come back here layer to wrap up the keywords to class (link list.....)
    public HashMap<Integer, HashSet<Integer>> hierarchy_keyword; // layer -> keywords
    public HashMap<Integer, HashMap<Integer, Integer>> hierarchy_config; // layer -> layer -> mapping

    //TODO: Supporting the multiple keywords search. The structure should be cleaned before the next time query.
    public BigIndex(SGraph graph, OntGraph ontGraph, CompressFactor cf) throws CloneNotSupportedException, IOException {
        this.cf = cf;
        this.graph = graph;
        this.top = graph;
        this.ontGraph = ontGraph;
        graph.layer = 1;
        graph.next = null;
        nLayers = 1;
        configs = new HashMap<Integer, HashMap<Integer, Integer>>();
        this.hierarchy_keyword = new HashMap<Integer, HashSet<Integer>>();
        this.hierarchy_config = new HashMap<Integer, HashMap<Integer, Integer>>();
        construct();
    }

    /**
     * Load index from disk
     *
     * @param path: index path
     */
    public BigIndex(String path) throws IOException {
        IOPack ioPack = new IOPack();
        int layer = 0;
        /**
         * Read generalization configuration
         */
        BufferedReader confbr = ioPack.getBufferInstance(path + ".config");
        String line;
        this.configs = new HashMap<Integer, HashMap<Integer, Integer>>();
        while ((line = confbr.readLine()) != null) {
            String[] data = line.split("\t");
            layer = Integer.parseInt(data[0]);
            int config_size = Integer.parseInt(data[1]);
            HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
            for (int i = 0; i < config_size; i++) {
                line = confbr.readLine();
                data = line.split("\t");
                config.put(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
            }
            configs.put(layer, config);
        }

        SGraph top = null;
        for (int i = 1; i <= layer + 1; i++) {
            String basePath = path + i;
            SGraph diskGraph = new SGraph();
            diskGraph.ReadFromDisk(basePath);
            diskGraph.setLayer(i);
            diskGraph.next = top;
            if (top != null) {
                top.prev = diskGraph;
            }
            top = diskGraph;
            MulGraph.put(i, diskGraph);
            System.out.println("Layer\t" + i + "\t Finish loading");
        }
        nLayers = MulGraph.size();
        this.hierarchy_keyword = new HashMap<Integer, HashSet<Integer>>();
        this.hierarchy_config = new HashMap<Integer, HashMap<Integer, Integer>>();
    }

    /**
     * Write index to the disk
     *
     * @param Path: The storage path
     */
    public void WriteToDisk(String Path) throws IOException {
        Set<Integer> layers = MulGraph.keySet();
        System.out.println("[index]" + layers);
        for (int layer : layers) {
            SGraph graph = MulGraph.get(layer);
            graph.WriteToDisk(Path + "" + layer + "");
        }
        IOPack ioPack = new IOPack();
        BufferedWriter confbw = ioPack.getWritter(Path + ".config");
        layers = configs.keySet();
        System.out.println("[index]" + layers);
        for (int layer : layers) {
            HashMap<Integer, Integer> config = configs.get(layer);
            confbw.write("" + layer + "\t" + config.size() + "\n");
            for (int k1 : config.keySet()) {
                confbw.write(k1 + "\t" + config.get(k1) + "\n");
            }
        }
        confbw.flush();
        confbw.close();
    }

    public void construct() throws CloneNotSupportedException, IOException {
        int count = 1;
        HashMap<Integer, Integer> config = generateConfig();
        top.generalization(config);
        MulGraph.put(nLayers, top);
        while (!isHalt()) {
            System.out.println("[layer]" + (count++));
            System.out.println("Begin compression");
            config = generateConfig();
            configs.put(nLayers, config);
            SGraph tmp = top.clone();
            System.out.println("[Number keywords]" + tmp.allKeywords.size());
            tmp.generalization(config);
            System.out.println("[Number keywords]" + tmp.allKeywords.size());
            SGraph compressed = compress(tmp);
            compressed.printGraph();
            compressed.next = top;
            top.prev = compressed;
            top = compressed;
            nLayers++;
            MulGraph.put(nLayers, top);
            System.out.println("======Finish a layer======");
        }
//        printConfigs();
    }

    public void printConfigs() {
        System.out.println("==========================");
        Set<Integer> keys = configs.keySet();
        for (int key : keys) {
            System.out.println("[Layer]" + key);
            HashMap<Integer, Integer> config = configs.get(key);
            for (Map.Entry e : config.entrySet()) {
                System.out.println("[Ori]" + e.getKey() + "\t[Gen]" + e.getValue() + "\n");
            }
        }
        System.out.println("==========================");
    }

    public HashMap<Integer, Integer> generateConfig() {
        switch (this.gf) {
            case SIMPLE:
                return generateConfigSimple(1);
            case SubTree:
                return getSubTree(1);
            case HighDegree:
                return generateConfigWithHighDegree(1);
            case Distortion:
                return generateConfigByDT(10);
            case Entropy:
                return generateConfigByEntropy();
            case CompressionRatio:
                return generateConfigByCR();
            case AllRoot:
                return allRoot();
        }
        return new HashMap<Integer, Integer>();
    }

    public HashMap<Integer, Integer> getSubTree(int root) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        for (int keyword : allK) {
            if (!ontGraph.attr2ID.containsKey(keyword)) {
                config.put(keyword, 0);
            } else {
                ontGraph.attr2ID.get(keyword);
            }
        }
        Queue<Integer> bfsQ = new LinkedList<Integer>();
        bfsQ.add(root);
        HashSet<Integer> visited = new HashSet<Integer>();
        HashSet<Integer> foundedKeywords = new HashSet<Integer>();
        while (!bfsQ.isEmpty()) {
            int head = bfsQ.remove();
            if (visited.contains(head))
                continue;
            for (int c : ontGraph.getChildren(head)) {
                if (visited.contains(c))
                    continue;
            }
            bfsQ.addAll(ontGraph.getChildren(head));
            if (allK.contains(ontGraph.getAttribute(head))) {
                config.put(ontGraph.getAttribute(head), ontGraph.getAttribute(root));
                foundedKeywords.add(ontGraph.getAttribute(head));
            }
            visited.add(head);
        }
        for (int keyword : allK) {
            if (foundedKeywords.contains(keyword))
                continue;
            if (ontGraph.getParents(ontGraph.attr2ID.get(keyword)).isEmpty())
                config.put(keyword, 1);
            config.put(keyword, keyword);
        }
        return config;
    }

    public HashMap<Integer, Integer> allRoot() {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;
        for (int keyword : allK)
            config.put(keyword, 0);
        return config;
    }

    public HashMap<Integer, Integer> generateConfigWithHighDegree(int MulLayer) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;

        for (int i = 0; i < MulLayer; i++) {
            //System.out.println("[Warn Size]" + allK.size());
            HashSet<Integer> allK_tmp = new HashSet<Integer>();
            for (int keyword : allK) {
                int vid = ontGraph.attr2ID.get(keyword);
                Set<SVertex> parents = ontGraph.getParents(ontGraph.getVertex(vid));
                if (parents.isEmpty()) {
                    config.put(keyword, keyword);
                    config.put(keyword, 1);
//                    allK_tmp.add(keyword);
                } else {
                    int highVid = 0;
                    int max = Integer.MIN_VALUE;
                    for (SVertex p : parents) {
                        int pid = p.id;
                        if (max < ontGraph.outDegree(pid)) {
                            max = ontGraph.outDegree(pid);
                            highVid = pid;
                        }
                    }
                    int attr = ontGraph.getVertex(highVid).attr;
                    config.put(keyword, attr);
//                    config.put(keyword, 1);
                    allK_tmp.add(attr);
                }
            }
            allK = allK_tmp;
        }
        return config;
    }

    public HashMap<Integer, Integer> generateConfigSimple(int MulLayer) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        HashSet<Integer> allK = top.allKeywords;

        for (int i = 0; i < MulLayer; i++) {
            HashSet<Integer> allK_tmp = new HashSet<Integer>();
            for (int keyword : allK) {
                int vid = ontGraph.attr2ID.get(keyword);

                Set<SVertex> parents = ontGraph.getParents(ontGraph.getVertex(vid));
                if (parents.isEmpty()) {
                    config.put(keyword, keyword);
                    allK_tmp.add(keyword);
                } else {
                    int p = parents.iterator().next().attr;
                    config.put(keyword, p);
                    allK_tmp.add(p);
                }
            }
            allK = allK_tmp;
        }
        return config;
    }

    public HashMap<Integer, Integer> generateConfigRandomization() {

        return null;
    }

    //TODO: here should attach a config function built on the DT, CR, Entropy
    public HashMap<Integer, Integer> generateConfigByDT(int bounded_keywords) {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        GraphDistrotion dt = new GraphDistrotion(top);

        for (int i = 0; i < bounded_keywords; i++) {

        }
        return config;
    }

    public HashMap<Integer, Integer> generateConfigByCR() {
        GraphSample samples = new GraphSample(top, 1000, 4);

        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        samples.computeBsim(generateConfigSimple(1));
        return config;
    }

    public HashMap<Integer, Integer> generateConfigByEntropy() {
        HashMap<Integer, Integer> config = new HashMap<Integer, Integer>();
        return config;
    }

    public void keywordGen(HashSet<Integer> ori_keywords) {
        searchLayer = peekTopLayer(ori_keywords);
    }

    /**
     * This function determine which layer the query keywords should go to in BigIndex
     *
     * @param ori_keywords: Query keywords set
     * @return: The layer number
     */
    public int peekTopLayer(HashSet<Integer> ori_keywords) {
        int layer = 1;
        hierarchy_keyword.put(layer, ori_keywords);
        l:
        while (layer < nLayers && layer < 4) {
            HashSet<Integer> generalize = new HashSet<Integer>();
            HashMap<Integer, Integer> tmpK = configs.get(layer);
            System.out.println("[ori]" + ori_keywords);
            for (int km : ori_keywords) {
                for (int kn : ori_keywords) {
                    if (kn == km)
                        continue;
                    int tkm = tmpK.get(km);
                    int tkn = tmpK.get(kn);

                    if (tkm == tkn) {
                        System.out.println("[search]" + layer);
                        return layer;
                    }
                }
            }
            HashMap<Integer, Integer> query_config = new HashMap<Integer, Integer>();
            for (int k : ori_keywords) {
                generalize.add(tmpK.get(k));
                query_config.put(tmpK.get(k), k);
            }
            layer++;
            hierarchy_keyword.put(layer, generalize);
            hierarchy_config.put(layer, query_config);
            //TODO: check following code
            ori_keywords = generalize;
        }
        return layer;
    }

    public HashSet<Integer> getGenKeywords() {
        System.out.println("print generalized query keywords");
        for (int key : hierarchy_keyword.keySet()) {
            System.out.println(key + "\t" + hierarchy_keyword.get(key));
        }
        return hierarchy_keyword.get(searchLayer);
    }

    public SGraph compress(SGraph graph){
        long t0 = System.currentTimeMillis();
        graph.reset();
        SCC scc = new SCC(graph);
        System.out.println("SCC computation time:\t" + (System.currentTimeMillis() - t0));
        graph.initRank(scc);
        System.out.println("[Max]" + graph.maxRank);
        PaigeTarjan pt = new PaigeTarjan();
        SGraph compressed = pt.bisimulationGeneral(graph);
        return compressed;
    }

    //TODO: This function is judge the halting condition
    public boolean isHalt() {
        if (nLayers == 1) {
            return false;
        }
        if (cf == CompressFactor.CR) {
            if (computeCR() < 0.95)
                return false;
        }
        if (cf == CompressFactor.DT) {
            if (computeDT() > 0.8)
                return true;
        }
        if (cf == CompressFactor.ENTROPY) {

        }
        if (cf == CompressFactor.NOTCR) {

        }
        if (cf == CompressFactor.NOTDT) {

        }
        if (cf == CompressFactor.NOTENTROPY) {

        }
        if (cf == CompressFactor.All) {

        }
        return true;
    }

    //TODO: Following are three interfaces for computing an efficient Gen function
    public double computeDT() {

        return 0;
    }

    public double computeCR() {
        return (double) (top.allvertices.size() + top.edgeSize()) / (double) (top.next.allvertices.size() + top.next.edgeSize());
    }

    public double computeEntropy() {
        return 0;
    }

    public String toString() {
        String res = "[************BigIndex**************]\n";
        Set<Integer> graphkeys = MulGraph.keySet();
        for (int key : graphkeys) {
            SGraph tmp = MulGraph.get(key);
            res += "[Layer]\t" + key + "\n";
            res += tmp + "\n";
        }
        return res;
    }

    public HashSet<SVertex> getGroundVertices(int layer, int vid) {
        HashSet<SVertex> res = new HashSet<SVertex>();

        HashSet<Integer> all_res = new HashSet<Integer>();
        all_res.add(vid);

        while (layer > 1) {
            HashSet<Integer> tmp = new HashSet<Integer>();
            SGraph stmp = MulGraph.get(layer);
            for (int v : all_res) {
                tmp.addAll(stmp.dmaps.get(v));
            }
            all_res = tmp;
            layer--;
        }
        SGraph ground = MulGraph.get(1);
        for (int v : all_res) {
            res.add(ground.getVertex(v));
        }
        return res;
    }

    public HashSet<SVertex> MulDMap(int top, int bottom, int vid) {
        HashSet<SVertex> res = new HashSet<SVertex>();

        HashSet<Integer> all_res = new HashSet<Integer>();
        all_res.add(vid);

        while (top > bottom) {
            HashSet<Integer> tmp = new HashSet<Integer>();
            SGraph stmp = MulGraph.get(top);
            for (int v : all_res) {
                tmp.addAll(stmp.getVertex(v).dmap);
            }
            all_res = tmp;
            top--;
        }
        SGraph ground = MulGraph.get(bottom);
        for (int v : all_res) {
            res.add(ground.getVertex(v));
        }
        return res;
    }
}
