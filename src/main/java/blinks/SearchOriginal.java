package blinks;

import algorithms.Blinks;
import config.Pattern;
import config.Query;
import graph.AGraph;
import graph.OntGraph;
import graph.SGraph;
import result.BlinksResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by samjjx on 2017/11/24.
 */
public class SearchOriginal {
    public static void main(String[] args) throws IOException, CloneNotSupportedException {

        String graph_path = args[0];
        String ont_path = args[1];
        String query_path = args[2];
        String log_path = args[3];
        SGraph graph = new SGraph(graph_path, true);
        OntGraph ont = new OntGraph(ont_path, true, 0, true);
        Query query = new Query(query_path);

        Logger logger = Logger.getLogger("original");

        FileHandler fh = new FileHandler(log_path);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        for (Pattern ptn : query.batchQuery) {
            try {
                logger.info("===============================================");
                logger.info("[Pattern]" + ptn.path);
                logger.info(""+ptn.keywords);
                long t0 = System.currentTimeMillis();
                Blinks blinks = new Blinks(1, graph, 5);
                logger.info("index construction time on the original graph"+(System.currentTimeMillis()-t0));
                t0=System.currentTimeMillis();
                ArrayList<BlinksResult> cres = blinks.semantic(ptn.keywords, graph);
                logger.info("Total Visited vertex\t" + blinks.getVisitedNode());
                logger.info("Searching time on original graph:"+ (System.currentTimeMillis() - t0));

                Queue<AGraph> Lcres = new LinkedList<AGraph>();
                System.out.println("[Answer Info]");
                for (BlinksResult c : cres) {
                    Lcres.add(c.ans);
                    System.out.println(c);
                }
                System.out.println("Lcres size is:\t" + Lcres.size());
            }catch (java.lang.NullPointerException e){
                logger.info("[Pattern]" + ptn.path+"NullpointerException");
                continue;
            }
        }

//        //        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/fund/synt0", true);
////        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/fund/synt0.ont", true, 0, true);
//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/synt/synt2", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/synt/synt5.ont", true, 0, true);
//
//        long t0 = System.currentTimeMillis();
//        Blinks blinks = new Blinks(1, graph, 5);
//
//        System.out.println("Blinks building time on Original Graph: \t" + (System.currentTimeMillis() - t0));
//        KWSelector kwSelector = new KWSelector(ont, KWFactor.TEST_CASE, 2);
//        HashSet<Integer> keywords = kwSelector.SelectKW();
//        System.out.println(keywords);
//        ArrayList<BlinksResult> results = blinks.semantic(keywords, graph);
//        System.out.println("Searching Time on Original Graph: \t" + (System.currentTimeMillis() - t0));
//        System.out.println("Answer graph on original graph");
//        System.out.println("Answer graph size:"+ results.size());
//        for (BlinksResult res : results) {
//            System.out.println(res);
//        }
//        System.out.println("===============");
//        ArrayList<BlinksResult> results1 = blinks.semantic(keywords, graph);
//        for (BlinksResult res : results1) {
//            System.out.println(res);
//        }
    }
}
