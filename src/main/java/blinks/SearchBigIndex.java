package blinks;

import algorithms.Blinks;
import config.Pattern;
import config.Query;
import decompress.OPTDecompressor;
import graph.AGraph;
import graph.OntGraph;
import graph.SGraph;
import index.BigIndex;
import result.BlinksResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by samjjx on 2017/12/12.
 */
public class SearchBigIndex {
    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        //arg1: /Users/samjjx/dev/dataset/synt/synt2 /Users/samjjx/dev/dataset/synt/synt5.ont /Users/samjjx/dev/git/qp-compression-kws/query/scale_query/ /Users/samjjx/dev/git/qp-compression-kws/log/exp.log
        //arg2: /Users/samjjx/dev/dataset/testpt/test /Users/samjjx/dev/dataset/testpt/test.ont /Users/samjjx/dev/git/qp-compression-kws/query/scale_query/ /Users/samjjx/dev/git/qp-compression-kws/log/exp.log
        //arg3: /Users/samjjx/dev/dataset/synt/synt1 /Users/samjjx/dev/dataset/synt/synt5.ont /Users/samjjx/dev/git/qp-compression-kws/query/scale_query/ /Users/samjjx/dev/git/qp-compression-kws/log/exp.log

        String graph_path = args[0];
        String ont_path = args[1];
        String query_path = args[2];
        String log_path = args[3];
        String index_path = args[4];
        SGraph graph = new SGraph(graph_path, true);
        OntGraph ont = new OntGraph(ont_path, true, 0, true);
        Query query = new Query(query_path);

//        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/testpt/test", true);
//        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/testpt/test.ont", true, 0, true);
//        Query query = new Query("/Users/samjjx/dev/git/qp-compression-kws/query/");

        Logger logger = Logger.getLogger("BigIndex");

        FileHandler fh = new FileHandler(log_path);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

//        BigIndex bi = new BigIndex(graph, ont, CompressFactor.CR);
//        bi.WriteToDisk(index_path);
//        BigIndex bi = new BigIndex("/tmp/jxjian/index/dbpedia");
        BigIndex bi = new BigIndex(index_path);
        for (Pattern ptn : query.batchQuery) {
            logger.info("===============================================");
            logger.info("[Pattern]" + ptn.path);
            bi.keywordGen(ptn.keywords);
            if (bi.searchLayer < 3) {
                logger.info("[Pattern]" + ptn.path + " at lower search layer");
                logger.info("Searching layer\t" + bi.searchLayer + "\n");
                continue;
            }
            long t0 = System.currentTimeMillis();
            logger.info("Searching layer\t" + bi.searchLayer + "\n");
            logger.info("# of keywords\t" + bi.MulGraph.get(bi.searchLayer).allKeywords.size() + "\n");
            Blinks bl = new Blinks(1, bi.MulGraph.get(bi.searchLayer), 5);
            logger.info("Index construction time on the top layer\t" + (System.currentTimeMillis() - t0) + " ms");
            t0 = System.currentTimeMillis();
            logger.info("[SearchLayer]" + bi.searchLayer);
            ArrayList<BlinksResult> cres = bl.semantic(bi.getGenKeywords(), bi.MulGraph.get(bi.searchLayer));
            logger.info("Total Visited vertex\t" + bl.getVisitedNode());
            logger.info("Searching time on compressed graph:\t" + (System.currentTimeMillis() - t0) + " ms");
            System.out.println(bi.configs.keySet().size());
            Queue<AGraph> Lcres = new LinkedList<AGraph>();
            System.out.println("[Answer Info]");
            for (BlinksResult c : cres) {
                Lcres.add(c.ans);
//                System.out.println(c);
            }
            System.out.println("Lcres size is:\t" + Lcres.size());
            logger.info("Lcres size is:\t" + Lcres.size() + "\n");
            t0 = System.currentTimeMillis();
            OPTDecompressor opt = new OPTDecompressor(Lcres, bi, logger);
            opt.decompressor();
//            Decompressor dc = new Decompressor(Lcres, bi);

//            dc.DecompressByLayerOri();
            logger.info("Results decompression time: " + (System.currentTimeMillis() - t0) + " ms");

//            Queue<AGraph> dr = dc.getResults();
            logger.info("Result size\t" + opt.allAnswers.size());
            System.out.println("Time cost\t" + (System.currentTimeMillis() - t0));
//            while (!dr.isEmpty()) {
//                System.out.println(dr.remove());
//            }
        }

        /*
        KWSelector kwSelector = new KWSelector(ont, KWFactor.TEST_CASE, 2);
        HashSet<Integer> keywords = kwSelector.SelectKW();
        bi.keywordGen(keywords);
        Blinks bl = new Blinks(1, bi.MulGraph.get(bi.searchLayer), 5);

        System.out.println("[SearchLayer]" + bi.searchLayer);
        ArrayList<BlinksResult> cres = bl.semantic(bi.getGenKeywords(), bi.MulGraph.get(bi.searchLayer));
        System.out.println(bi.configs.keySet().size());
        Queue<AGraph> Lcres = new LinkedList<AGraph>();
        System.out.println("[Answer Info]");
        for (BlinksResult c : cres) {
            Lcres.add(c.ans);
        }
        System.out.println("Lcres size is:\t" + Lcres.size());
        long t0;
        Decompressor dc = new Decompressor(Lcres, bi);
        t0 = System.currentTimeMillis();
        dc.DecompressByLayerOri();
        System.out.println("Results decompression time: " + (System.currentTimeMillis() - t0));

        Queue<AGraph> dr = dc.getResults();
        System.out.println("[size]\t" + dr.size());
        System.out.println("Time cost\t" + (System.currentTimeMillis() - t0));
        while (!dr.isEmpty()) {
            System.out.println(dr.remove());
        }

        // Second query test
        bl = new Blinks(1, bi.MulGraph.get(bi.searchLayer), 5);
        logger.fine("second query test");
        ArrayList<BlinksResult> cres1 = bl.semantic(bi.getGenKeywords(), bi.MulGraph.get(bi.searchLayer));
        Lcres = new LinkedList<AGraph>();
        for (BlinksResult c : cres1) {
            Lcres.add(c.ans);
        }
        System.out.println("Lcres size is:\t" + Lcres.size());
        dc = new Decompressor(Lcres, bi);
        dc.DecompressByLayerOri();
        dr = dc.getResults();
        System.out.println("[size]\t" + dr.size());
        while (!dr.isEmpty()) {
            System.out.println(dr.remove());
        }
        */
    }
}
