import algorithms.Blinks;
import config.CompressFactor;
import config.KWFactor;
import decompress.Decompressor;
import graph.AGraph;
import graph.OntGraph;
import graph.SGraph;
import index.BigIndex;
import result.BlinksResult;
import util.KWSelector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by samjjx on 2017/5/10.
 */
public class BlinkTest {
    static HashSet<Integer> keywords = new HashSet<Integer>();

    static void generateNumKeywords(int number) {
        keywords = new HashSet<Integer>();
        for (int i = 0; i < number; i++) {
            keywords.add(1 + i);
        }
    }

    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        /**
         Following is for local test
         */
        SGraph graph = new SGraph("/Users/samjjx/dev/dataset/fund/synt0", true);
        OntGraph ont = new OntGraph("/Users/samjjx/dev/dataset/fund/synt0.ont", true, 0, true);

        /**
         Following is for server deployment
         */
//        SGraph graph = new SGraph("/tmp/jxjian/yago/test_yago/yago_fact_data", true);
//        OntGraph ont = new OntGraph("/tmp/jxjian/yago/test_yago/yago_ont_invert", true, 0, true);
//        ont = ont.genTree(5672015);

        long t0 = System.currentTimeMillis();

        System.out.println("Blinks building time on Original Graph: \t" + (System.currentTimeMillis() - t0));
        KWSelector kwSelector = new KWSelector(ont, KWFactor.TEST_CASE, 2);
        keywords = kwSelector.SelectKW();

//        keywords.add(13);
//        keywords.add(3);
//        keywords.add(8);

        t0 = System.currentTimeMillis();

        BigIndex bi = new BigIndex(graph, ont, CompressFactor.CR);
//        System.out.println(bi);
        bi.keywordGen(keywords);
        System.out.println("Building Index time:" + (System.currentTimeMillis() - t0));

        t0 = System.currentTimeMillis();
        Blinks bl = new Blinks(1, bi.MulGraph.get(bi.searchLayer), 5);
        System.out.println("Blinks building time on Compressed Graph:" + (System.currentTimeMillis() - t0));

        t0 = System.currentTimeMillis();

        System.out.println("[tmp]"+bi.searchLayer);
        ArrayList<BlinksResult> cres = bl.semantic(bi.getGenKeywords(), bi.MulGraph.get(bi.searchLayer));

        System.out.println("Searching on the compressed time: " + (System.currentTimeMillis() - t0));
        System.out.println("[Search Layer]" + bi.searchLayer);
        Queue<AGraph> Lcres = new LinkedList<AGraph>();
        System.out.println("[Answer Info]");
        for (BlinksResult c : cres) {
            Lcres.add(c.ans);
            System.out.println(c);
        }
        System.out.println("Lcres size is:\t" + Lcres.size());

        Decompressor dc = new Decompressor(Lcres, bi);
        t0 = System.currentTimeMillis();
        dc.DecompressByLayerOri();
        System.out.println("Results decompression time: " + (System.currentTimeMillis() - t0));

        Queue<AGraph> dr = dc.getResults();
        System.out.println("[size]\t" + dr.size());
        System.out.println("Time cost\t" + (System.currentTimeMillis() - t0));
        while (!dr.isEmpty()) {
            System.out.println(dr.remove());
        }
    }
}
