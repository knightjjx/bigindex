filev = open("yago1.v", "r")
filee = open("yago1.e", "r")

fileg = open("yago1.graph", "w")

maxID = 0

for line in filev:
    data = line.split("\t")
    if int(data[0]) > maxID:
        maxID = int(data[0])

graph = {}

edge_count = 0
for line in filee:
    data = line.split("\t")
    src = int(data[0])
    dst = int(data[1])
    if not graph.__contains__(src):
        graph[src] = set([])

    graph[src].add(dst)
    edge_count += 1

fileg.write(str(maxID) +"\t"+ str(edge_count)+"\n")

for (k,v) in graph.items():
    fileg.write(str(k)+"\t" + str(len(v)))
    for v_ in v:
        fileg.write("\t" + str(v_))
    fileg.write("\n")

fileg.flush()
fileg.close()
