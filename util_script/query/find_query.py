import sys

basePath = sys.argv[1]

threshold = int(sys.argv[2])

vfile = open(basePath, "r")

freq = {}

for line in vfile:
    data = line.split("\t")
    attr = data[1].strip()
    if freq.__contains__(attr):
        freq[attr] = freq[attr] + 1
    else:
        freq[attr] = 1

ont_file = open("yago_ont_invert.t", "r")

ont_attr = {}
for line in ont_file:
    data = line.split("\t")
    attr_id = data[0].strip()
    attr = data[1].strip()
    ont_attr[attr_id] = attr

for (a, b) in freq.items():
    if (b > threshold):
        print "| " + str(a) + " | " + ont_attr[a] + " | " + str(b) + " |"
