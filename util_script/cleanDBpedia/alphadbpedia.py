# encoding=utf8
import sys

reload(sys)
sys.setdefaultencoding('utf8')


def clean(line):
    return filter(str.isalpha, str(line.split("/")[-1]).strip())

yfile = open(".t", "r")
ytranslate = {}
for line in yfile:
    data = line.split("\t")
    ytranslate[filter(str.isalpha, str(data[1]))] = data[0]

dafile = open(".attr", "r")
attr = {}

vfile = open(".v", "w")
count = 0
for line in dafile:
    attr = clean(line)
    if ytranslate.__contains__(attr):
        vfile.write(str(count) + "\t" + ytranslate[attr]+"\n")
        print "[True]"
    else:
        vfile.write(str(count) + "\t" + ytranslate["owl:Thing"])
        print "[False]"
    count += 1
    if count % 10000 == 0:
        print "[Finish]", count
vfile.flush()
vfile.close()


