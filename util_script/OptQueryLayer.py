
def readV(path):
    file = open(path, "r")
    attr = {}
    for line in file:
        data = line.strip().split("\t")
        if data[1] in attr:
            data[1] = data[1] + 1
        else:
            data[1] = 1
    return attr


ptn_base_path = "/tmp/jxjian/query/yago_optimal_query_layer/layer"

layer_ptn_number = [8, 8, 8, 8, 6, 4]
for i in range(1,6,1):
    path = "/tmp/jxjian/yago_bigindex/yago" + str(i) + ".v"
    attr = readV(path)

    print "Layer", i
    print "========================="
    num = layer_ptn_number[i-1]

    for j in range(1, num, 1):
        support = 0
        ptn_path = ptn_base_path+ str(i) +"/" + str(j) + ".ptn"
        fr = open(ptn_path, "r")
        for line in fr:
            data = line.strip().split("\t")
            for d in data:
                support += attr[d]

        print str(j) + ".ptn\t", support

layer_ptn_number.__len__()