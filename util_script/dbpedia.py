# encoding=utf8
import rdflib
import sys

reload(sys)
sys.setdefaultencoding('utf8')


def writeVertex(g, Gont_vertex, basePath):
    thing = "owl:Thing"
    vertexSet = {}
    count = 0

    non_attribute = 0

    path = basePath + ".v"
    file = open(path, "w")

    attribute_path = basePath + ".attr"
    file_attr = open(attribute_path, "w")

    for n in g.all_nodes():
        count += 1
        file_attr.write(str(n) + "\t" + str(count) + "\n")
        vertexSet[str(n)] = count
        if Gont_vertex.__contains__(str(n)):
            file.write(str(count) + "\t" + str(Gont_vertex[str(n)]) + "\n")
        else:
            file.write(str(count) + "\t" + str(Gont_vertex[thing]) + "\n")
            non_attribute += 1
    file_attr.flush()
    file_attr.close()
    file.flush()
    file.close()
    print "non_attribute", non_attribute
    return vertexSet


def writeEdge(g, vertexMap, basePath):
    path = basePath + ".e"
    file = open(path, "w")

    for s, p, o in g:
        file.write(str(vertexMap[str(s)]) + "\t" + str(vertexMap[str(o)]) + "\t1\n")
    file.flush()
    file.close()


def getTypePair(g):
    typekey = str("type")

    Gont = rdflib.Graph()
    removed_node = set([])
    for s, p, o in g:
        if typekey in p:
            g.remove((s, p, o))
            removed_node.add(o)
            Gont.add((s, p, o))

    for o in removed_node:
        g.remove((None, None, o))

    return (g, Gont)


def combineType(Gont, owl_path):
    file = open(owl_path, "r")

    for line in file:
        data = line.split("\t")
        Gont.add((rdflib.URIRef(data[0].strip()), rdflib.URIRef("<http://dbpedia.org/ontology/subClassOf>"),
                  rdflib.URIRef(data[1].strip())))

    return Gont


def writeOntology(Gont, basePath):
    vfile = open(basePath + ".v", "w")
    efile = open(basePath + ".e", "w")
    count = 0
    vertexSet = {}
    for n in Gont.all_nodes():
        count += 1
        vertexSet[str(n)] = count
        vfile.write(str(count) + "\t" + str(count) + "\n")
    vfile.flush()
    vfile.close()

    for s, p, o in Gont:
        efile.write(str(vertexSet[str(o)]) + "\t" + str(vertexSet[str(s)]) + "\t1\n")
    efile.flush()
    efile.close()
    return vertexSet


if __name__ == "__main__":
    pathbase = "dbpedia_clean"
    pathbase = sys.argv[1]
    dataPath = sys.argv[2]
    g = rdflib.Graph()
    g.parse(dataPath, format="nt")
    print "Finish Loading"
    (g, Gont) = getTypePair(g)
    print "Finish extracting types from data graph"
    Gont = combineType(Gont, "ontology.pair")
    print "Finish Combination"
    Gont_vertex = writeOntology(Gont, pathbase + ".ont")
    print "Finish Writting Ontology"
    vertex_map = writeVertex(g, Gont_vertex, pathbase)
    print "Finish Writting Data Vertices"
    writeEdge(g, vertex_map, pathbase)
    print "Finish Writting Data Edges"
