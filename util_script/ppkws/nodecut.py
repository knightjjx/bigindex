import sys

path = sys.argv[1]

n_frag = str(sys.argv[2])

rfile = open(path+".r"+n_frag, "r")
rTable = {}
vTable = {}
for line in rfile:
    data = line.split("\t")
    vid = data[0].strip()
    rid = data[1].strip()
    if rid not in rTable:
        rTable[rid] = set([])
    rTable[rid].add(vid)
    vTable[vid] = rid
print("=======Block Size========")
bid = 0
for key in rTable:
    print("bid\t"+str(bid)+"\t"+str(len(rTable[key])))
    bid += 1


node_cut = set([])
efile = open(path+".e", "r")
for line in efile:
    data = line.split("\t")
    src = data[0].strip()
    dst = data[1].strip()

    if vTable[src] != vTable[dst]:
        if(node_cut.__contains__(src) or node_cut.__contains__(dst)):
            continue
        node_cut.add(src)

print(len(node_cut))

nc= open(path+".nc"+n_frag, "w")

for node in node_cut:
    nc.write(node+"\n")

nc.flush()
nc.close()

