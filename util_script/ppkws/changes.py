import sys

def loadOneData(path):
    y = open(path + "/dblp-public-author-list", "r")
    y_data = {}

    for line in y:
        data = line.split("#")
        y_data[data[1]] = set(data[5].split(";"))
    return y_data

def getOneAuthor(author, y1, y2, datasets):
    try:
        d1 = datasets[y1]
        d2 = datasets[y2]
        print "check 1"
        y1_data = d1[author]
        y2_data = d2[author]
        print "check 2"
        print "Before Y1\t", y1_data
        print "Before Y2\t", y2_data
        print "Changes\t", y2_data - y1_data
        print "Common\t", y1_data & y2_data
        return True
    except KeyError:
        return False

''' loading data'''

print "Loading data"
'''"0|Guy Ferland|2013|2014|"'''
datasets = {}
# datasets[2013] = {}
# datasets[2014] = {}
# datasets[2015] = {}
# datasets[2016] = {}
datasets["2013"] = loadOneData("/home/comp/jxjian/dblp-top10/dblp2013")
datasets["2014"] = loadOneData("/home/comp/jxjian/dblp-top10/dblp2014")
datasets["2015"] = loadOneData("/home/comp/jxjian/dblp-top10/dblp2015")
datasets["2016"] = loadOneData("/home/comp/jxjian/dblp-top10/dblp2016")

while True:
    try:
        print "1 author_name time_point_1 time_point_2"
        str = input("Input a query author and two years:")

        ''' 1 author_name time_point_1 time_point_2'''
        data = str.split("|")
        if data[0] == "-1":
            exit(-1)
        if not getOneAuthor(data[1], data[2], data[3],datasets):
            print "Not such a author"
    except Exception:
        print "Fail, try again"





