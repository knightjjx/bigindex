#!/usr/bin/python
import sys
def readTSV(dir):
    edge = set([])
    instance_map = {}
    instance_invert = {}
    count = 0
    with open(dir) as tsvReader:
        for data in tsvReader:
            tri = data.split("\t")

            if not instance_map.has_key(tri[1]):
                instance_map[tri[1]] = count
                instance_invert[count] = tri[1]
                count += 1
            if not instance_map.has_key(tri[3]):
                instance_map[tri[3]] = count
                instance_invert[count] = tri[3]
                count += 1
            edge.add((instance_map[tri[1]],instance_map[tri[3]]))

    return [edge, instance_map, instance_invert]

def readTypeTSV(dir, data_graph):
    edge = set([])
    instance_map = {}
    instance_invert = {}
    count = 0
    with open(dir) as tsvReader:
        for data in tsvReader:
            tri = data.split("\t")
            if not data_graph[1].has_key(tri[1]):
                continue

            if not instance_map.has_key(tri[1]):
                instance_map[tri[1]] = count
                instance_invert[count] = tri[1]
                count += 1
            if not instance_map.has_key(tri[3]):
                instance_map[tri[3]] = count
                instance_invert[count] = tri[3]
                count += 1
            edge.add((instance_map[tri[1]],instance_map[tri[3]]))

    return [edge, instance_map, instance_invert]



def combineGraph(graph1, graph2):
    instance_new_map = graph1[1]
    instance_new_invert = graph1[2]

    count = len(graph1[1])

    edge = graph1[0]

    for instance in graph2[1]:
        if not instance_new_map.has_key(instance):
            instance_new_map[instance] = count
            instance_new_invert[count] = instance
            count += 1

    translate = graph2[2]
    for (src,dst) in graph2[0]:
        edge.add((instance_new_map[translate[src]],instance_new_map[translate[dst]]))

    return [edge, instance_new_map, instance_new_invert]

def writeTofile(graph, dir):
    ePath = dir + ".e"
    efile = open(ePath,"wb")
    vPath = dir + ".v"

    for (src,dst) in graph[0]:
        efile.write(str(src)+"\t"+str(dst)+"\t1\n")
    efile.flush()
    efile.close()
    vfile = open(vPath,"wb")
    for (key,value) in graph[2].iteritems():
        vfile.write(str(key)+"\t"+str(key)+"\n")
    vfile.flush()
    vfile.close()

    translate = graph[2]
    tPath = dir + ".t"
    tFile = open(tPath,"wb")
    for (key, value) in translate.iteritems():
        tFile.write(str(key)+"\t"+value+"\n")

def invert_translate(translate):
    return {v: k for k, v in translate.iteritems()}

def writeDataGraph(graph, translate, dir):
    translate_invert = invert_translate(translate)
    ePath = dir + ".e"
    efile = open(ePath, "wb")
    vPath = dir + ".v"
    vfile = open(vPath, "wb")
    rPath = dir + ".r"
    rfile = open(rPath, "wb")

    for (src,dst) in graph[0]:
        efile.write(str(src)+"\t"+str(dst)+"\t1\n")
    efile.flush()
    efile.close()
    count1 = 0
    count2 = 0
    for (key,value) in graph[2].iteritems():
        try:
            vfile.write(str(key)+"\t"+str(translate_invert[value])+"\n")
            count1 += 1
        except KeyError:
            vfile.write(str(key) + "\t" + str(translate_invert["owl:Thing"]) + "\n")
            count2 += 1
    print "UFOUND", count2
    print "FOUND", count1

    vfile.flush()
    vfile.close()

    for (key, value) in graph[2].iteritems():
        rfile.write(str(key) + "\t" + str(1) + "\n")
    rfile.flush()
    rfile.close()

if __name__ == "__main__":
    fact_path = sys.argv[1]
    graph_fact = readTSV(fact_path)
    graph_fact = readTSV("/Users/samjjx/dev/dataset/yago/yagoFacts.tsv")
    print len(graph_fact[0])
    graph = readTypeTSV("/Users/samjjx/dev/dataset/yago/yagoTypes.tsv", graph_fact)
    print len(graph[0])
    graph2 = readTSV("/Users/samjjx/dev/dataset/yago/yagoTaxonomy.tsv")
    print len(graph2[0])
    graph3 = combineGraph(graph, graph2)
    print len(graph3[0])
    writeDataGraph(graph_fact, graph3[2], "/Users/samjjx/dev/dataset/yago/ontology/yago_fact_data")