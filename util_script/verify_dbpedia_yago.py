# encoding=utf8
import sys
import rdflib

reload(sys)
sys.setdefaultencoding('utf8')

if __name__ == "__main__":
    dpath = sys.argv[1]

    ypath = sys.argv[2]

    dfile = open(dpath, "r")
    yfile = open(ypath, "r")

    dattr = set([])
    yattr = set([])
    for line in dfile:
        tmp = filter(str.isalpha, str(line.split("/")[-1]).strip())
        if tmp == "":
            dattr.add(str(line.split("/")[-1]).strip())
            continue
        dattr.add(tmp)

    for line in yfile:
        print filter(str.isalpha, str(line.split("\t")[1].split("<")[1].split(">")[0]))
        yattr.add(filter(str.isalpha, str(line.split("\t")[1].split("<")[1].split(">")[0])))

    count = 0

    for attr in dattr:
        if yattr.__contains__(attr):
            print "[True]", attr
            count += 1
        else:
            print "[False]", attr

    print "[Match]", str(count)

    # print yattr
    # print dattr