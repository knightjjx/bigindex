#!/usr/bin/python
import sys
import random

root = 0


def generateOnt(depth, numTypes, maxDegree, numRelation):
    types = []
    types.append(root)
    allDepth = {}
    edges = {}
    attr = {}
    edges[root] = set([])
    allDepth[root] = 0

    for i in range(0, numTypes+1):
        types.append(i)
        attr[i] = i
        edges[i] = set([])
        allDepth[i] = sys.maxint

    for i in range(1, numRelation):
        src = random.randint(0, numTypes-1)
        dst = random.randint(0, numTypes-1)
        if src > dst:
            src, dst = dst, src
        while src == dst or edges[src].__contains__(dst) or len(edges[src]) > maxDegree:
            src = random.randint(0, numTypes-1)
            dst = random.randint(0, numTypes-1)

        edges[src].add(dst)
        print i

    return edges, attr


def generateGraph(numVertex, numEdge, numAttr):
    edges = {}
    vertices = []
    attr = {}

    for i in range(0, numVertex):
        vertices.append(i)
        edges[i] = set([])
        attr[i] = random.randint(0, numAttr)

    for i in range(1, numEdge):
        src = random.randint(0, numVertex-1)
        dst = random.randint(0, numVertex-1)
        if src > dst:
            src, dst = dst, src
        while src == dst or edges[src].__contains__(dst):
            src = random.randint(0, numVertex-1)
            dst = random.randint(0, numVertex-1)
            if src > dst:
                src, dst = dst, src
        edges[src].add(dst)
    return edges, attr

def writeVertices(path, vertices):
    fw = open(path, "w")
    fw.write(str(0)+"\t"+ str(1) +"\n")
    for (key, value) in vertices.iteritems():
        fw.write(str(key)+"\t"+ str(value) +"\n")
    fw.flush()
    fw.close()

def writeRTable(path, numVertices):
    fw = open(path, "w")
    for i in range(0, numVertices):
        fw.write(str(i) + "\t" + str(0) + "\n")
    fw.flush()
    fw.close()

def writeEdges(path, edges):
    fw = open(path, "w")
    for (src, dsts) in edges.iteritems():
        for dst in dsts:
            fw.write(str(src) + "\t" + str(dst) + "\t" + str(1) + "\n")
    fw.flush()
    fw.close()

if __name__ == "__main__":
    # Output path
    path_base = sys.argv[1]

    # Data graph information
    num_vertices = int(sys.argv[2])
    num_edges = int(sys.argv[3])

    # Ontology graph information
    ont_depth = int(sys.argv[4])
    num_types = int(sys.argv[5])
    num_relations = int(sys.argv[6])
    max_degree = int(sys.argv[7])

    # Generate ontology graph and data graph
    ontology, ontVertex = generateOnt(ont_depth, num_types, max_degree, num_relations)
    print "Finish Generation"
    graph, vertex = generateGraph(num_vertices, num_edges, num_types)
    print "Finish Generation"
    # Write graph
    writeVertices(path_base+".v", vertex)
    print "Finish Graph Vertex"
    writeEdges(path_base+".e", graph)
    print "Finish Graph Edges"
    writeRTable(path_base+".r", num_vertices)
    print "Finish R Table"
    # Write ontology
    writeVertices(path_base+".ont.v", ontVertex)
    print "Finish OntGraph Vertices"
    writeEdges(path_base+".ont.e", ontology)
    print "Finish OntGraph Edges"
