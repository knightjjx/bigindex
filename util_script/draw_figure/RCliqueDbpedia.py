# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 24
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)


def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, blinks, BiGindex, topK):
    setting(plt)
    x_label = []
    for i in range(10):
        x_label.append("Q"+str(i + 1))

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    rects1 = plt.bar(index, blinks, bar_width, alpha=opacity, color='g', label='w/o BiGindex')
    # rects2 = plt.bar(index + bar_width, BiGindex, bar_width, alpha=opacity, color='b')
    plt.xlabel('Query(id)')
    plt.ylabel(r'Query Time($10^3$ sec)')
    plt.title('TopK = ' + str(topK), fontsize = MEDIUM_SIZE)

    # plt.legend((rects1,rects2),("RClique","BiGindex + RClique"));
    Bigindex = np.array([0.361410, 0.600290, 2.71082, 0.43375, 0.90032, 4.52833, 10.03288, 7.83328])
    exploration_time = np.array([0.220341, 0.382982, 0.9732, 0.1922, 0.5582, 2.83239, 7.89851, 5.79822])
    pruning = np.array([0.00015, 0.000143, 0.000129, 0.000125, 0.000098, 0.000123, 0.000184, 0.000133])
    generation = Bigindex - exploration_time - pruning

    e_legend = plt.bar(index + bar_width, exploration_time, bar_width, color='red', label='BiGindex (Step 1)')
    p_legend = plt.bar(index + bar_width, pruning, bar_width, bottom=exploration_time, color='black', label='BiGindex (Pruning)')
    a_legend = plt.bar(index + bar_width, generation, bar_width, bottom=(exploration_time + pruning), color='blue',
                       label='BiGindex (Ans. Gen.)')
    plt.legend(loc=[0, 0.675], prop={'size': 16})
    plt.xticks(index + bar_width, x_label)
    plt.ylim(0, 20)

    plt.tight_layout();
    plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/r-clique-dbpedia.eps', dpi=300)
    plt.show();


n_groups = 8
rclique = (0.341252, 0.734394, 3.842190, 0.69120, 1.235330, 5.278360, 11.820996, 9.93583)
BiGindex = (0.361410, 0.600290, 2.71082, 0.43375, 0.90032, 4.52833, 10.03288, 7.83328)

sum = 0.0
for i in range(rclique.__len__()):
    sum+= (1-BiGindex[i]/rclique[i])
print sum/8

drawPillar(n_groups, rclique, BiGindex, 10)
