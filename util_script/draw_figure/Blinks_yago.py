# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 30
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)

def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, blinks, BiGindex, threshold, topK):
    setting(plt)
    x_label = []
    for i in range(10):
        x_label.append("Q"+str(i + 1))

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    #rects2 = plt.bar(index + bar_width, BiGindex, bar_width, alpha=opacity, color='b')
    plt.xlabel('Query(id)')
    plt.ylabel('Query Time(ms)')
    plt.title('Threshold = ' + str(threshold) + ', TopK = ' + str(topK), fontsize = SMALL_SIZE)

    my_y_ticks = np.arange(0, 500, 100)
    plt.yticks(my_y_ticks)
    plt.ylim(0, 400);


    plt.tight_layout();

    Bigindex = np.array([81, 52, 67, 54, 82, 113, 128, 107])
    exploration_time = np.array([53, 36, 44, 28, 67, 86, 78, 60])
    pruning = np.array([8, 7, 3, 5, 4, 6, 8, 9])
    generation = Bigindex - exploration_time - pruning

    rects1 = plt.bar(index, blinks, bar_width, alpha=opacity, color='g',label='w/o BiGindex')

    e_legend = plt.bar(index + bar_width, exploration_time, bar_width, color='red', label='BiGindex (Step 1)')
    p_legend = plt.bar(index + bar_width, pruning, bar_width, bottom=exploration_time, color='black',
                       label='BiGindex (Pruning)')
    a_legend = plt.bar(index + bar_width, generation, bar_width, bottom=(exploration_time + pruning), color='blue',
                       label='BiGindex (Ans. Gen.)')
    plt.legend(loc=[0, 0.675], prop={'size': 16})
    plt.xticks(index + bar_width, x_label)

    plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/yago_blinks_threshold_'+ str(threshold)+'.eps', dpi=150)
    plt.show();

n_groups = 8
blinks = (181, 151, 172, 192, 249, 277, 288, 259)
BiGindex = (81, 52, 67, 54, 82, 113, 128, 107)
drawPillar(n_groups, blinks, BiGindex, 10, 100)
'''
53% 65% 61% 72% 67% 59% 56% 59% avg reduce 61%
'''
#
# blinks = (199, 166, 161, 178, 213, 281, 283, 246)
# BiGindex = (79, 80, 75, 98, 75, 102, 118, 108)
# drawPillar(n_groups, blinks, BiGindex, 20, 100)
