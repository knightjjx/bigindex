# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 30
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)

def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, blinks, BiGindex, threshold, topK):
    setting(plt)
    x_label = []
    for i in range(10):
        x_label.append("Q"+str(i + 1))

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    rects1 = plt.bar(index, BiGindex, bar_width, alpha=opacity, color='g')
    rects2 = plt.bar(index + bar_width, blinks, bar_width, alpha=opacity, color='b')
    plt.xlabel('Query(id)')
    plt.ylabel('Query Time(ms)')
    # plt.title('Specialization Ordering', fontsize = SMALL_SIZE)
    plt.xticks(index + bar_width, x_label)
    plt.ylim(0, 250);

    plt.legend((rects1,rects2),("With Ordering","Without Ordering"));

    plt.tight_layout();
    plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/dfs_yago_blinks_threshold_'+ str(threshold)+'.eps', dpi=150)
    plt.show();

n_groups = 8
baseline = (81, 52, 67, 54, 82, 113, 128, 107)
opt = (58, 54, 55, 38, 72, 86, 135, 90)


sum = 0.0
for i in range(opt.__len__()):
    sum+= (1-float(opt[i])/float(baseline[i]))
print sum/8

drawPillar(n_groups, baseline, opt, 10, 100)
