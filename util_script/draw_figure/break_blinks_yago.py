import matplotlib.pyplot as plt
import numpy as np

x = [1, 2, 3, 4, 5, 6, 7, 8]

SMALL_SIZE = 30
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)


setting(plt)
x_label = []
for i in range(10):
    x_label.append("Q"+str(i + 1))

BiGindex = np.array([81, 52, 67, 54, 82, 113, 128, 107])
exploration_time = np.array([53, 36, 44, 28, 67, 86, 78, 60])
pruning = np.array([8, 7, 3, 5, 4, 6, 8, 9])
generation = BiGindex - exploration_time - pruning
print generation
bar_width = 0.35
x = np.arange(8)
plt.xticks(x + bar_width, x_label)
plt.xlabel('Query(id)')
plt.ylabel('Query Time(ms)')
plt.title('Yago & Blinks', fontsize = SMALL_SIZE)
plt.ylim(0, 300)

e_legend = plt.bar(x, exploration_time, color='green', label='Exploration')
p_legend = plt.bar(x, pruning, bottom=exploration_time, color='red', label='Pruning time')
a_legend = plt.bar(x, generation, bottom=(exploration_time + pruning), color='blue', label='Answer Generation')


plt.legend((e_legend, p_legend, a_legend),("Exploration","Pruning","Generation"),bbox_to_anchor=(1.063, 0.999),loc='upper right');
# plt.legend(loc=[1, 0])
plt.tight_layout();
plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/break_down_performance_yago_blinks.eps', dpi=150)
plt.show()
