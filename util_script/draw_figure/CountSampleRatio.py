
def DrawOne(path, fw):

    file = open(path)

    ori_size = 0
    bsim_size = 0
    count = 0
    whole_ratio = 0

    ori_list = []
    bsim_list = []

    while True:
        line = file.readline()
        line = line.strip()
        data = line.split("\t")
        count += 1
        if int(data[0]) > 10000:
            tmp = file.readline().strip().split("\t")
            whole_ratio = (float(tmp[0])+float(tmp[1]))/(float(data[0])+float(data[1]))
            fw.write(str(float(bsim_size)/float(ori_size))+"\t"+str(whole_ratio)+"\n")
            break
        if count % 2 == 1:
            ori_size += (int(data[0]) + int(data[1]))
        if count % 2 == 0:
            bsim_size += (int(data[0]) + int(data[1]))
        if count % 10 == 0:
            ori_list.append(ori_size)
            bsim_list.append(bsim_size)

    file.close()
    ratio = [float(b) / float(m) for b, m in zip(bsim_list, ori_list)]


fw = open("1all_ratio.log", 'w')

base1 = "./sample_log"
for i in range(1,50,1):
    path = base1+"/"+str(i)+"exp_statistic.log"
    DrawOne(path, fw)

base2 = "./stastic/sample_log"
for i in range(1,67,1):
    path = base1+"/"+str(i)+"exp_statistic.log"
    try:
        DrawOne(path, fw)
    except:
        print path


fw.flush()
fw.close()

