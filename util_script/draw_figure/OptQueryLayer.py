# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random

from IPython.core.pylabtools import figsize

SMALL_SIZE = 25
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=14)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)


def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, layers, threshold, topK):

    setting(plt)
    x_label = []
    for i in range(10):
        x_label.append("Q"+str(i + 1))


    ax = plt.subplot(111)
    index = np.arange(n_groups)
    bar_width = 0.1

    opacity = 0.2
    rects={}
    for i in range(1 ,7, 1):
        rects[i] = plt.bar(index + bar_width*i, layers[i], bar_width, alpha=opacity, color=(random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1)))

    plt.xlabel('Query(id)')
    plt.ylabel('Query Time(ms)')
    plt.xticks(index + bar_width*3, x_label)
    my_y_ticks = np.arange(0, 1600, 200)
    plt.yticks(my_y_ticks)
    plt.ylim(0, 600);

    plt.legend((rects[1],rects[2],rects[3],rects[4],rects[5],rects[6]),("layer1","layer2","layer3","layer4","layer5","layer6","layer7"),bbox_to_anchor=(1.113, 0.999),loc='upper right');


    plt.tight_layout();

    fig = plt.gcf()
    fig.set_size_inches(11.2, 4)
    fig.subplots_adjust(bottom=0.2)
    fig.subplots_adjust(right=0.9)
    fig.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/opt_layer.eps', dpi=200)
    # plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/opt_layer.eps', dpi=150)
    plt.show();

n_groups = 8
layers={}
layers[1] = (305, 209, 188, 169, 230, 414, 459, 482)
layers[2] = (230, 127, 143, 135, 170, 284, 317, 343)
layers[3] = (107, 97, 83, 130, 125, 113, 92, 162)
layers[4] = (82, 44, 159, 56, 128, 165, 72, 152)
layers[5] = (104, 44, 57, 117, 326, 64, -1, -1)
layers[6] = (75, 39, 120, 59, -1, -1, -1, -1)

drawPillar(n_groups, layers, 10, 100)
