# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 24
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('figure', titlesize=BIGGER_SIZE)


def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, blinks, BiGindex, topK):
    setting(plt)
    x_label = []
    for i in range(10):
        x_label.append("Q"+str(i + 1))

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    rects1 = plt.bar(index, blinks, bar_width, alpha=opacity, color='g', label='w/o BiGindex')
    # rects2 = plt.bar(index + bar_width, BiGindex, bar_width, alpha=opacity, color='b')

    Bigindex = np.array([0.00069, 0.0083, 0.09732, 0.6823, 1.23737, 1.85260, 0.91054, 1.27260])
    exploration_time = np.array([0.00024, 0.0038, 0.05337, 0.4822, 0.83289, 1.33282, 0.58385, 0.79822])
    pruning = np.array([0.00008, 0.000093, 0.000139, 0.000073, 0.000068, 0.000143, 0.000068, 0.000173])
    generation = Bigindex - exploration_time - pruning

    e_legend = plt.bar(index + bar_width, exploration_time, bar_width, color='red', label='BiGindex (Step 1)')
    p_legend = plt.bar(index + bar_width, pruning, bar_width, bottom=exploration_time, color='black',
                       label='BiGindex (Pruning)')
    a_legend = plt.bar(index + bar_width, generation, bar_width, bottom=(exploration_time + pruning), color='blue',
                       label='BiGindex (Ans. Gen.)')



    plt.legend(loc=[0, 0.675], prop={'size': 16})

    plt.xlabel('Query(id)')
    plt.ylabel('Query Time($10^3$ sec)')
    plt.title('TopK = ' + str(topK), fontsize = MEDIUM_SIZE)
    plt.xticks(index + bar_width, x_label)
    my_y_ticks = np.arange(0, 5, 1)
    plt.yticks(my_y_ticks)
    plt.ylim(0, 4);

    # plt.legend((rects1,rects2),("RClique","BiGindex + RClique"));

    plt.tight_layout();
    plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/r-clique.eps', dpi=300)
    plt.show();


n_groups = 8
rclique = (0.00113, 0.01359, 0.16523, 0.98420, 2.17334, 3.39012, 1.55305, 1.97087)
BiGindex = (0.00069, 0.0083, 0.09732, 0.6823, 1.23737, 1.85260, 0.91054, 1.27260)

sum = 0.0
for i in range(rclique.__len__()):
    sum+= (1-BiGindex[i]/rclique[i])
print sum/8
drawPillar(n_groups, rclique, BiGindex, 10)
