# -*- coding:utf-8 -*-

from matplotlib.font_manager import FontManager, FontProperties
import matplotlib.pyplot as plot
import numpy as np
import matplotlib.pyplot as plt
import matplotlib


SMALL_SIZE = 24
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=MEDIUM_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)

def getChineseFont():
    return FontProperties(fname='/System/Library/Fonts/PingFang.ttc')


def drawPillar(n_groups, blinks, dataset, largest):
    setting(plot)

    x_label = []
    for i in range(7):
        x_label.append(str(i + 1))

    fig, ax = plot.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    rects1 = plot.bar(index, blinks, bar_width, alpha=opacity, color='b', label=dataset, align='center')
    plot.xlabel('Layer')
    plot.ylabel('Index Size(M)')
    plot.title('Index size |E| of BiGindex', fontsize = MEDIUM_SIZE)
    plot.xticks(index , x_label)
    plot.ylim(0, largest);
    plot.legend();

    plot.tight_layout();
    plot.savefig('/Users/samjjx/dev/git/BiGindex-draft/vldb_format/figures/index-edge-' + dataset + '.eps', dpi=150)
    plot.show();


if __name__ == '__main__':

    y_dbpedia_size = [15.8, 11.3, 10.5, 9.7, 9.1, 8.5, 8.0]
    y_yago_size = [5.3, 1.8, 1.5, 1.24, 1.03, 0.88, 0.81]
    ngroup = 7

    drawPillar(ngroup, y_dbpedia_size, "Dbpedia", 18)
    drawPillar(ngroup, y_yago_size, "Yago", 6)
