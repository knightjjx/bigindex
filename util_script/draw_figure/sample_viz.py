import numpy as np
import matplotlib.pyplot as plt
import random


SMALL_SIZE = 20
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=10)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)

def DrawOne(path, plt):
    setting(plt)
    file = open(path)

    ori_size = 0
    bsim_size = 0
    count = 0
    whole_ratio = 0

    ori_list = []
    bsim_list = []

    while True:
        line = file.readline()
        line = line.strip()
        data = line.split("\t")
        count += 1
        if int(data[0]) > 10000:
            tmp = file.readline().strip().split("\t")
            whole_ratio = (float(tmp[0])+float(tmp[1]))/(float(data[0])+float(data[1]))
            break
        if count % 2 == 1:
            ori_size += (int(data[0]) + int(data[1]))
        if count % 2 == 0:
            bsim_size += (int(data[0]) + int(data[1]))
        if count % 10 == 0:
            ori_list.append(ori_size)
            bsim_list.append(bsim_size)


    ratio = [float(b) / float(m) for b, m in zip(bsim_list, ori_list)]
    plt.ylim(0.3, 0.6);
    index = np.arange(ori_list.__len__())*5

    colors = (random.uniform(0.5, 1),random.uniform(0.5, 1),random.uniform(0.5, 1))
    rects1, = plt.plot(index, ratio, color=colors)

    x = [0, ori_list.__len__()*5]
    y = [whole_ratio, whole_ratio]
    rects2, = plt.plot(x, y, color=colors)
    # plt.legend((rects1, rects2), ("sample", "whole graph"));

'''
path = './stastic/7exp_statistic.log'
DrawOne(path, plt)
path = './stastic/6exp_statistic.log'
DrawOne(path, plt)
path = './stastic/5exp_statistic.log'
DrawOne(path, plt)
path = './stastic/2exp_statistic.log'
DrawOne(path, plt)
path = './stastic/1exp_statistic.log'6
DrawOne(path, plt)
'''

for i in range(1,20,1):
    path = './sample_log/'+str(i)+'exp_statistic.log'
    DrawOne(path, plt)

fig = plt.gcf()
fig.set_size_inches(8, 4)
fig.subplots_adjust(bottom=0.3)


font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 16,
        }
plt.text(80, 0.51, r'estimated compresstion ratio', fontdict=font)
plt.text(107, 0.38, r'real compression ratio', fontdict=font)
# plt.title('Sample size vs "the avg. compression ratio of the sample graphs"', fontsize = 16)
plt.ylabel('Compression Ratio')
plt.xlabel('Number of sample graphs($n$)')
# plt.text(5,5, "estimated compresstion ratio",horizontalalignment='center', fontsize=20)
plt.draw()
plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/TKDE/figures/sample_1000.eps', dpi=150)
plt.show()

