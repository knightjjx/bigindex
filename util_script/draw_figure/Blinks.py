# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 30
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)

def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, blinks, BiGindex, threshold, topK):
    setting(plt)
    x_label = []
    for i in range(10):
        x_label.append("Q"+str(i + 1))

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    rects1 = plt.bar(index, blinks, bar_width, alpha=opacity, color='g', label='w/o BiGindex')
    # rects2 = plt.bar(index + bar_width, BiGindex, bar_width, alpha=opacity, color='b')
    Bigindex = np.array([0.433, 0.385, 0.457, 0.457, 0.451, 0.555, 0.635, 0.681])
    exploration_time = np.array([0.232, 0.251, 0.337, 0.223, 0.329, 0.393, 0.385, 0.423])
    pruning = np.array([0.051, 0.023, 0.014, 0.033, 0.018, 0.014, 0.028, 0.017])
    generation = Bigindex - exploration_time - pruning

    e_legend = plt.bar(index + bar_width, exploration_time, bar_width, color='red', label='BiGindex (Step 1)')
    p_legend = plt.bar(index + bar_width, pruning, bar_width, bottom=exploration_time, color='black',
                       label='BiGindex (Pruning)')
    a_legend = plt.bar(index + bar_width, generation, bar_width, bottom=(exploration_time + pruning), color='blue',
                       label='BiGindex (Ans. Gen.)')
    plt.legend(loc=[0, 0.675], prop={'size': 16})
    plt.xlabel('Query(id)')
    plt.ylabel('Query Time(sec)')
    plt.title('Threshold = ' + str(threshold) + ', TopK = ' + str(topK), fontsize = SMALL_SIZE)
    plt.xticks(index + bar_width, x_label)
    plt.ylim(0, 3);

    # plt.legend((rects1,rects2),("Blinks","BiGindex + Blinks"));

    plt.tight_layout();
    plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/blinks_threshold_'+ str(threshold)+'.eps', dpi=150)
    plt.show();


n_groups = 8
blinks = (0.972, 0.795, 1.010, 1.541, 0.958, 1.239, 1.885, 1.422)
BiGindex = (0.433, 0.385, 0.457, 0.457, 0.451, 0.555, 0.635, 0.681)
drawPillar(n_groups, blinks, BiGindex, 10, 100)
'''
55% 52% 55% 70% 53% 55% 66% 52% avg reduce
'''

#
# blinks = (0.952, 0.805, 1.625, 0.960, 0.994, 1.164, 1.259, 1.457)
# BiGindex = (0.431, 0.390, 0.467, 0.477, 0.465, 0.736, 0.646, 0.692)
# drawPillar(n_groups, blinks, BiGindex, 20, 100)