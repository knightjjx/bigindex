'''
dataset: 1M | 2M | 4M | 8M |
blinks:  1.545s |  2.730s | 4.618s  |   1.246s |
blinks+bigindex: 0.812s | 1.48s | 3.696s | 5.542s |
'''


# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

SMALL_SIZE = 24
MEDIUM_SIZE = 35
BIGGER_SIZE = 48

BOLD = 'bold'
LIGHT = 'light'

def setting(plt):
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)


def boldSetting():
    font = {'weight': 'bold',
            'size': 17}
    matplotlib.rc('font', **font)

def smallSetting():
    font = {'size': 24}
    matplotlib.rc('font', **font)


def drawPillar(n_groups, blinks, BiGindex, topK):
    setting(plt)
    x_label = []
    x_label.append("S-1M")
    x_label.append("S-2M")
    x_label.append("S-4M")
    x_label.append("S-8M")

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4

    rects1 = plt.bar(index, blinks, bar_width, alpha=opacity, color='g')
    rects2 = plt.bar(index + bar_width, BiGindex, bar_width, alpha=opacity, color='b')
    plt.xlabel('Synthetic datasets')
    plt.ylabel('Query Time($sec$)')
    plt.title('TopK = ' + str(topK) +" answer graphs", fontsize = MEDIUM_SIZE)
    plt.xticks(index + bar_width, x_label)
    plt.ylim(0, 500);

    plt.legend((rects1,rects2),("RClique","BiGindex + RClique"));

    plt.tight_layout();
    plt.savefig('/Users/samjjx/dev/git/BiGindex-draft/ICDE/figures/synt_scale_rclique.eps', dpi=300)
    plt.show();


n_groups = 4
rclique = (110.919, 132.884, 196.613, 270.496)
BiGindex = (58.332, 60.852, 103.917, 188.384)
drawPillar(n_groups, rclique, BiGindex, 10)
'''
dataset: 1M | 2M | 4M | 8M |
blinks:  1.545s |  2.730s | 4.618s  |   1.246s |
blinks+bigindex: 0.812s | 1.48s | 3.696s | 5.542s |
'''
