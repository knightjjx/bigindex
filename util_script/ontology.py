#!/usr/bin/python
import sys
def readTSV(dir):
    edge = set([])
    instance_map = {}
    instance_invert = {}
    count = 0
    with open(dir) as tsvReader:
        for data in tsvReader:
            tri = data.split("\t")

            if not instance_map.has_key(tri[1]):
                instance_map[tri[1]] = count
                instance_invert[count] = tri[1]
                count += 1
            if not instance_map.has_key(tri[3]):
                instance_map[tri[3]] = count
                instance_invert[count] = tri[3]
                count += 1
            edge.add((instance_map[tri[1]],instance_map[tri[3]]))


    return [edge, instance_map, instance_invert]

def combineGraph(graph1, graph2):
    instance_new_map = graph1[1]
    instance_new_invert = graph1[2]

    count = len(graph1[1])

    edge = graph1[0]

    for instance in graph2[1]:
        if not instance_new_map.has_key(instance):
            instance_new_map[instance] = count
            instance_new_invert[count] = instance
            count += 1

    translate = graph2[2]
    for (src,dst) in graph2[0]:
        edge.add((instance_new_map[translate[src]],instance_new_map[translate[dst]]))

    return [edge, instance_new_map, instance_new_invert]

def writeTofile(graph, dir):
    ePath = dir + ".e"
    efile = open(ePath,"wb")
    vPath = dir + ".v"

    #!!!!!!!!!!!!!!!!! Important !!!!!!!!!!!!!!!!!!!!!!!
    # remember this file need to invert when write the data graph

    for (src,dst) in graph[0]:
        efile.write(str(dst)+"\t"+str(src)+"\t1\n")
    efile.flush()
    efile.close()
    vfile = open(vPath,"wb")
    for (key,value) in graph[2].iteritems():
        vfile.write(str(key)+"\t"+str(key)+"\n")
    vfile.flush()
    vfile.close()

    translate = graph[2]
    tPath = dir + ".t"
    tFile = open(tPath,"wb")
    for (key, value) in translate.iteritems():
        tFile.write(str(key)+"\t"+value+"\n")

if __name__ == "__main__":
    # graph = readTSV("/Users/samjjx/dev/dataset/yago/yagoTypes.tsv")
    # print len(graph[0])
    # graph2 = readTSV("/Users/samjjx/dev/dataset/yago/yagoTaxonomy.tsv")
    # print len(graph2[0])
    # graph3 = combineGraph(graph, graph2)
    # print len(graph3[0])
    # writeTofile(graph3, "/Users/samjjx/dev/dataset/yago/ontology/yago_ont")
    basePath = sys.argv[1]
    graph = readTSV(basePath+"yagoTypes.tsv")
    print len(graph[0])
    graph2 = readTSV(basePath+"yagoTaxonomy.tsv")
    print len(graph2[0])
    graph3 = combineGraph(graph, graph2)
    print len(graph3[0])
    writeTofile(graph3, basePath+ "yago_ont_invert")