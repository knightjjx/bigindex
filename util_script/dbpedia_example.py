import rdflib
g=rdflib.Graph()
g.load('http://dbpedia.org/resource/Semantic_Web')

for s,p,o in g:
    print s,p,o

semweb=rdflib.URIRef('http://dbpedia.org/resource/Semantic_Web')
type=g.value(semweb, rdflib.RDFS.label)

dbpedia=rdflib.Namespace('http://dbpedia.org/ontology/')

abstracts=list(x for x in g.objects(semweb, dbpedia['abstract']) if x.language=='en')